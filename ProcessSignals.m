%% R peak height 0.2
%% RS distance = 50
%% Min ST distance 50
%% ST distance 100
%% Min Peak distance 250.
function varargout = ProcessSignals(varargin)
% PROCESSSIGNALS MATLAB code for ProcessSignals.fig
%      PROCESSSIGNALS, by itself, creates a new PROCESSSIGNALS or raises the existing
%      singleton*.
%
%      H = PROCESSSIGNALS returns the handle to a new PROCESSSIGNALS or the handle to
%      the existing singleton*.
%
%      PROCESSSIGNALS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROCESSSIGNALS.M with the  input arguments.
%
%      PROCESSSIGNALS('Property','Value',...) creates a new PROCESSSIGNALS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ProcessSignals_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ProcessSignals_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ProcessSignals

% Last Modified by GUIDE v2.5 08-Mar-2020 15:23:06

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @ProcessSignals_OpeningFcn, ...
    'gui_OutputFcn',  @ProcessSignals_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



warning off



% --- Executes just before ProcessSignals is made visible.
function ProcessSignals_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ProcessSignals (see VARARGIN)

% Choose default command line output for ProcessSignals
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ProcessSignals wait for user response (see UIRESUME)
% uiwait(handles.figure1);
%% uitable to frame
clear global
global displayMatrix
global StartDisplayFrame
%global DeletedSignals
% global chkAddPeak

%% flag to take previous display
global FONT_SIZE_PLOT_NO
global myEventT

myEventT = timer('StartDelay', 4, 'Period', 4, 'TasksToExecute', 2,'ExecutionMode', 'fixedRate');

FONT_SIZE_PLOT_NO=8;

%%[1. rowECG 2. processedECG 3.rowBVP1 4.processedBVP1 5.rowBVP2 6.processedBVP2]
%displayMatrix = [1 0 1 0 1 0];
displayMatrix =[0 get(handles.chkDataECG,'Value') 0 get(handles.chkDataBVP1,'Value') 0 get(handles.chkDataBVP2,'Value') ]
StartDisplayFrame = 1;
%chkAddPeak = false;
% 
uiTableECG = uitable(handles.uiPDataTable,'ColumnName',{'Q wave','R wave','S wave','T wave'},'Position',[2 2 200 270],'ColumnEditable',[true,true,true,true]);
uiTableBVP = uitable(handles.uiPDataTableBVP,'ColumnName',{'foot','peak','end',},'Position',[5 5 200 270],'ColumnEditable',[true,true,true,false]);

uiTableBVP2 = uitable(handles.uiPDataTableBVP1,'ColumnName',{'foot','peak','end',},'Position',[5 5 200 270],'ColumnEditable',[true,true,true,false]);

% tabg=uitabgroup(handles.panelProcess,'Position',[2 2 200 270])
% 
% 
% tab1 = uitab(tabg,'Title','ECG');
% tab2 = uitab(tabg,'Title','BVP');


%set(handles.uiPanelBVP1R,'Position',get(handles.uiPanelECGR,'Position'));
%set(handles.uiPanelBVP2R,'Position',get(handles.uiPanelECGR,'Position'));


resetTabPannel(handles);











%uitable(handles.uiPDataTable,'ColumnName',{'Q wave','R wave','S wave','T wave'},'Position',[2 2 200 270],'ColumnEditable',[true,true,true,true]);
%uitable(handles.uiPDataTable,'ColumnName',{'Q wave','R wave','S wave','T wave'},'Position',get(handles.uiPDataTable,'Position'),'ColumnEditable',[true,true,true,true]);
%uitable(handles.uiPDataTableBVP,'ColumnName',{'foot','peak','end',},'Position',[1 1 200 270],'ColumnEditable',[true,true,true,false]);
%uitable(handles.uiPDataTableBVP1,'ColumnName',{'foot','peak','end',},'Position',[1 1 200 270],'ColumnEditable',[true,true,true,false]);
















% set(a,'Parent',tab1);
% set(handles.uiPanelBVP1_2,'Parent',tab1);
% set(b,'Parent',tab2);

% % 
% % %Place panels into each tab
% % set(handles.uiPanelECG2,'Parent',handles.tab1)
% % set(handles.uiPanelBVP1_2,'Parent',handles.tab2)
% % set(handles.uiPanelBVP1_3,'Parent',handles.tab3)
% % 
% % 
% % set(handles.uiPDataTable,'Parent',handles.tab1);
% % set(handles.uiPanelECG2,'Parent',handles.tab1);
% % set(handles.uiPanelECG3,'Parent',handles.tab1);
% % set(handles.uiPDataTable,'position',get(handles.uiPDataTable,'position'));
% % set(handles.uiPanelECG2,'position',get(handles.uiPanelECG2,'position'));
% % set(handles.uiPanelECG3,'position',get(handles.uiPanelECG3,'position'));
% % 
% % 
% % set(handles.uiPDataTableBVP,'Parent',handles.tab2);
% % set(handles.uiPanelBVP1_2,'Parent',handles.tab2);
% % set(handles.uiPanelBVP1_3,'Parent',handles.tab2);
% % set(handles.uiPDataTableBVP,'position',get(handles.uiPDataTableBVP,'position'));
% % set(handles.uiPanelBVP1_2,'position',get(handles.uiPanelBVP1_2,'position'));
% % set(handles.uiPanelBVP1_3,'position',get(handles.uiPanelBVP1_3,'position'));
% % 
% % 
% % 
% % 
% % 
% % 
% % 
% % 
% % 
% % 
% % 
% % 
% % 
% % %Reposition each panel to same location as panel 1
% % set(handles.uiPanelECG2,'position',get(handles.uiPanelECG2,'position'));
% % set(handles.uiPanelBVP1_2,'position',get(handles.uiPanelBVP1_2,'position'));


%uiTableECG = uitable(ones(1,4),'ColumnName',{'W wave','R wave','S wave','T wave'},'Position',[20 20 200 200],'ColumnEditable',[true,true,true,true]);
setDisplayColor(handles)
clearDeletedPeak();
set(handles.pMenuDeletedRange,'String',{'Select one of Range'});



%set(handles.axes1,'Units','Pixels','Position',getpixelposition(handles.axes1)-[FIGURE_FRAME_X,0,FIGURE_FRAME_Y,0])
%set( findall( handles.uiPanelMain, '-property', 'Units' ), 'Units', 'Normalized' )

%set(handles.figure1,'Units','Pixels','Position',get(0,'ScreenSize'))



function resetTabPannel(handles)

handles.tgroup = uitabgroup('Parent', handles.panelProcess,'TabLocation', 'left');
handles.tgroupFilter = uitabgroup('Parent', handles.panelFilter,'TabLocation', 'left');

set(handles.uiPanelECGR,'Visible',0);
set(handles.uiPanelBVP1R,'Visible',0);
set(handles.uiPanelBVP2R,'Visible',0);
set(handles.uiPanelECGFilter,'Visible',0);
set(handles.uiPanelBVP1Filter,'Visible',0);
set(handles.uiPanelBVP2Filter,'Visible',0);


set(handles.pbDisplayRawECG,'Visible',0);
set(handles.pbDisplayProcessECG,'Visible',0);
set(handles.pbProcessECG,'Visible',0);
set(handles.chkSaveProcessedECG,'Visible',0);
set(handles.chkSaveECG_RSHeight,'Visible',0);
set(handles.chkECG,'Visible',0);
    
    
    
set(handles.pbDisplayRawBVP1,'Visible',0);
set(handles.pbDisplayProcessBVP1,'Visible',0);
set(handles.pbProcessBVP1,'Visible',0);
set(handles.chkSaveProcessedBVP1,'Visible',0);
set(handles.chkBVP1,'Visible',0);


set(handles.pbDisplayRawBVP2,'Visible',0);
set(handles.pbDisplayProcessBVP2,'Visible',0);
set(handles.pbProcessBVP2,'Visible',0);
set(handles.chkSaveProcessedBVP2,'Visible',0);
set(handles.chkBVP2,'Visible',0);


if get(handles.chkDataECG,'Value')==1
    handles.tab1 = uitab('Parent', handles.tgroup, 'Title', 'ECG');
    handles.filterTab1 = uitab('Parent', handles.tgroupFilter, 'Title', 'ECG');
    set(handles.uiPanelECGR,'Parent',handles.tab1,'Position',get(handles.uiPanelECGR,'Position'));
    set(handles.uiPanelECGFilter,'Parent',handles.filterTab1,'Position',get(handles.uiPanelECGFilter,'Position'));
    set(handles.uiPanelECGR,'Visible',1);
    set(handles.uiPanelECGFilter,'Visible',1);
    
    set(handles.pbDisplayRawECG,'Visible',1);
    set(handles.pbDisplayProcessECG,'Visible',1);
    set(handles.pbProcessECG,'Visible',1);
    set(handles.chkSaveProcessedECG,'Visible',1);
    set(handles.chkSaveECG_RSHeight,'Visible',1);
    set(handles.chkECG,'Visible',1);
    
end
if get(handles.chkDataBVP1,'Value')==1
    handles.tab2 = uitab('Parent', handles.tgroup, 'Title', 'BVP 1');
    handles.filterTab2 = uitab('Parent', handles.tgroupFilter, 'Title', 'BVP 1');
    set(handles.uiPanelBVP1R,'Parent',handles.tab2,'Position',get(handles.uiPanelECGR,'Position'));
    set(handles.uiPanelBVP1Filter,'Parent',handles.filterTab2,'Position',get(handles.uiPanelECGFilter,'Position'));
    set(handles.uiPanelBVP1R,'Visible',1);
    set(handles.uiPanelBVP1Filter,'Visible',1);
    
    
    set(handles.pbDisplayRawBVP1,'Visible',1);
    set(handles.pbDisplayProcessBVP1,'Visible',1);
    set(handles.pbProcessBVP1,'Visible',1);
    set(handles.chkSaveProcessedBVP1,'Visible',1);
    set(handles.chkBVP1,'Visible',1);
    
    
    
end
if  get(handles.chkDataBVP2,'Value')==1
    handles.tab3 = uitab('Parent', handles.tgroup, 'Title', 'BVP 2');
    handles.filterTab3 = uitab('Parent', handles.tgroupFilter, 'Title', 'BVP 2');
    set(handles.uiPanelBVP2R,'Parent',handles.tab3,'Position',get(handles.uiPanelECGR,'Position'));
    set(handles.uiPanelBVP2Filter,'Parent',handles.filterTab3,'Position',get(handles.uiPanelECGFilter,'Position'));
    set(handles.uiPanelBVP2R,'Visible',1);
    set(handles.uiPanelBVP2Filter,'Visible',1);
    
    
    set(handles.pbDisplayRawBVP2,'Visible',1);
    set(handles.pbDisplayProcessBVP2,'Visible',1);
    set(handles.pbProcessBVP2,'Visible',1);
    set(handles.chkSaveProcessedBVP2,'Visible',1);
    set(handles.chkBVP2,'Visible',1);
    
    
    
end

% --- Outputs from this function are returned to the command line.
function varargout = ProcessSignals_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function edit65_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


function edit2_Callback(~, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editFFilterF_Callback(hObject, eventdata, handles)
% hObject    handle to editFFilterF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFFilterF as text
%        str2double(get(hObject,'String')) returns contents of editFFilterF as a double


% --- Executes during object creation, after setting all properties.
function editFFilterF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFFilterF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editFFilterB_Callback(hObject, eventdata, handles)
% hObject    handle to editFFilterB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFFilterB as text
%        str2double(get(hObject,'String')) returns contents of editFFilterB as a double


% --- Executes during object creation, after setting all properties.
function editFFilterB_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFFilterB (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function bvpMedianFilter_FN_Callback(hObject, eventdata, handles)
% hObject    handle to bvpMedianFilter_FN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bvpMedianFilter_FN as text
%        str2double(get(hObject,'String')) returns contents of bvpMedianFilter_FN as a double


% --- Executes during object creation, after setting all properties.
function bvpMedianFilter_FN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bvpMedianFilter_FN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function bvpRSDist_Callback(hObject, eventdata, handles)
% hObject    handle to editBVPQRDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBVPQRDist as text
%        str2double(get(hObject,'String')) returns contents of editBVPQRDist as a double


% --- Executes during object creation, after setting all properties.
function bvpRSDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBVPQRDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editBVPMinPeakDist_Callback(hObject, eventdata, handles)
% hObject    handle to editBVPMinPeakDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBVPMinPeakDist as text
%        str2double(get(hObject,'String')) returns contents of editBVPMinPeakDist as a double


% --- Executes during object creation, after setting all properties.
function editBVPMinPeakDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBVPMinPeakDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editBVPQRDist_Callback(hObject, eventdata, handles)
% hObject    handle to editBVPQRDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBVPQRDist as text
%        str2double(get(hObject,'String')) returns contents of editBVPQRDist as a double


% --- Executes during object creation, after setting all properties.
function editBVPQRDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBVPQRDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbLoadFiles.
function pbLoadFiles_Callback(hObject, eventdata, handles)
% hObject    handle to pbLoadFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global files
global fileID
global fileDir
global displayMatrix
global FLAG_PREV_DISPLAY

fFilterF=get(handles.editFFilterF,'String');
fFilterB=get(handles.editFFilterB,'String');
fileDir = uigetdir();

selIndex = get(handles.pMenuDataType,'Value')
allOptions = get(handles.pMenuDataType,'String')
dataExt = allOptions{selIndex};

if get(handles.rbRawData,'Value')==1
    files = dir(strcat(fileDir,'/',fFilterF,'*',fFilterB,'.',dataExt));
    fileID = 1;
    
    if size(files,1)>0
  
          
    
        fileNames = {};
        for i=1:size(files,1)
            fileNames{1,i}=files(i).name;
        end

        set(handles.pMenuSignalFiles,'String',fileNames);

        pMenuSignalFiles_Callback(handles.pMenuSignalFiles,eventdata,handles);
        set(handles.editVersion,'String','');
        FLAG_PREV_DISPLAY = true;
        %prevDisplayMatrix = displayMatrix;
        %displayMatrix=[1 0 1 0 1 0];
        displayMatrix=[get(handles.chkDataECG,'Value') 0 get(handles.chkDataBVP1,'Value') 0 get(handles.chkDataBVP2,'Value') 0]
        clearDeletedPeak();
        set(handles.txtTpeakECG,'String','# Peaks:');
        set(handles.txtTpeakBVP1,'String','# Peaks:');
        set(handles.txtTpeakBVP2,'String','# Peaks:');
    else
       %  msgbox('Data Raw files with the configuration not found..');
    end
end


if get(handles.rbProcessedData,'Value')==1
    %% get all processed files with out version
    files2 = dir(strcat(fileDir,'/',fFilterF,'*',fFilterB,'.mat'));
    %% get all processed files with version
    files1 = dir(strcat(fileDir,'/',fFilterF,'*',fFilterB,'-*.mat'));
    
    files = [files1;files2];
    fileID = 1;
    
    if size(files,1)>0
    
        fileNames = {};
        for i=1:size(files,1)
            fileNames{1,i}=files(i).name;
        end
        set(handles.pMenuSignalFiles,'String',fileNames);
        clearDeletedPeak();
        pMenuSignalFiles_Callback(handles.pMenuSignalFiles,eventdata,handles);
    else
        msgbox('Processed files with the configuration not found..');
    end


end

% % --- Executes on selection change in pMenuSignalFiles.
% function pMenuSignalFiles_Callback(hObject, eventdata, handles)
% % hObject    handle to pMenuSignalFiles (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% global displayMatrix
% global prevDisplayMatrix
% global chkRange
% global FLAG_PREV_DISPLAY
% global StartDisplayFrame
% global fileDir
% 
% %global allParam
% global deletedRangeInfo
% clearAllData(handles);
% clearPlots();
% clearDeletedPeak();
% set(handles.chkInvertECG,'Value',0);
% if get(handles.rbProcessedData,'Value')==0
%     
%     %% reset displayMatrix to previous display matrix
%     chkRange = false;
%     prevDisplayMatrix=displayMatrix;
%     
%     
%     
%     setDisplayColor(handles);
%     %displaySignal(handles);
%     displayMatrix=[1 0 1 0 1 0];
%     viewRawData(handles);
%     set(handles.editVersion,'String','');
%     %%% on the previous display flag
%     FLAG_PREV_DISPLAY = true;
%     
%     set(handles.txtTpeakECG,'String','# Peaks:');
%     set(handles.txtTpeakBVP1,'String','# Peaks:');
%     set(handles.txtTpeakBVP2,'String','# Peaks:');
% else
%     allFiles = get(handles.pMenuSignalFiles,'String');
%     selectedIndex = get(handles.pMenuSignalFiles,'Value');
%     try
%         fileName = allFiles{selectedIndex};
%     catch
%         %% there is error when program is swich from exel to mat
%         selectedIndex=1;
%         fileName = allFiles{1}
%     end
%     load(strcat(fileDir,'/',fileName));
%     loadAllParams(handles,allParam)
%     
%     set(handles.pMenuDeletedRange,'String',deletedRangeInfo);
%     StartDisplayFrame=1;
%     displaySignal(handles)
%     setDisplayColor(handles);
%     enabledDataDisplay(handles);
%     
% end





% Hints: contents = cellstr(get(hObject,'String')) returns pMenuSignalFiles contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuSignalFiles




% --- Executes on selection change in pMenuSignalFiles.
function pMenuSignalFiles_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuSignalFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global displayMatrix
global prevDisplayMatrix
global chkRange
global FLAG_PREV_DISPLAY
global StartDisplayFrame
global ecgDataProcessed
global bvpDataProcessed
global bvpDataProcessed2
global fileDir

%global allParam
global deletedRangeInfo
clearAllData(handles);
clearPlots();
clearDeletedPeak();




set(handles.chkInvertECG,'Value',0);

allFiles = get(handles.pMenuSignalFiles,'String');
selectedIndex = get(handles.pMenuSignalFiles,'Value');
try
    fileName = allFiles{selectedIndex};
catch
    %% there is error when program is swich from exel to mat
   
    set(handles.pMenuSignalFiles,'Value',1);
    fileName = allFiles{1}
end

if get(handles.rbProcessedData,'Value')==0
   
    %% reset displayMatrix to previous display matrix
    chkRange = false;
    prevDisplayMatrix=displayMatrix;
   
    
    
   
   
    setDisplayColor(handles);
    %displaySignal(handles);
    %displayMatrix=[get(handles.chkDataECG,'Value') 0 get(handles.chkDataBVP1,'Value') 0 get(handles.chkData,'Value') 0];
    viewRawData(handles);
    set(handles.editVersion,'String','');
    %%% on the previous display flag
    FLAG_PREV_DISPLAY = true;
   
    set(handles.txtTpeakECG,'String','# Peaks:');
    set(handles.txtTpeakBVP1,'String','# Peaks:');
    set(handles.txtTpeakBVP2,'String','# Peaks:');
   
      if ~isempty(strfind(fileName,'BA_a'))|  ~isempty(strfind(fileName,'BB_a'))|~isempty(strfind(fileName,'BC_a'))|~isempty(strfind(fileName,'BD_a'))	
	        set(handles.editFrameSize,'String','250')	
	    else	
	        set(handles.editFrameSize,'String','500');	
	    end	
	   	
else
   
    load(strcat(fileDir,'/',fileName));
    loadAllParams(handles,allParam)
    
    if size(ecgDataProcessed,1)==0
        set(handles.chkDataECG,'Value',0);
    else
        set(handles.chkDataECG,'Value',1);
    end


    if size(bvpDataProcessed,1)==0
        set(handles.chkDataBVP1,'Value',0);
    else
        set(handles.chkDataBVP1,'Value',1);
    end


    if size(bvpDataProcessed2,1)==0
        set(handles.chkDataBVP2,'Value',0);
    else
        set(handles.chkDataBVP2,'Value',1);
    end
    
    set(handles.pMenuDeletedRange,'String',deletedRangeInfo);
    StartDisplayFrame=1;
    displaySignal(handles)
    setDisplayColor(handles);
    enabledDataDisplay(handles);
    resetTabPannel(handles);
end





% --- Executes during object creation, after setting all properties.
function pMenuSignalFiles_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuSignalFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editCuttOffFront_Callback(hObject, eventdata, handles)
% hObject    handle to editCuttOffFront (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editCuttOffFront as text
%        str2double(get(hObject,'String')) returns contents of editCuttOffFront as a double


% --- Executes during object creation, after setting all properties.
function editCuttOffFront_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editCuttOffFront (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editCuttOffBack_Callback(hObject, eventdata, handles)
% hObject    handle to editCuttOffBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editCuttOffBack as text
%        str2double(get(hObject,'String')) returns contents of editCuttOffBack as a double


% --- Executes during object creation, after setting all properties.
function editCuttOffBack_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editCuttOffBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editFrameSize_Callback(hObject, eventdata, handles)
% hObject    handle to editFrameSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editFrameSize as text
%        str2double(get(hObject,'String')) returns contents of editFrameSize as a double


% --- Executes during object creation, after setting all properties.
function editFrameSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editFrameSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit22_Callback(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit22 as text
%        str2double(get(hObject,'String')) returns contents of edit22 as a double


% --- Executes during object creation, after setting all properties.
function edit22_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecgHalfPowerFreq1_Callback(hObject, eventdata, handles)
% hObject    handle to ecgHalfPowerFreq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgHalfPowerFreq1 as text
%        str2double(get(hObject,'String')) returns contents of ecgHalfPowerFreq1 as a double


% --- Executes during object creation, after setting all properties.
function ecgHalfPowerFreq1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecgHalfPowerFreq1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecgMinPeakD_Callback(hObject, eventdata, handles)
% hObject    handle to ecgMinPeakD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgMinPeakD as text
%        str2double(get(hObject,'String')) returns contents of ecgMinPeakD as a double


% --- Executes during object creation, after setting all properties.
function ecgMinPeakD_CreateFcn(hObject, ~, handles)
% hObject    handle to ecgMinPeakD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecgHalfPowerFreq2_Callback(hObject, eventdata, handles)
% hObject    handle to ecgHalfPowerFreq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgHalfPowerFreq2 as text
%        str2double(get(hObject,'String')) returns contents of ecgHalfPowerFreq2 as a double


% --- Executes during object creation, after setting all properties.
function ecgHalfPowerFreq2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecgHalfPowerFreq2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecgRSDist_Callback(hObject, eventdata, handles)
% hObject    handle to ecgRSDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgRSDist as text
%        str2double(get(hObject,'String')) returns contents of ecgRSDist as a double


% --- Executes during object creation, after setting all properties.
function ecgRSDist_CreateFcn(hObject, eventdata, ~)
% hObject    handle to ecgRSDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecgQRDist_Callback(hObject, eventdata, handles)
% hObject    handle to ecgQRDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgQRDist as text
%        str2double(get(hObject,'String')) returns contents of ecgQRDist as a double


% --- Executes during object creation, after setting all properties.
function ecgQRDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecgQRDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecgSTDist_Callback(hObject, eventdata, handles)
% hObject    handle to ecgSTDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgSTDist as text
%        str2double(get(hObject,'String')) returns contents of ecgSTDist as a double


% --- Executes during object creation, after setting all properties.
function ecgSTDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecgSTDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbProcessSignal.
function pbProcessSignal_Callback(hObject, eventdata, handles)
% hObject    handle to pbProcessSignal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pbDisplayAll.
function pbDisplayAll_Callback(hObject, eventdata, handles)
% hObject    handle to pbDisplayAll (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dataCuttOff
global StartDisplayFrame
global DataRaw

%% convert row to column if data is in columns
if size(DataRaw,1)<size(DataRaw,2)
    DataRaw = DataRaw';
end

StartDisplayFrame = 1;
if isempty(dataCuttOff)
    set(handles.editDWindowSize,'String',num2str(size(DataRaw,1)))
else
    set(handles.editDWindowSize,'String',num2str(size(dataCuttOff,1)))
end
set(handles.pbAddPeak,'enable','off')
displaySignal(handles)
clearDeletedPeak()



% --- Executes on button press in pbDisplayBack.
function pbDisplayBack_Callback(hObject, eventdata, handles)
% hObject    handle to pbDisplayBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

function clearPlots()
global ax1
global ax2
global ax3
global ax4
global ax5
global ax6



try
    cla(ax1)
    %close(ax1)
catch
end
try
    %close(ax2)
    %drawnow
    cla(ax2)
    
catch
end
try
    cla(ax3)
catch
end
try
    cla(ax4)
catch
end
try
    cla(ax5)
catch
end
try
    cla(ax6)
catch
end

%%chkAddPeak = false

% --- Executes on button press in pbDisplayNext.
function pbDisplayNext_Callback(hObject, eventdata, handles)
% hObject    handle to pbDisplayNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global dataCuttOff

global StartDisplayFrame




% if StartDisplayFrame <= (size(dataCuttOff,1) - str2num(get(handles.editDWindowSize,'String')))
%     StartDisplayFrame = min(StartDisplayFrame + str2num(get(handles.editDWindowSize,'String'))+1, size(dataCuttOff,1)-str2num(get(handles.editDWindowSize,'String'))+1);
%     displaySignal(handles)
% end

StartDisplayFrame = StartDisplayFrame +str2num(get(handles.editStepsMove,'String'));
StartDisplayFrame = min(StartDisplayFrame,size(dataCuttOff,1) - str2num(get(handles.editStepsMove,'String')));
if StartDisplayFrame <0
    StartDisplayFrame = 1;
end
displaySignal(handles);
clearDeletedPeak();
set(handles.pbAddPeak,'enable','off')


function clearDeletedPeak()
global deletedECGPeak
global deletedBVP1Peak
global deletedBVP2Peak
global chkPeakFrame

deletedECGPeak=[];
deletedBVP1Peak=[];
deletedBVP2Peak=[];
chkPeakFrame = false;


% --- Executes on button press in pbSaveResults.
function editBVPRSDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pbSaveResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --- Executes on button press in pbSaveResults.
function pbSaveResults_Callback(hObject, eventdata, handles)
% hObject    handle to pbSaveResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



allFiles = get(handles.pMenuSignalFiles,'String');
selectedIndex = get(handles.pMenuSignalFiles,'Value');
fileName = allFiles{selectedIndex};
fileName = strsplit(fileName,'.');
fileName = fileName{1};

global  dataCuttOff
global  DataRaw

global  ecgDataProcessed
global  bvpDataProcessed
global  bvpDataProcessed2

global  ecgLocRwave
global  ecgLocSwave
global  ecgLocQwave
global  ecgLocTwave



global  bvpLocRwave
global  bvpLocSwave
global  bvpLocQwave

global  bvpLocRwave2
global  bvpLocSwave2
global  bvpLocQwave2

global displayMatrix

global SAVE_EXCEL_FILE
global DeletedPeaksCol


vName = get(handles.editVersion,'String');
if ~ isempty(vName)
    fileName=strcat(fileName,'-',vName);
end


if(get(handles.chkSaveInExcel,'Value')==1)
    SAVE_EXCEL_FILE = get(handles.editExcelFileName,'String');
    saveInExcel(handles,fileName);
end
if (get(handles.chkSaveInMat,'Value')==1)
    
    %tmpFileName = strcat('Results_MATwithP/',fileName,'.mat');
    %tmpFileName = strcat('Results_oldP8withP/',fileName,'.mat');
    saveLoc = uigetdir();
    tmpFileName = strcat(saveLoc,'/',fileName,'.mat');
    
    allParam = getAllParams(handles);
    
    deletedRangeInfo = get(handles.pMenuDeletedRange,'String')
    
    
    save(tmpFileName,'DataRaw','dataCuttOff','ecgDataProcessed','bvpDataProcessed','bvpDataProcessed2','ecgLocRwave','ecgLocSwave','ecgLocQwave','ecgLocTwave','bvpLocRwave','bvpLocSwave','bvpLocQwave','bvpLocRwave2','bvpLocSwave2','bvpLocQwave2','displayMatrix','allParam','deletedRangeInfo','DeletedPeaksCol');
   % msgbox(strcat('All Results are saved at :',tmpFileName));
end

set(findall(handles.uiPanelMain, '-property', 'enable'), 'enable', 'on')



function loadAllParams(handles,param)


set(handles.editCuttOffFront,'String',param.CutOffFront);
set(handles.editCuttOffBack,'String',param.CutOffBack);
set(handles.ecgHalfPowerFreq1,'String',param.ECGHalfPowFreq1);
set(handles.ecgHalfPowerFreq2,'String',param.ECGHalfPowFreq2);
set(handles.ecgMedianFilter_FN,'String',param.ECGMedianFilter);

set(handles.ecgRPeakHeight,'String',param.ECGRPeakHeight);
set(handles.ecgMinPeakD,'String',param.ECGMinPeakDist);
set(handles.ecgRSDist,'String',param.ECGRSDist);
set(handles.ecgQRDist,'String',param.ECGQRDist);
set(handles.ecgSTDist,'String',param.ECGSTDist);

try
    set(handles.ecgSTDistMin,'String',param.ECGSTDistMin);
catch
    set(handles.ecgSTDistMin,'String',0);
end

%% inverse options was added later on so many not work in some of results ending with _8
try
    set(handles.chkInvertECG,'Value',param.InverseECG);
catch
    set(handles.chkInvertECG,'Value',0);
end

set(handles.chkLowPassIIRFilter,'Value',param.ChkLowPassIRRFilter);
set(handles.chkHighPassIIRFilter,'Value',param.ChkHighPassIRRFilter);
set(handles.ecgLowPassBandF,'String',param.ECGLowPassBandF);
set(handles.ecgHighPassBandF,'String',param.ECGHighPassBandF);
set(handles.ecgLowPassBandR,'String',param.ECGLowPassBandR);
set(handles.ecgHighPassBandR,'String',param.ECGHighPassBandR);
set(handles.editDWindowSize,'String',param.DisplayWindowSize);




set(handles.bvpMedianFilter_FN,'String',param.BVP1MedianFilter)
set(handles.editBVPMinPeakHeight,'String',param.BVP1MinPeakHeight )
set(handles.editBVPQRDist,'String',param.BVP1QRDist)
set(handles.editBVPMinPeakDist,'String',param.BVP1MinPeakDist)
set(handles.editBVPRSDist,'String',param.BVP1RSDist)

set(handles.bvp1MedianFilter_FN,'String',param.BVP2MedianFilter)
set(handles.editBVP1MinPeakHeight,'String',param.BVP2MinPeakHeight)
set(handles.editBVP1QRDist,'String',param.BVP2QRDist)
set(handles.bvp1MinPeakDist,'String',param.BVP2MinPeakDist)
set(handles.editBVP1RSDist,'String',param.BVP2RSDist)


set(handles.editExcelFileName,'String',param.ExcelFileName)
set(handles.editVersion,'String',param.Version)
set(handles.editFrameSize,'String',param.FrameSize)
set(handles.editDWindowSize,'String',param.WindowSize)
set(handles.editStartFrame,'String',param.StartFrame)



function [param]=getAllParams(handles)

param={};
param.CutOffFront=get(handles.editCuttOffFront,'String');
param.CutOffBack=get(handles.editCuttOffBack,'String');
param.ECGHalfPowFreq1=get(handles.ecgHalfPowerFreq1,'String');
param.ECGHalfPowFreq2=get(handles.ecgHalfPowerFreq2,'String');
param.ECGMedianFilter=get(handles.ecgMedianFilter_FN,'String');
param.ECGRPeakHeight=get(handles.ecgRPeakHeight,'String');
param.ECGMinPeakDist=get(handles.ecgMinPeakD,'String');
param.ECGRSDist=get(handles.ecgRSDist,'String');
param.ECGQRDist=get(handles.ecgQRDist,'String');
param.ECGSTDist=get(handles.ecgSTDist,'String');
param.InverseECG=get(handles.chkInvertECG,'Value');
param.ECGSTDistMin = get(handles.ecgSTDistMin,'String');

param.ChkLowPassIRRFilter=get(handles.chkLowPassIIRFilter,'Value');
param.ChkHighPassIRRFilter=get(handles.chkHighPassIIRFilter,'Value');
param.ECGLowPassBandF=get(handles.ecgLowPassBandF,'String');
param.ECGHighPassBandF=get(handles.ecgHighPassBandF,'String');
param.ECGLowPassBandR=get(handles.ecgLowPassBandR,'String');
param.ECGHighPassBandR=get(handles.ecgHighPassBandR,'String');
param.DisplayWindowSize = get(handles.editDWindowSize,'String');




param.BVP1MedianFilter = get(handles.bvpMedianFilter_FN,'String')
param.BVP1MinPeakHeight = get(handles.editBVPMinPeakHeight,'String')
param.BVP1QRDist=get(handles.editBVPQRDist,'String')
param.BVP1MinPeakDist=get(handles.editBVPMinPeakDist,'String')
param.BVP1RSDist=get(handles.editBVPRSDist,'String')

param.BVP2MedianFilter = get(handles.bvp1MedianFilter_FN,'String')
param.BVP2MinPeakHeight = get(handles.editBVP1MinPeakHeight,'String')
param.BVP2QRDist = get(handles.editBVP1QRDist,'String')
param.BVP2MinPeakDist = get(handles.bvp1MinPeakDist,'String')
param.BVP2RSDist = get(handles.editBVP1RSDist,'String')


param.ExcelFileName = get(handles.editExcelFileName,'String')
param.Version=get(handles.editVersion,'String')
param.FrameSize = get(handles.editFrameSize,'String')
param.WindowSize = get(handles.editDWindowSize,'String')
param.StartFrame = get(handles.editStartFrame,'String')


function saveInExcel(handles,colName)


%%h = waitbar(0,'Please wait...');
%set(findall(handles.uiPanelMain, '-property', 'enable'), 'enable', 'off');
frameSize = str2num(get(handles.editFrameSize,'String'))

saveMsg{1,1} = strcat('FileName :',colName);
saveMsg{1,2}='--------------------';

saveCount=2;
eSavingCount=0;

%%% ECG information
TotalCount = 25;
if get(handles.chkDataECG,'Value')==1
    if get(handles.chkSaveECG_QL,'Value')==1

        %waitbar(saveCount/TotalCount,h,'ECG Q peak Location saving....')
        flagECGqLoc = saveECG_qPeakLoc(colName);
        saveECG_qPeakLocInTime(colName,frameSize);
        if ~flagECGqLoc
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='ECG Q peak Location saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end
    if get(handles.chkSaveECG_RL,'Value')==1
        flagECGrLoc = saveECG_rPeakLoc(colName);
        saveECG_rPeakLocInTime(colName,frameSize);
        %waitbar(saveCount/TotalCount,h,'ECG R peak Location saving....')
        if ~flagECGrLoc
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='ECG R peak Location saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end
    if get(handles.chkSaveECG_SL,'Value')==1
        flagECGsLoc = saveECG_sPeakLoc(colName);
        saveECG_sPeakLocInTime(colName,frameSize);
        %waitbar(saveCount/TotalCount,h,'ECG S peak Location saving....')
        if ~flagECGsLoc
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='ECG S peak Location saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveECG_TL,'Value')==1
        flagECGtLoc = saveECG_tPeakLoc(colName);
        saveECG_tPeakLocInTime(colName,frameSize)
        %waitbar(saveCount/TotalCount,h,'ECG T peak Location saving....')
        if ~flagECGtLoc
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='ECG T peak Location saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end


    if get(handles.chkSaveECG_QH,'Value')==1
        flagECGqHeight = saveECG_qPeakHeight(colName);
        %waitbar(saveCount/TotalCount,h,'ECG Q peak Height saving....')
        if ~flagECGqHeight
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='ECG Q peak Height saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end
    if get(handles.chkSaveECG_RH,'Value')==1
        flagECGrHeight = saveECG_rPeakHeight(colName);
        %waitbar(saveCount/TotalCount,h,'ECG R peak Height saving....')
        if ~flagECGrHeight
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='ECG R peak Height saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end
    if get(handles.chkSaveECG_SH,'Value')==1
        flagECGsHeight = saveECG_sPeakHeight(colName);
        %waitbar(saveCount/TotalCount,h,'ECG S Peak Height saving....')
        if ~flagECGsHeight
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='ECG S peak Height saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveECG_TH,'Value')==1
        flagECGtHeight = saveECG_tPeakHeight(colName);
        %waitbar(saveCount/TotalCount,h,'ECG T Peak Height saving....')
        if ~flagECGtHeight
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='ECG T peak Height saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end


end



if get(handles.chkRTInterval,'Value')==1 &&   get(handles.chkDataECG,'Value')==1
    %flagECG_RTInterval = saveECG_tPeakHeight(colName);
    flagECG_RTInterval = saveECG_RTInterval(colName,frameSize);
    %waitbar(saveCount/TotalCount,h,'ECG RT Interval saving...')
    if ~flagECG_RTInterval
        saveCount = saveCount+1;
        saveMsg{1,saveCount} ='ECG RT Interval saved';
    else
        eSavingCount=eSavingCount+1;
    end
end




if get(handles.chkTRInterval,'Value')==1 &&   get(handles.chkDataECG,'Value')==1
    %flagECG_RTInterval = saveECG_tPeakHeight(colName);
    flagECG_TRInterval = saveECG_TRInterval(colName,frameSize);
    %waitbar(saveCount/TotalCount,h,'ECG TR Interval saving...')
    if ~flagECG_TRInterval
        saveCount = saveCount+1;
        saveMsg{1,saveCount} ='ECG TR Interval saved.';
    else
        eSavingCount=eSavingCount+1;
    end
end



if get(handles.chkRRInterval,'Value')==1 &&   get(handles.chkDataECG,'Value')==1
    %flagECG_RTInterval = saveECG_tPeakHeight(colName);
    flagECG_RRInterval = saveECG_RRInterval(colName,frameSize);
    %waitbar(saveCount/TotalCount,h,'ECG TR Interval saving...')
    if ~flagECG_RRInterval
        saveCount = saveCount+1;
        saveMsg{1,saveCount} ='ECG TR Interval saved.';
    else
        eSavingCount=eSavingCount+1;
    end
end



%%% BVP 1 information

if get(handles.chkDataBVP1,'Value')==1

    if get(handles.chkSaveBVP1_FL,'Value')==1 && get(handles.chkDataBVP1,'Value')==1
        flagBVP1qLoc = saveBVP1_FPeakLoc(colName);
        %waitbar(saveCount/TotalCount,h,'BVP1 Foot Location saving....')
        if ~flagBVP1qLoc
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP1 foot Location saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveBVP1_PL,'Value')==1 && get(handles.chkDataBVP1,'Value')==1
        flagBVP1rLoc = saveBVP1_PPeakLoc(colName);
        %waitbar(saveCount/TotalCount,h,'BVP1 Peak Location saving....');
        if ~flagBVP1rLoc
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP1 peak Location saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveBVP1_EL,'Value')==1 && get(handles.chkDataBVP1,'Value')==1
        flagBVP1sLoc = saveBVP1_EPeakLoc(colName);
        %waitbar(saveCount/TotalCount,h,'BVP1 End Location saving....');
        if ~flagBVP1sLoc
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP1 end Location saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveBVP1_FH,'Value')==1 && get(handles.chkDataBVP1,'Value')==1
        flagBVP1qHeight =saveBVP1_FPeakHeight(colName);
        %waitbar(saveCount/TotalCount,h,'BVP1 Foot Height saving....');
        if ~flagBVP1qHeight
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP1 foot Height saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveBVP1_PH,'Value')==1 && get(handles.chkDataBVP1,'Value')==1
        flagBVP1rHeight =saveBVP1_PPeakHeight(colName);
        %waitbar(saveCount/TotalCount,h,'BVP1 Peak Height saving....');
        if ~flagBVP1rHeight
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP1 peak Height saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveBVP1_EH,'Value')==1 && get(handles.chkDataBVP1,'Value')==1
        flagBVP1sHeight =saveBVP1_EPeakHeight(colName);
        %waitbar(saveCount/TotalCount,h,'BVP1 End Height saving....');
        if ~flagBVP1sHeight
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP1 end Height saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

end
%%%%%  BVP 2 information

if get(handles.chkDataBVP2,'Value')==1
    if get(handles.chkSaveBVP2_FL,'Value')==1 && get(handles.chkDataBVP2,'Value')==1
        flagBVP2qLoc = saveBVP2_FPeakLoc(colName);
        %waitbar(saveCount/TotalCount,h,'BVP2 Foot Location saving....');
        if ~flagBVP2qLoc
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP2 foot Location saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveBVP2_PL,'Value')==1 && get(handles.chkDataBVP2,'Value')==1
        flagBVP2rLoc = saveBVP2_PPeakLoc(colName);
        %waitbar(saveCount/TotalCount,h,'BVP2 Peak Location saving....');
        if ~flagBVP2rLoc
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP2 peak Location saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveBVP2_EL,'Value')==1 && get(handles.chkDataBVP2,'Value')==1
        flagBVP2sLoc = saveBVP2_EPeakLoc(colName);
        %waitbar(saveCount/TotalCount,h,'BVP2 End Location saving....');
        if ~flagBVP2sLoc
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP2 end Location saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveBVP2_FH,'Value')==1 && get(handles.chkDataBVP2,'Value')==1
        flagBVP2qHeight = saveBVP2_FPeakHeight(colName);
        %waitbar(saveCount/TotalCount,h,'BVP2 Foot Height saving....');
        if ~flagBVP2qHeight
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP2 foot Height saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveBVP2_PH,'Value')==1 && get(handles.chkDataBVP2,'Value')==1
        flagBVP2rHeight = saveBVP2_PPeakHeight(colName);
        %waitbar(saveCount/TotalCount,h,'BVP2 Peak Height saving....');
        if ~flagBVP2rHeight
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP2 peak Height saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

    if get(handles.chkSaveBVP2_EH,'Value')==1 && get(handles.chkDataBVP2,'Value')==1
        flagBVP2sHeight = saveBVP2_EPeakHeight(colName);
        %waitbar(saveCount/TotalCount,h,'BVP2 End Height saving....');
        if ~flagBVP2sHeight
            saveCount = saveCount+1;
            saveMsg{1,saveCount} ='BVP2 end Height saved.';
        else
            eSavingCount=eSavingCount+1;
        end
    end

end



%%% index deleted ECG BVP1 and BVP2


% flagECGDel = saveECGDelIndex(colName);
% if ~flagECGDel
%     %waitbar(saveCount/TotalCount,h,'ECG Deleted Index saving....');
%     saveCount = saveCount+1;
%     saveMsg{1,saveCount} ='ECG Deleted Index saved.';
% end
% flagBVP1Del = saveBVP1DelIndex(colName);
% if ~flagBVP1Del
%     %waitbar(saveCount/TotalCount,h,'BVP1 Deleted Index saving....');
%     saveCount = saveCount+1;
%     saveMsg{1,saveCount} ='BVP1 Deleted Index saved.';
% end
% flagBVP2Del = saveBVP2DelIndex(colName);
% if ~flagBVP2Del
%     %waitbar(saveCount/TotalCount,h,'BVP2 Deleted Index saving....');
%     saveCount = saveCount+1;
%     saveMsg{1,saveCount} ='BVP2 Deleted Index saved.';
% end

%% ECG BVP1 delay
if get(handles.chkSaveDelayECG_BVP1,'Value')==1
    flagECG_BVP1Delay = saveECG_BVP1Delay(colName);
    if ~flagECG_BVP1Delay
        %waitbar(saveCount/TotalCount,h,'BVP(R) BVP1(foot) Delay  saving....');
        saveCount = saveCount+1;
        saveMsg{1,saveCount} ='ECG(R) BVP1(foot) Delay saved.';
    else
        eSavingCount=eSavingCount+1;
    end
end
% ECG BVP2 delay
if get(handles.chkSaveDelayECG_BVP2,'Value')==1
    flagECG_BVP2Delay = saveECG_BVP2Delay(colName);
    %waitbar(saveCount/TotalCount,h,'BVP(R) BVP2(foot) Delay  saving....');
    if ~flagECG_BVP2Delay
        saveCount = saveCount+1;
        saveMsg{1,saveCount} ='ECG(R) BVP2(foot) Delay saved.';
    else
        eSavingCount=eSavingCount+1;
    end
end

%% ECG RS Height
if get(handles.chkSaveECG_RSHeight,'Value')==1
    flagECG_RSHeight = saveECG_RSHeight(colName);
    %waitbar(saveCount/TotalCount,h,'ECG RS peak Height....');
    if ~flagECG_RSHeight
        saveCount = saveCount+1;
        saveMsg{1,saveCount} ='ECG RS peak Height saved.';
    else
        eSavingCount=eSavingCount+1;
    end
end

%% ECG Processed Signals


if get(handles.chkSaveProcessedECG,'Value')==1
    flagECG_Processed = saveDataECGProcessed(colName);
    if ~flagECG_Processed
        saveCount = saveCount+1;
        saveMsg{1,saveCount} ='ECG Processed Signal Saved.';
    else
        eSavingCount=eSavingCount+1;
    end
end

if get(handles.chkSaveProcessedBVP1,'Value')==1
    flagECG_Processed = saveDataBVP1Processed(colName);
    if ~flagECG_Processed
        saveCount = saveCount+1;
        saveMsg{1,saveCount} ='BVP1 Processed Signal Saved.';
    else
        eSavingCount=eSavingCount+1;
    end
end


if get(handles.chkSaveProcessedBVP2,'Value')==1
    flagECG_Processed = saveDataBVP2Processed(colName);
    if ~flagECG_Processed
        saveCount = saveCount+1;
        saveMsg{1,saveCount} ='BVP2 Processed Signal Saved.';
    else
        eSavingCount=eSavingCount+1;
    end
end



% if eSavingCount==0
%     msgbox(saveMsg,'Success ');
% elseif saveCount ==2
%     msgbox(saveMsg,'Error','error');
% else
%     msgbox(saveMsg,'Partially Successed','warn');
% end


if ~exist('Results', 'dir')
    mkdir('Results')
end
%%close(h)


%%% string rPeak values in excel file
function [isFoundECG_Q]=saveECG_qPeakLoc(colName)
global ecgLocQwave
global SAVE_EXCEL_FILE


qECGpeakLoc = ecgLocQwave(:,1);
qECGpeakLoc = [colName;num2cell(qECGpeakLoc)];
isFoundECG_Q = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'qECGpeakLoc');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_Q = true;
            break
        end
    end
    
    if isFoundECG_Q == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        maxSize = max(size(existingData,1),size(qECGpeakLoc,1));
        if size(qECGpeakLoc,1) < maxSize
            qECGpeakLoc = [qECGpeakLoc;num2cell(nan(maxSize - size(qECGpeakLoc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  qECGpeakLoc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'qECGpeakLoc');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), qECGpeakLoc, 'qECGpeakLoc');
end


%%% string rPeak values in excel file
function [isFoundECG_Q]=saveECG_qPeakLocInTime(colName,frameSize)
global ecgLocQwave
global SAVE_EXCEL_FILE


qECGpeakLoc = ecgLocQwave(:,1);
qECGpeakLoc = [colName;num2cell((qECGpeakLoc/frameSize)*1000)];
isFoundECG_Q = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'qECGpeakLocInTime');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_Q = true;
            break
        end
    end
    
    if isFoundECG_Q == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        maxSize = max(size(existingData,1),size(qECGpeakLoc,1));
        if size(qECGpeakLoc,1) < maxSize
            qECGpeakLoc = [qECGpeakLoc;num2cell(nan(maxSize - size(qECGpeakLoc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  qECGpeakLoc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'qECGpeakLocInTime');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), qECGpeakLoc, 'qECGpeakLocInTime');
end



%%% string rPeak values in excel file
function [isFoundECG_R]=saveECG_rPeakLoc(colName)
global ecgLocRwave
global SAVE_EXCEL_FILE

rECGpeakLoc = ecgLocRwave(:,1);
rECGpeakLoc = [colName;num2cell(rECGpeakLoc)];
isFoundECG_R = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'rECGpeakLoc');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_R = true;
            
            break
        end
        
    end
    
    if isFoundECG_R == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        maxSize = max(size(existingData,1),size(rECGpeakLoc,1));
        if size(rECGpeakLoc,1) < maxSize
            rECGpeakLoc = [rECGpeakLoc;num2cell(nan(maxSize - size(rECGpeakLoc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  rECGpeakLoc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'rECGpeakLoc');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), rECGpeakLoc, 'rECGpeakLoc');
end



%%% string rPeak values in excel file
function [isFoundECG_R]=saveECG_rPeakLocInTime(colName,frameSize)
global ecgLocRwave
global SAVE_EXCEL_FILE

rECGpeakLoc = ecgLocRwave(:,1);
rECGpeakLoc = [colName;num2cell((rECGpeakLoc/frameSize)*1000)];
isFoundECG_R = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'rECGpeakLocInTime');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_R = true;
            
            break
        end
        
    end
    
    if isFoundECG_R == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        maxSize = max(size(existingData,1),size(rECGpeakLoc,1));
        if size(rECGpeakLoc,1) < maxSize
            rECGpeakLoc = [rECGpeakLoc;num2cell(nan(maxSize - size(rECGpeakLoc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  rECGpeakLoc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'rECGpeakLocInTime');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), rECGpeakLoc, 'rECGpeakLocInTime');
end





%%% string rPeak values in excel file
function [isFoundECG_S]=saveECG_sPeakLoc(colName)
global ecgLocSwave
global SAVE_EXCEL_FILE

sECGpeakLoc = ecgLocSwave(:,1);
sECGpeakLoc = [colName;num2cell(sECGpeakLoc)];
isFoundECG_S = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'sECGpeakLoc');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_S = true;
            
            break
        end
        
    end
    
    if isFoundECG_S == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(sECGpeakLoc,1));
        if size(sECGpeakLoc,1) < maxSize
            sECGpeakLoc = [sECGpeakLoc;num2cell(nan(maxSize - size(sECGpeakLoc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  sECGpeakLoc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'sECGpeakLoc');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), sECGpeakLoc, 'sECGpeakLoc');
end



%%% string rPeak values in excel file
function [isFoundECG_S]=saveECG_sPeakLocInTime(colName,frameSize)
global ecgLocSwave
global SAVE_EXCEL_FILE

sECGpeakLoc = ecgLocSwave(:,1);
sECGpeakLoc = [colName;num2cell((sECGpeakLoc/frameSize)*1000)];
isFoundECG_S = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'sECGpeakLocInTime');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_S = true;
            
            break
        end
        
    end
    
    if isFoundECG_S == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(sECGpeakLoc,1));
        if size(sECGpeakLoc,1) < maxSize
            sECGpeakLoc = [sECGpeakLoc;num2cell(nan(maxSize - size(sECGpeakLoc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  sECGpeakLoc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'sECGpeakLocInTime');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), sECGpeakLoc, 'sECGpeakLocInTime');
end

%%% string rPeak values in excel file
function [isFoundECG_S]=saveECG_tPeakLoc(colName)
global ecgLocTwave
global SAVE_EXCEL_FILE

tECGpeakLoc = ecgLocTwave(:,1);
tECGpeakLoc = [colName;num2cell(tECGpeakLoc)];
isFoundECG_S = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'tECGpeakLoc');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_S = true;
            
            break
        end
        
    end
    
    if isFoundECG_S == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(tECGpeakLoc,1));
        if size(tECGpeakLoc,1) < maxSize
            tECGpeakLoc = [tECGpeakLoc;num2cell(nan(maxSize - size(tECGpeakLoc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  tECGpeakLoc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'tECGpeakLoc');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), tECGpeakLoc, 'tECGpeakLoc');
end








%%% string rPeak values in excel file
function [isFoundECG_S]=saveECG_tPeakLocInTime(colName,frameSize)
global ecgLocTwave
global SAVE_EXCEL_FILE

tECGpeakLoc = ecgLocTwave(:,1);
tECGpeakLoc = [colName;num2cell((tECGpeakLoc/frameSize)*1000)];
isFoundECG_S = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'tECGpeakLocInTime');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_S = true;
            
            break
        end
        
    end
    
    if isFoundECG_S == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(tECGpeakLoc,1));
        if size(tECGpeakLoc,1) < maxSize
            tECGpeakLoc = [tECGpeakLoc;num2cell(nan(maxSize - size(tECGpeakLoc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  tECGpeakLoc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'tECGpeakLocInTime');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), tECGpeakLoc, 'tECGpeakLocInTime');
end
























%%% string rPeak values in excel file



%%% string rPeak values in excel file
function [isFoundECG_Q]=saveECG_qPeakHeight(colName)
global ecgLocQwave
global ecgDataProcessed
global SAVE_EXCEL_FILE

qECGpeakHeight = ecgDataProcessed(ecgLocQwave(:,1),1);
qECGpeakHeight = [colName;num2cell(qECGpeakHeight)];
isFoundECG_Q = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'qECGpeakHeight');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_Q = true;
            
            break
        end
        
    end
    
    if isFoundECG_Q == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(qECGpeakHeight,1));
        if size(qECGpeakHeight,1) < maxSize
            qECGpeakHeight = [qECGpeakHeight;num2cell(nan(maxSize - size(qECGpeakHeight,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  qECGpeakHeight];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'qECGpeakHeight');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), qECGpeakHeight, 'qECGpeakHeight');
end






%%% string rPeak values in excel file
function [isFoundECG_R]=saveECG_rPeakHeight(colName)
global ecgLocRwave
global ecgDataProcessed
global SAVE_EXCEL_FILE

rECGpeakHeight = ecgDataProcessed(ecgLocRwave(:,1),1);
rECGpeakHeight = [colName;num2cell(rECGpeakHeight)];
isFoundECG_R = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'rECGpeakHeight');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_R = true;
            
            break
        end
        
    end
    
    if isFoundECG_R == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(rECGpeakHeight,1));
        if size(rECGpeakHeight,1) < maxSize
            rECGpeakHeight = [rECGpeakHeight;num2cell(nan(maxSize - size(rECGpeakHeight,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  rECGpeakHeight];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'rECGpeakHeight');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), rECGpeakHeight, 'rECGpeakHeight');
end







%%% string rPeak values in excel file
function [isFoundECG_S]=saveECG_sPeakHeight(colName)
global ecgLocSwave
global ecgDataProcessed
global SAVE_EXCEL_FILE

sECGpeakHeight = ecgDataProcessed(ecgLocSwave(:,1),1);
sECGpeakHeight = [colName;num2cell(sECGpeakHeight)];
isFoundECG_S = false;

try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'sECGpeakHeight');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_S = true;
            
            break
        end
        
    end
    
    if isFoundECG_S == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(sECGpeakHeight,1));
        if size(sECGpeakHeight,1) < maxSize
            sECGpeakHeight = [sECGpeakHeight;num2cell(nan(maxSize - size(sECGpeakHeight,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  sECGpeakHeight];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'sECGpeakHeight');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), sECGpeakHeight, 'sECGpeakHeight');
end




%%% string tPeak values in excel file
function [isFoundECG_S]= saveECG_tPeakHeight(colName)
global ecgLocTwave
global ecgDataProcessed
global SAVE_EXCEL_FILE

isFoundECG_S = false;
tECGpeakHeight = ecgDataProcessed(ecgLocTwave(:,1),1);
tECGpeakHeight = [colName;num2cell(tECGpeakHeight)];
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'tECGpeakHeight');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_S = true;
            
            break
        end
        
    end
    
    if isFoundECG_S == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(tECGpeakHeight,1));
        if size(tECGpeakHeight,1) < maxSize
            tECGpeakHeight = [tECGpeakHeight;num2cell(nan(maxSize - size(tECGpeakHeight,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  tECGpeakHeight];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'tECGpeakHeight');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), tECGpeakHeight, 'tECGpeakHeight');
end



%%% string tPeak values in excel file
function [isFound]= saveECG_RTInterval(colName,frameSize)
global ecgLocTwave
global ecgLocRwave

global SAVE_EXCEL_FILE


RT_interval = ((ecgLocTwave - ecgLocRwave)./frameSize)*1000;



isFound = false;
RT_interval = [colName;num2cell(RT_interval)];
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'ECG_RTInterval');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFound = true;
            
            break
        end
        
    end
    
    if isFound == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(RT_interval,1));
        if size(RT_interval,1) < maxSize
            RT_interval = [RT_interval;num2cell(nan(maxSize - size(RT_interval,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  RT_interval];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'ECG_RTInterval');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), RT_interval, 'ECG_RTInterval');
end





%% string tPeak values in excel file
function [isFound]= saveECG_TRInterval(colName,frameSize)
global ecgLocTwave
global ecgLocRwave
global DeletedPeaksCol
global SAVE_EXCEL_FILE




TR_interval = ((ecgLocRwave(2:end,1) - ecgLocTwave(1:end-1,1))./frameSize)*1000;
RRnext_IndexRange = [ecgLocRwave(2:end,1) ecgLocRwave(1:end-1)];

%% check the range where peaks are deleted in range

if size(DeletedPeaksCol,1) > 1
    delIndex = cell2mat(DeletedPeaksCol(:,1:2));
    for i=1:size(delIndex,1)
        startIndex = delIndex(i,1);
        endIndex = delIndex(i,2);
        ind=find(RRnext_IndexRange(:,1)>=startIndex & RRnext_IndexRange(:,2)<=endIndex)
        TR_interval(ind,1)=NaN;
    end
end

isFound = false;
TR_interval = [colName;num2cell(TR_interval)];
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'ECG_TRInterval');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFound = true;
            
            break
        end
        
    end
    
    if isFound == true
      %  msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(TR_interval,1));
        if size(TR_interval,1) < maxSize
            TR_interval = [TR_interval;num2cell(nan(maxSize - size(TR_interval,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  TR_interval];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'ECG_TRInterval');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), TR_interval, 'ECG_TRInterval');
end





%% string tPeak values in excel file
function [isFound]= saveECG_RRInterval(colName,frameSize)

global ecgLocRwave
global DeletedPeaksCol
global SAVE_EXCEL_FILE




RR_interval = ((ecgLocRwave(2:end,1) - ecgLocRwave(1:end-1,1))./frameSize)*1000;
RRnext_IndexRange = [ecgLocRwave(2:end,1) ecgLocRwave(1:end-1)];

%% check the range where peaks are deleted in range

if size(DeletedPeaksCol,1) > 1
    delIndex = cell2mat(DeletedPeaksCol(:,1:2));
    for i=1:size(delIndex,1)
        startIndex = delIndex(i,1);
        endIndex = delIndex(i,2);
        ind=find(RRnext_IndexRange(:,1)>=startIndex & RRnext_IndexRange(:,2)<=endIndex)
        RR_interval(ind,1)=NaN;
    end
end

isFound = false;
RR_interval = [colName;num2cell(RR_interval)];
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'ECG_RRInterval');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFound = true;
            
            break
        end
        
    end
    
    if isFound == true
     %   msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(RR_interval,1));
        if size(RR_interval,1) < maxSize
            RR_interval = [RR_interval;num2cell(nan(maxSize - size(RR_interval,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  RR_interval];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'ECG_RRInterval');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), RR_interval, 'ECG_RRInterval');
end






%%% string rPeak values in excel file
function [isFoundBVP1_F]= saveBVP1_FPeakLoc(colName)
global bvpLocQwave
global SAVE_EXCEL_FILE

footBVP1Loc = bvpLocQwave(:,1);
footBVP1Loc = [colName;num2cell(footBVP1Loc)];
%% check if the file information is already in the excel file or not
isFoundBVP1_F = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'footBVP1Loc');
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP1_F = true;
            
            break
        end
        
    end
    
    if isFoundBVP1_F == true
        %msgbox('BVP1 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(footBVP1Loc,1));
        if size(footBVP1Loc,1) < maxSize
            footBVP1Loc = [footBVP1Loc;num2cell(nan(maxSize - size(footBVP1Loc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  footBVP1Loc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'footBVP1Loc');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), footBVP1Loc, 'footBVP1Loc');
end






%%% string rPeak values in excel file
function [isFoundBVP1_P]=saveBVP1_PPeakLoc(colName)
global bvpLocRwave
global SAVE_EXCEL_FILE

peakBVP1Loc = bvpLocRwave(:,1);
peakBVP1Loc = [colName;num2cell(peakBVP1Loc)];
%% check if the file information is already in the excel file or not
isFoundBVP1_P = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'peakBVP1Loc');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP1_P = true;
            
            break
        end
        
    end
    
    if isFoundBVP1_P == true
        %msgbox('BVP1 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(peakBVP1Loc,1));
        if size(peakBVP1Loc,1) < maxSize
            peakBVP1Loc = [peakBVP1Loc;num2cell(nan(maxSize - size(peakBVP1Loc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  peakBVP1Loc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'peakBVP1Loc');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), peakBVP1Loc, 'peakBVP1Loc');
end







%%% string rPeak values in excel file
function [isFoundBVP1_E]=saveBVP1_EPeakLoc(colName)
global bvpLocSwave
global SAVE_EXCEL_FILE

endBVP1Loc = bvpLocSwave(:,1);
endBVP1Loc = [colName;num2cell(endBVP1Loc)];
%% check if the file information is already in the excel file or not
isFoundBVP1_E = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'endBVP1Loc');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP1_E = true;
            
            break
        end
        
    end
    
    if isFoundBVP1_E == true
        %msgbox('BVP1 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(endBVP1Loc,1));
        if size(endBVP1Loc,1) < maxSize
            endBVP1Loc = [endBVP1Loc;num2cell(nan(maxSize - size(endBVP1Loc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  endBVP1Loc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'endBVP1Loc');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), endBVP1Loc, 'endBVP1Loc');
end




%%% string rPeak values in excel file
function [isFoundBVP1_F]=saveBVP1_FPeakHeight(colName)
global bvpLocQwave
global bvpDataProcessed
global SAVE_EXCEL_FILE

footBVP1Height = bvpDataProcessed(bvpLocQwave(:,1),1);
footBVP1Height = [colName;num2cell(footBVP1Height)];
%% check if the file information is already in the excel file or not
isFoundBVP1_F = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'footBVP1Height');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP1_F = true;
            
            break
        end
        
    end
    
    if isFoundBVP1_F == true
        %msgbox('BVP1 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(footBVP1Height,1));
        if size(footBVP1Height,1) < maxSize
            footBVP1Height = [footBVP1Height;num2cell(nan(maxSize - size(footBVP1Height,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  footBVP1Height];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'footBVP1Height');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), footBVP1Height, 'footBVP1Height');
end






%%% string rPeak values in excel file
function [isFoundBVP1_P]=saveBVP1_PPeakHeight(colName)
global bvpLocRwave
global bvpDataProcessed
global SAVE_EXCEL_FILE

peakBVP1Height = bvpDataProcessed(bvpLocRwave(:,1),1);
peakBVP1Height = [colName;num2cell(peakBVP1Height)];
%% check if the file information is already in the excel file or not
isFoundBVP1_P = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'peakBVP1Height');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP1_P = true;
            
            break
        end
        
    end
    
    if isFoundBVP1_P == true
        %msgbox('BVP1 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(peakBVP1Height,1));
        if size(peakBVP1Height,1) < maxSize
            peakBVP1Height = [peakBVP1Height;num2cell(nan(maxSize - size(peakBVP1Height,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  peakBVP1Height];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'peakBVP1Height');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), peakBVP1Height, 'peakBVP1Height');
end







%%% string rPeak values in excel file
function [isFoundBVP1_E]=saveBVP1_EPeakHeight(colName)
global bvpLocSwave
global bvpDataProcessed
global SAVE_EXCEL_FILE

endBVP1Height = bvpDataProcessed(bvpLocSwave(:,1),1);
endBVP1Height = [colName;num2cell(endBVP1Height)];
%% check if the file information is already in the excel file or not
isFoundBVP1_E = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'endBVP1Height');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP1_E = true;
            
            break
        end
        
    end
    
    if isFoundBVP1_E == true
        %msgbox('BVP1 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(endBVP1Height,1));
        if size(endBVP1Height,1) < maxSize
            endBVP1Height = [endBVP1Height;num2cell(nan(maxSize - size(endBVP1Height,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  endBVP1Height];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'endBVP1Height');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), endBVP1Height, 'endBVP1Height');
end











%%% string rPeak values in excel file
function [isFoundBVP2_F]=saveBVP2_FPeakHeight(colName)
global bvpLocQwave2
global bvpDataProcessed2
global SAVE_EXCEL_FILE

footBVP2Height = bvpDataProcessed2(bvpLocQwave2(:,1),1);
footBVP2Height = [colName;num2cell(footBVP2Height)];
%% check if the file information is already in the excel file or not
isFoundBVP2_F = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'footBVP2Height');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP2_F = true;
            
            break
        end
        
    end
    
    if isFoundBVP2_F == true
        %msgbox('BVP2 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(footBVP2Height,1));
        if size(footBVP2Height,1) < maxSize
            footBVP2Height = [footBVP2Height;num2cell(nan(maxSize - size(footBVP2Height,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  footBVP2Height];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'footBVP2Height');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), footBVP2Height, 'footBVP2Height');
end






%%% string rPeak values in excel file
function [isFoundBVP2_P]= saveBVP2_PPeakHeight(colName)
global bvpLocRwave2
global bvpDataProcessed2
global SAVE_EXCEL_FILE

peakBVP2Height = bvpDataProcessed2(bvpLocRwave2(:,1),1);
peakBVP2Height = [colName;num2cell(peakBVP2Height)];
%% check if the file information is already in the excel file or not
isFoundBVP2_P = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'peakBVP2Height');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP2_P = true;
            
            break
        end
        
    end
    
    if isFoundBVP2_P == true
        %msgbox('BVP2 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(peakBVP2Height,1));
        if size(peakBVP2Height,1) < maxSize
            peakBVP2Height = [peakBVP2Height;num2cell(nan(maxSize - size(peakBVP2Height,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  peakBVP2Height];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'peakBVP2Height');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), peakBVP2Height, 'peakBVP2Height');
end







%%% string rPeak values in excel file
function [isFoundBVP2_E]=saveBVP2_EPeakHeight(colName)
global bvpLocSwave2
global bvpDataProcessed2
global SAVE_EXCEL_FILE

endBVP2Height = bvpDataProcessed2(bvpLocSwave2(:,1),1);
endBVP2Height = [colName;num2cell(endBVP2Height)];
%% check if the file information is already in the excel file or not
isFoundBVP2_E = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'endBVP2Height');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP2_E = true;
            
            break
        end
        
    end
    
    if isFoundBVP2_E == true
        %msgbox('BVP2 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(endBVP2Height,1));
        if size(endBVP2Height,1) < maxSize
            endBVP2Height = [endBVP2Height;num2cell(nan(maxSize - size(endBVP2Height,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  endBVP2Height];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'endBVP2Height');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), endBVP2Height, 'endBVP2Height');
end


%%% string rPeak values in excel file
function [isFoundBVP2_F]=saveBVP2_FPeakLoc(colName)
global bvpLocQwave2
global SAVE_EXCEL_FILE

footBVP2Loc = bvpLocQwave2(:,1);
footBVP2Loc = [colName;num2cell(footBVP2Loc)];
%% check if the file information is already in the excel file or not
isFoundBVP2_F = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'footBVP2Loc');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP2_F = true;
            
            break
        end
        
    end
    
    if isFoundBVP2_F == true
        %msgbox('BVP2 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(footBVP2Loc,1));
        if size(footBVP2Loc,1) < maxSize
            footBVP2Loc = [footBVP2Loc;num2cell(nan(maxSize - size(footBVP2Loc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  footBVP2Loc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'footBVP2Loc');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), footBVP2Loc, 'footBVP2Loc');
end




%%% string rPeak values in excel file
function [isFoundBVP2_P]=saveBVP2_PPeakLoc(colName)
global bvpLocRwave2
global SAVE_EXCEL_FILE

peakBVP2Loc = bvpLocRwave2(:,1);
peakBVP2Loc = [colName;num2cell(peakBVP2Loc)];
%% check if the file information is already in the excel file or not
isFoundBVP2_P = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'peakBVP2Loc');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP2_P = true;
            
            break
        end
        
    end
    
    if isFoundBVP2_P == true
        %msgbox('BVP2 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(peakBVP2Loc,1));
        if size(peakBVP2Loc,1) < maxSize
            peakBVP2Loc = [peakBVP2Loc;num2cell(nan(maxSize - size(peakBVP2Loc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  peakBVP2Loc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'peakBVP2Loc');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), peakBVP2Loc, 'peakBVP2Loc');
end







%%% string rPeak values in excel file
function [isFoundBVP2_E]=saveBVP2_EPeakLoc(colName)
global bvpLocSwave2
global SAVE_EXCEL_FILE

endBVP2Loc = bvpLocSwave2(:,1);
endBVP2Loc = [colName;num2cell(endBVP2Loc)];
%% check if the file information is already in the excel file or not
isFoundBVP2_E = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'endBVP2Loc');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP2_E = true;
            break
        end
    end
    if isFoundBVP2_E == true
        %msgbox('BVP2 R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(endBVP2Loc,1));
        if size(endBVP2Loc,1) < maxSize
            endBVP2Loc = [endBVP2Loc;num2cell(nan(maxSize - size(endBVP2Loc,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  endBVP2Loc];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'endBVP2Loc');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), endBVP2Loc, 'endBVP2Loc');
end

















































%%%%%%%%%%%%%%%%%%%%%%%%%%  ECG DELETE Index
function [isFoundECG_R]=saveECGDelIndex(colName)
global ecgLocRwave
global SAVE_EXCEL_FILE

ecgDeleteFlag = ecgLocRwave(:,2);
ecgDeleteFlag = [colName;num2cell(ecgDeleteFlag)];
%% check if the file information is already in the excel file or not
isFoundECG_R = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'ECG_FLAG');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_R = true;
            break
        end
    end
    
    if isFoundECG_R == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        numberOfRow = size(existingData,1);
        maxSize = max(size(existingData,1),size(ecgDeleteFlag,1));
        if size(ecgDeleteFlag,1) < maxSize
            ecgDeleteFlag = [ecgDeleteFlag;num2cell(nan(maxSize - size(ecgDeleteFlag,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  ecgDeleteFlag];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'ECG_FLAG');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), ecgDeleteFlag, 'ECG_FLAG');
end


%%%%%%%%%%%%%%%%%%%%%%%%%%  BVP1 DELETE Indexal

function [isFoundECG_R]=saveBVP1DelIndex(colName)
global bvpLocRwave
global SAVE_EXCEL_FILE

bvp1DeleteFlag = bvpLocRwave(:,2);
bvp1DeleteFlag = [colName;num2cell(bvp1DeleteFlag)];
%% check if the file information is already in the excel file or not
isFoundECG_R = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'BVP1_FLAG');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_R = true;
            break
        end
    end
    
    if isFoundECG_R == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(bvp1DeleteFlag,1));
        if size(bvp1DeleteFlag,1) < maxSize
            bvp1DeleteFlag = [bvp1DeleteFlag;num2cell(nan(maxSize - size(bvp1DeleteFlag,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  bvp1DeleteFlag];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'BVP1_FLAG');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), bvp1DeleteFlag, 'BVP1_FLAG');
end



function [isFoundECG_R]=saveBVP2DelIndex(colName)

%%%%%%%%%%%%%%%%%%%%%%%%%%  bvp2 DELETE Index
global bvpLocRwave2
global SAVE_EXCEL_FILE

bvp2DeleteFlag = bvpLocRwave2(:,2);
bvp2DeleteFlag = [colName;num2cell(bvp2DeleteFlag)];
%% check if the file information is already in the excel file or not
isFoundECG_R = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'BVP2_FLAG');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_R = true;
            break
        end
    end
    
    if isFoundECG_R == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(bvp2DeleteFlag,1));
        if size(bvp2DeleteFlag,1) < maxSize
            bvp2DeleteFlag = [bvp2DeleteFlag;num2cell(nan(maxSize - size(bvp2DeleteFlag,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  bvp2DeleteFlag];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'bvp2_FLAG');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), bvp2DeleteFlag, 'BVP2_FLAG');
end





function [isFoundBVP1_Delay]=saveECG_BVP1Delay(colName)

global bvpLocQwave
global ecgLocRwave
global SAVE_EXCEL_FILE

minPeaks = min(size(bvpLocQwave,1),size(ecgLocRwave,1));
ecgBvp1Delay = ecgLocRwave(1:minPeaks,1)-bvpLocQwave(1:minPeaks,1);
ecgBvp1Delay = [colName;num2cell(ecgBvp1Delay)];
%% check if the file information is already in the excel file or not
isFoundBVP1_Delay = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'ecgBvp1Delay');
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP1_Delay = true;
            
            break
        end
        
    end
    
    if isFoundBVP1_Delay == true
        % msgbox('ECG BVP 1 delay records for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(ecgBvp1Delay,1));
        if size(ecgBvp1Delay,1) < maxSize
            ecgBvp1Delay = [ecgBvp1Delay;num2cell(nan(maxSize - size(ecgBvp1Delay,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  ecgBvp1Delay];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'ecgBvp1Delay');
        %msgbox('ECG BVP1 delay Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), ecgBvp1Delay, 'ecgBvp1Delay');
end





function [isFoundBVP2_Delay]=saveECG_BVP2Delay(colName)

global bvpLocQwave2
global ecgLocRwave
global SAVE_EXCEL_FILE

minPeaks = min(size(bvpLocQwave2,1),size(ecgLocRwave,1));
ecgBvp2Delay = ecgLocRwave(1:minPeaks,1)-bvpLocQwave2(1:minPeaks,1);
ecgBvp2Delay = [colName;num2cell(ecgBvp2Delay)];
%% check if the file information is already in the excel file or not
isFoundBVP2_Delay = false;
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'ecgBvp2Delay');
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundBVP2_Delay = true;
            break
        end
    end
    
    if isFoundBVP2_Delay == true
        % msgbox('ECG BVP 2 delay records for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(ecgBvp2Delay,1));
        if size(ecgBvp2Delay,1) < maxSize
            ecgBvp2Delay = [ecgBvp2Delay;num2cell(nan(maxSize - size(ecgBvp2Delay,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  ecgBvp2Delay];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'ecgBvp2Delay');
        %msgbox('ECG BVP2 delay Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), ecgBvp2Delay, 'ecgBvp2Delay');
end


%%% RS Height
function [isFoundECG_S]=saveECG_RSHeight(colName)
global ecgLocRwave
global ecgLocSwave
global ecgDataProcessed
global SAVE_EXCEL_FILE

isFoundECG_S = false;
ECG_RS_Height = ecgDataProcessed(ecgLocRwave(:,1),1) -  ecgDataProcessed(ecgLocSwave(:,1),1);
ECG_RS_Height = [colName;num2cell(ECG_RS_Height)];
try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'ECG_RS_Height');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_S = true;
            
            break
        end
        
    end
    
    if isFoundECG_S == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(ECG_RS_Height,1));
        if size(ECG_RS_Height,1) < maxSize
            ECG_RS_Height = [ECG_RS_Height;num2cell(nan(maxSize - size(ECG_RS_Height,1),1))];
        elseif size(existingData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  ECG_RS_Height];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'ECG_RS_Height');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), ECG_RS_Height, 'ECG_RS_Height');
end






%% saving ECG Processed Data
function [isFoundECG_S]=saveDataECGProcessed(colName)

global ecgDataProcessed
global SAVE_EXCEL_FILE

tmpECGData = ecgDataProcessed;
tmpECGData = [colName;num2cell(tmpECGData)];

isFoundECG_S = false;

try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'ECG_Processed');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_S = true;
            
            break
        end
        
    end
    
    if isFoundECG_S == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(tmpECGData,1));
        if size(tmpECGData,1) < maxSize
            tmpECGData = [tmpECGData;num2cell(nan(maxSize - size(tmpECGData,1),1))];
        elseif size(tmpECGData,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  tmpECGData];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'ECG_Processed');
        %close(file)
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), tmpECGData, 'ECG_Processed');
end




%% saving BVP1 Processed Data
function [isFoundECG_S]=saveDataBVP1Processed(colName)

global bvpDataProcessed
global SAVE_EXCEL_FILE

tmpBVP1Data =  bvpDataProcessed;
tmpBVP1Data = [colName;num2cell(tmpBVP1Data)];

isFoundECG_S = false;

try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'BVP1_Processed');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_S = true;
            
            break
        end
        
    end
    
    if isFoundECG_S == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(tmpBVP1Data,1));
        if size(tmpBVP1Data,1) < maxSize
            tmpBVP1Data = [tmpBVP1Data;num2cell(nan(maxSize - size(tmpBVP1Data,1),1))];
        elseif size(tmpBVP1Data,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  tmpBVP1Data];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'BVP1_Processed');
        %close(file);
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), tmpBVP1Data, 'BVP1_Processed');
end


%% saving BVP2 Processed Data
function [isFoundECG_S]=saveDataBVP2Processed(colName)

global bvpDataProcessed2
global SAVE_EXCEL_FILE

tmpBVP2Data =  bvpDataProcessed2;
tmpBVP2Data = [colName;num2cell(tmpBVP2Data)];

isFoundECG_S = false;

try
    [onlyData,allFileHeader, existingData] = xlsread(strcat(SAVE_EXCEL_FILE,'.xlsx'),'BVP2_Processed');
    %% check if the file information is already in the excel file or not
    
    for hid = 1:size(allFileHeader,2)
        if strcmp(allFileHeader{hid},colName)
            isFoundECG_S = true;
            
            break
        end
        
    end
    
    if isFoundECG_S == true
        %msgbox('ECG R peaks for the file is already found in the excel file', 'Error','error');
    else
        
        
        
        maxSize = max(size(existingData,1),size(tmpBVP2Data,1));
        if size(tmpBVP2Data,1) < maxSize
            tmpBVP2Data = [tmpBVP2Data;num2cell(nan(maxSize - size(tmpBVP2Data,1),1))];
        elseif size(tmpBVP2Data,1) < maxSize
            existingData=[existingData; num2cell( nan(maxSize - size(existingData,1),size(existingData,2)))];
        end
        newData = [existingData,  tmpBVP2Data];
        xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), newData, 'BVP2_Processed');
        %close(file);
        %msgbox('R Peak Info Added in Excel');
    end
catch
    xlswrite(strcat(SAVE_EXCEL_FILE,'.xlsx'), tmpBVP2Data, 'BVP2_Processed');
end




% --- Executes on button press in pbLoadPrevFile.
function pbLoadPrevFile_Callback(hObject, eventdata, handles)
% hObject    handle to pbLoadPrevFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global StartDisplayFrame



clearPlots()

% if StartDisplayFrame >= str2num(get(handles.editDWindowSize,'String'))
%     StartDisplayFrame = StartDisplayFrame - str2num(get(handles.editDWindowSize,'String'))+1 ;
%     
% else
%     StartDisplayFrame = str2num(get(handles.editStartFrame,'String'));
%     
% end

StartDisplayFrame = StartDisplayFrame -str2num(get(handles.editStepsMove,'String'));
StartDisplayFrame = max(StartDisplayFrame,1);
displaySignal(handles);
clearDeletedPeak();
set(handles.pbAddPeak,'enable','off')
displaySignal(handles)



% --- Executes on button press in pbDisplayNext.
function pbLoadNextFile_Callback(hObject, eventdata, handles)
% hObject    handle to pbDisplayNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)





function bvpMinPeakHeight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editSignalLoc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editSignalLoc as text
%        str2double(get(hObject,'String')) returns contents of editSignalLoc as a double



function editSignalLoc_Callback(hObject, eventdata, handles)
% hObject    handle to editSignalLoc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editSignalLoc as text
%        str2double(get(hObject,'String')) returns contents of editSignalLoc as a double


% --- Executes during object creation, after setting all properties.
function editSignalLoc_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editSignalLoc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit24_Callback(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit24 as text
%        str2double(get(hObject,'String')) returns contents of edit24 as a double


% --- Executes during object creation, after setting all properties.
function edit24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editDWindowSize_Callback(hObject, eventdata, handles)
% hObject    handle to editDWindowSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editDWindowSize as text
%        str2double(get(hObject,'String')) returns contents of editDWindowSize as a double


% --- Executes during object creation, after setting all properties.
function editDWindowSize_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDWindowSize (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function clearBVPData()

bvpData=medfilt1(bvpDataRaw,bvpMedianFilter_FN);
bvpDataWDEN = wden(bvpData,'sqtwolog','s','mln',4,'sym4');  %Mexican Hat Wavelet: mexh

%%% bvpData wondering
bvpBaseLineWF = FRAMING_SIZE/2;
kk = medfilt1(bvpDataWDEN,bvpBaseLineWF);
bvpBLWander=bvpDataWDEN-kk;

%%smoothing BVP signals using mean.....
bvpSmoothingCoff = 50;
bvpDataProcessed = bvpBLWander;
for i=1:size(bvpBLWander,1)
    if i<size(bvpBLWander,1)-bvpSmoothingCoff && i>bvpSmoothingCoff
        bvpDataProcessed(i,1)=mean(bvpBLWander(i-bvpSmoothingCoff:i+bvpSmoothingCoff,1));
    end
end





function viewRawData(handles)

global DataRaw
global fileDir;
clearPlots();
global displayMatrix
global ax1
global ax2
global ax3

%%[1. rowECG 2. processedECG 3.rowBVP1 4.processedBVP1 5.rowBVP2 6.processedBVP2 7.detailECG 8.detailBVP1 9.detailBVP2]
%displayMatrix = [1 0 1 0 1 0];
displayMatrix=[get(handles.chkDataECG,'Value') 0 get(handles.chkDataBVP1,'Value') 0 get(handles.chkDataBVP2,'Value') 0];

allFiles = get(handles.pMenuSignalFiles,'String');
selectedIndex = get(handles.pMenuSignalFiles,'Value');
fileName = allFiles{selectedIndex};

tDisplay= sum(displayMatrix);

DataRaw = load(strcat(fileDir,'/',fileName));

%DataRaw = load(strcat('P6-BioTrace-Mat-Cut\',files(fileID).name));
try
    DataRaw = struct2cell(DataRaw);
    DataRaw=DataRaw{1,1};
catch
end
if size(DataRaw,1)<size(DataRaw,2)
    DataRaw = DataRaw';
end
%% make sure all ECG BVB and BVP 2 records are present

DataRaw(sum(isnan(DataRaw),2)>0,:)=[];

%% only for txt file
%% Deepak remove later on

% if get(handles.chkDataECG,'Value') ==1 & get(handles.chkDataBVP1,'Value')==0
%     DataRaw = DataRaw *1000;
% elseif get(handles.chkDataECG,'Value') ==0 & get(handles.chkDataBVP1,'Value')==1
%         DataRaw=DataRaw';
% end

%set(handles.axes1,'Units','Pixels','Position',getpixelposition(handles.axes1)-[FIGURE_FRAME_X,0,FIGURE_FRAME_Y,0])

%DataRaw = DataRaw(1:5000,:);
currPlotIndex=1;
gap = ceil(size(DataRaw,1)/30);
if get(handles.chkDataECG,'Value')==1
    ax1=subplot(tDisplay,1,currPlotIndex);
    plot(DataRaw(:,currPlotIndex));
    axis([1,size(DataRaw,currPlotIndex),min(DataRaw(:,currPlotIndex)),max(DataRaw(:,currPlotIndex))]);
    %gap = ceil(size(DataRaw,1)/30);
    set(gca,'XTick',[1:gap:size(DataRaw,1)])
    currPlotIndex=currPlotIndex+1;
end




%set(h1,'Units','Pixels','Position',getpixelposition(h1)-[0,0,FIGURE_FRAME_Y,0])
if get(handles.chkDataBVP1,'Value')==1
    ax2=subplot(tDisplay,1,currPlotIndex);
    plot(DataRaw(:,currPlotIndex));
    try
        axis([1,size(DataRaw,currPlotIndex),min(DataRaw(:,currPlotIndex)),max(DataRaw(:,currPlotIndex))]);
        set(h2,'Units','Pixels','Position',getpixelposition(h2)-[0,0,FIGURE_FRAME_Y,0])
        set(gca,'XTick',[1:gap:size(DataRaw,1)]);
    catch
    end
    
    currPlotIndex = currPlotIndex+1;
end

if get(handles.chkDataBVP2,'Value')==1
    ax3=subplot(tDisplay,1,currPlotIndex);
    plot(DataRaw(:,currPlotIndex));
    try
        axis([1,size(DataRaw,currPlotIndex),min(DataRaw(:,currPlotIndex)),max(DataRaw(:,currPlotIndex))]);
    catch
    end
    set(gca,'XTick',[1:gap:size(DataRaw,1)]);
    %set(h3,'Units','Pixels','Position',getpixelposition(h3)-[0,0,FIGURE_FRAME_Y,0])
    %subplot(2, 2, 1);
    %plot(1:10, 'bo-');
    %title('subplot(2, 2, 1)');
end



function setDisplayColor(handles)
global displayMatrix

%%[1. rowECG 2. processedECG 3.rowBVP1 4.processedBVP1 5.rowBVP2 6.processedBVP2 7.detailECG 8.detailBVP1 9.detailBVP2]
%displayMatrix

if displayMatrix(1,1)==1
    
    set(handles.pbDisplayRawECG,'BackgroundColor',[0.39,0.83,0.07])
else
    set(handles.pbDisplayRawECG,'BackgroundColor',[0.94,0.6,0.46])
end

if displayMatrix(1,2)==1
    
    set(handles.pbDisplayProcessECG,'BackgroundColor',[0.39,0.83,0.07])
else
    set(handles.pbDisplayProcessECG,'BackgroundColor',[0.94,0.6,0.46])
end
if displayMatrix(1,3)==1
    
    set(handles.pbDisplayRawBVP1,'BackgroundColor',[0.39,0.83,0.07])
else
    set(handles.pbDisplayRawBVP1,'BackgroundColor',[0.94,0.6,0.46])
end
if displayMatrix(1,4)==1
    
    set(handles.pbDisplayProcessBVP1,'BackgroundColor',[0.39,0.83,0.07])
else
    set(handles.pbDisplayProcessBVP1,'BackgroundColor',[0.94,0.6,0.46])
end
if displayMatrix(1,5)==1
    
    set(handles.pbDisplayRawBVP2,'BackgroundColor',[0.39,0.83,0.07])
else
    set(handles.pbDisplayRawBVP2,'BackgroundColor',[0.94,0.6,0.46])
end
if displayMatrix(1,6)==1
    
    set(handles.pbDisplayProcessBVP2,'BackgroundColor',[0.39,0.83,0.07])
else
    set(handles.pbDisplayProcessBVP2,'BackgroundColor',[0.94,0.6,0.46])
end



% if displayMatrix(1,7)==1
%     set(handles.pbECGViewChange,'BackgroundColor',[0.39,0.83,0.07])
% else
%     set(handles.pbECGViewChange,'BackgroundColor',[0.94,0.6,0.46])
% end
% if displayMatrix(1,8)==1
%
%     set(handles.pbBVPViewChange,'BackgroundColor',[0.39,0.83,0.07])
% else
%     set(handles.pbBVPViewChange,'BackgroundColor',[0.94,0.6,0.46])
% end
% if displayMatrix(1,9)==1
%
%     set(handles.pbBVP1ViewChange,'BackgroundColor',[0.39,0.83,0.07])
% else
%     set(handles.pbBVP1ViewChange,'BackgroundColor',[0.94,0.6,0.46])
% end




% --- Executes on button press in pbViewOrgSignal.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pbViewOrgSignal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pbProcessSignals.
function pbProcessSignals_Callback(hObject, eventdata, handles)
% hObject    handle to pbProcessSignals (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


global displayMatrix
global chkRange
%global DeletedSignals
global prevDisplayMatrix
global FLAG_PREV_DISPLAY

chkRange = false;
DeletedSignals=[];

%%h = waitbar(0,'Please wait...');

clearPlots();
%set(findall(handles.uiPanelMain, '-property', 'enable'), 'enable', 'off')


loadDataForProcessing(handles);

%%h = waitbar(0,'Please wait...');
%%waitbar(1/4,h,'Filtering ECG Data...');
if get(handles.chkDataECG,'Value')==1
    filterECGData(handles);
end
%%h = waitbar(0,'Please wait...');
%%waitbar(1/4,h,'Filtering ECG Data...');
%%waitbar(2/4,h,'Filtering BVP1 Data...');
if get(handles.chkDataBVP1,'Value')==1
    filterBVPData(handles);
end
%%h = waitbar(0,'Please wait...');
%%waitbar(1/4,h,'Filtering ECG Data...');
%%waitbar(2/4,h,'Filtering BVP1 Data...');
%%waitbar(3/4,h,'Filtering BVP2 Data...');
if get(handles.chkDataBVP2,'Value')==1
	filterBVPData1(handles);
end

%% reset display matrix to previous setting
displayMatrix =[0 get(handles.chkDataECG,'Value') 0 get(handles.chkDataBVP1,'Value') 0 get(handles.chkDataBVP2,'Value') ];
% %% if there is same setting from previous display move to previous display
% if sum(prevDisplayMatrix)>0 & FLAG_PREV_DISPLAY == true
%     displayMatrix = prevDisplayMatrix;
%     %% off the previus display flag
%     FLAG_PREV_DISPLAY = false;
% end
%displayMatrix =[0 1 0 1 0 1 ];
setDisplayColor(handles);
%displaySignal(handles);
enabledDataDisplay(handles);
%set(findall(handles.uiPanelMain, '-property', 'enable'), 'enable', 'on')
%%h = waitbar(0,'Please wait...');
%%waitbar(1/4,h,'Filtering ECG Data...');
%%waitbar(2/4,h,'Filtering BVP1 Data...');
%%waitbar(3/4,h,'Filtering BVP2 Data...');
%%%close(h);
displaySignal(handles)
%refreshdata
%drawnow

function loadDataForProcessing(handles)
global DataRaw
global dataCuttOff

%% remove this line after whole processing second version (except first 54 files as DataRaw is not saved in it.
if isempty(DataRaw)
    DataRaw = dataCuttOff;
end

try
    startFrame = str2num(get(handles.editCuttOffFront,'String'))+1;
    dataCuttOff = DataRaw(startFrame:end-str2num(get(handles.editCuttOffBack ,'String')),:);
catch
    dataCuttOff=DataRaw;
end

% %TEST_CASE
% try
%     dataCuttOff = dataCuttOff (1:10000,:);
% catch
% end
% DataRaw = DataRaw (1:10000,:);




function clearAllData(handles)

global dataCuttOff
global DataRaw



global ecgLocRwave
global ecgLocQwave
global ecgLocTwave

global ecgDataProcessed
global bvpDataProcessed
global bvpDataProcessed2

global bvpLocRwave
global bvpLocSwave
global bvpLocQwave


global bvpLocRwave2
global bvpLocSwave2
global bvpLocQwave2

global deletedRangeInfo


%global DeletedSignals

global DeletedECGWave
global DeletedBVPWave
global DeletedBVPWave2
global DeletedPeaksCol


global chkPeakFrame

DataRaw = [];
dataCuttOff = [];

ecgLocRwave= [];
ecgLocQwave=[];
ecgLocTwave=[];



ecgDataProcessed=[];
bvpDataProcessed=[];
bvpDataProcessed2=[];

bvpLocRwave = [];
bvpLocSwave = [];
bvpLocQwave = [];


bvpLocRwave2 = [];
bvpLocSwave2 = [];
bvpLocQwave2 = [];

DeletedPeaksCol ={};

%DeletedSignals=[];

DeletedECGWave=[];
DeletedBVPWave=[];
DeletedBVPWave2=[];

deletedRangeInfo={};


chkPeakFrame = false;

set(handles.pbAddPeak,'enable','off');

try
    close axis1
catch
end

try

uitablehandle = findall(handles.uiPDataTable,'type','uitable');
set(uitablehandle,'Data',[])
catch
    x=1;
end

try
    uitablehandleBVP = findall(handles.uiPDataTableBVP,'type','uitable');
    set(uitablehandleBVP,'Data',[])
catch
    x=1;
end
try
    uitablehandleBVP1 = findall(handles.uiPDataTableBVP1,'type','uitable');
    set(uitablehandleBVP1,'Data',[])
catch
    x=1;
end
enabledDataDisplay(handles)




function enabledDataDisplay(handles)
global displayMatrix

if displayMatrix(1,2)==1
    set(handles.pbDisplayProcessECG,'Enable','on')
    set(handles.pbECGViewChange,'Enable','on')
else
    set(handles.pbDisplayProcessECG,'Enable','off')
    set(handles.pbECGViewChange,'Enable','off')
end

if displayMatrix(1,4)==1
    set(handles.pbDisplayProcessBVP1,'Enable','on')
    set(handles.pbBVPViewChange,'Enable','on')
else
    set(handles.pbDisplayProcessBVP1,'Enable','off')
    set(handles.pbBVPViewChange,'Enable','off')
end

if displayMatrix(1,6)==1
    set(handles.pbDisplayProcessBVP2,'Enable','on')
    set(handles.pbBVP1ViewChange,'Enable','on')
else
    set(handles.pbDisplayProcessBVP2,'Enable','off')
    set(handles.pbBVP1ViewChange,'Enable','off')
end


		
function filterECGData(handles)	
	global dataCuttOff	
		
		
	global ecgLocRwave	
	global ecgLocSwave	
	global ecgLocQwave	
	global ecgLocTwave	
		
	global StartDisplayFrame;	
		
	global ecgDataProcessed;	
		
		
		
		
%	set(findall(handles.uiPanelMain, '-property', 'enable'), 'enable', 'off')	
		
		
		
	%h = waitbar(0,'ECG Filter ......');	
		
	%%waitbar(2/4,h,'Filtering BVP1 Data...');	
	%%waitbar(3/4,h,'Filtering BVP2 Data...');	
	%%%close(h);	
		
		
		
	%save(tmpFileName,'dataCuttOff','ecgDataProcessed','bvpDataProcessed','bvpDataProcessed2','ecgLocRwave','ecgLocSwave','ecgLocQwave','ecgLocTwave','bvpLocRwave','bvpLocSwave','bvpLocQwave','bvpLocRwave2','bvpLocSwave2','bvpLocQwave2','displayMatrix','allParam','deletedRangeInfo','DeletedPeaksCol');	
	ecgDataRaw= dataCuttOff(:,1);	
		
	FRAMING_SIZE=str2num(get(handles.editFrameSize ,'String'));	
	StartDisplayFrame = 1;	
	%EndDisplayFrame=  StartDisplayFrame+ str2num(get(handles.editDWindowSize ,'String'));	
		
		
	%%waitbar(1/10,h,'Applying Median Filter');	
	ecgMedianF_FN =str2num(get(handles.ecgMedianFilter_FN ,'String'));	
		
		
		
		
		
	%ecgBaseWonderingFrame =str2num(get(handles.editBaseWFrame  ,'String'));	
	ecgRPeakHeight  = str2num(get(handles.ecgRPeakHeight ,'String'));	
	ecgMinPeackDist =  str2num(get(handles.ecgMinPeakD ,'String'));	
		
		
	ecgRSDist  =str2num(get(handles.ecgRSDist  ,'String'));	
	ecgQRDist   =str2num(get(handles.ecgQRDist ,'String'));	
	ecgSTDistInPer =  str2num(get(handles.ecgSTDist  ,'String'));	
		
	ecgSTDistMin = str2num(get(handles.ecgSTDistMin  ,'String'));	
		
		
	invertECGData = ecgDataRaw;	
		
	if get(handles.chkInvertECG,'Value')==1	
	    invertECGData = invertECGData*-1;	
	end	
		
		
	ecgMedianF_FN = str2num(get(handles.ecgMedianFilter_FN,'String'));	
	%EndDisplayFrame = size(ecgDataRaw,1);	
	%lowPassBanF=20; %% in default (from tony)	
	lowPassBandF = str2num(get(handles.ecgLowPassBandF,'String'));	
	lowPassBandR = str2num(get(handles.ecgLowPassBandR,'String'));	
	ecgTonyFilter = invertECGData;	
	%% Lowpass IIR Filter - Mat	
	if get(handles.chkLowPassIIRFilter,'Value')==1	
	    %waitbar(1/10,h,'ECG Filter: Applying Low Pass IRR Filter');	
	    lpFilt = designfilt('lowpassiir','FilterOrder',3,'PassbandFrequency',lowPassBandF,'PassbandRipple',lowPassBandR,'SampleRate',FRAMING_SIZE);	
	    % fvtool(lpFilt) % Plot Filter	
	    ecgTonyFilter = filter(lpFilt, ecgTonyFilter);	
	end	
		
		
	%highPassBanF=3; %% in default (from tony)	
	highPassBanF = str2num(get(handles.ecgHighPassBandF,'String'));	
	highPassBandR = str2num(get(handles.ecgHighPassBandR,'String'));	
	%% Lowpass IIR Filter - Mat	
	if get(handles.chkHighPassIIRFilter,'Value')==1	
	    %waitbar(2/10,h,'ECG Filter: Applying High Pass IRR Filter');	
	    hpFilt = designfilt('highpassiir','FilterOrder',3,'PassbandFrequency',highPassBanF,'PassbandRipple',highPassBandR,'SampleRate',FRAMING_SIZE);	
	    ecgTonyFilter = filter(hpFilt, ecgTonyFilter);	
	end	
		
		
	%waitbar(3/10,h,'ECG Filter: Applying Median Filter..');	
	ecgMedianF = medfilt1(ecgTonyFilter,ecgMedianF_FN);	
		
		
		
		
	%xd = wdenoise(XN,4);	
	%% denosing	
	%% https://uk.mathworks.com/help/wavelet/ug/wavelet-denoising.html	
	%% wavelate family	
	%% https://uk.mathworks.com/help/wavelet/ref/waveinfo.html	
	%https://uk.mathworks.com/help/wavelet/ug/wavelet-families-additional-discussion.html	
	%waitbar(4/10,h,'ECG Filter: Denosing using wden wavelets.');	
	xdMODWT = wden(ecgMedianF,'sqtwolog','s','mln',4,'db6');  %daubechies db6 wavelet	
		
	%bvpData = wdenoise(bvpDataRaw,18)	
		
		
		
		
		
		
		
	%plot(xd(100:5000,1)+200,'color','green')	
		
		
	ecgHalfPowerFreq1  =str2num(get(handles.ecgHalfPowerFreq1 ,'String'));	
	ecgHalfPowerFreq2 =  str2num(get(handles.ecgHalfPowerFreq2 ,'String'));	
	if ecgHalfPowerFreq1==ecgHalfPowerFreq2	
	    ecgHalfPowerFreq2=ecgHalfPowerFreq2+1;	
	    set(handles.ecgHalfPowerFreq2 ,'String',num2str(ecgHalfPowerFreq2))	
	   	
	end	
	%ecgBandStopFO = ceil(str2num(get(handles.ecgBandStopFOrdor),'String'));	
	%%% removing low frequency noise.....	
	% designing filter to remove regular guassian noise...	
	%waitbar(5/10,h,'ECG Filter: Applying bandstop IIR filter.');	
	rnRemovalFilter = designfilt('bandstopiir','FilterOrder',2, ...	
	    'HalfPowerFrequency1',ecgHalfPowerFreq1,'HalfPowerFrequency2',ecgHalfPowerFreq2, ...	
	    'DesignMethod','butter','SampleRate',FRAMING_SIZE);	
		
	%% applying the filter to remove noise	
	xdMODWT = xdMODWT';	
		
	xdMODWT = filtfilt(rnRemovalFilter,xdMODWT);	
		
	xdMODWT=xdMODWT';	
		
		
		
		
	smoothECG = xdMODWT;	
	% BASE_LINE_WINDOW=25;	
	% %%%%%    baseline wander	
	% ecgDataProcessed = nan(size(smoothECG,1),1);	
	% for i=1:size(smoothECG,1)-BASE_LINE_WINDOW	
	%     if i<=BASE_LINE_WINDOW	
	%         ecgDataProcessed(i)=smoothECG(i+1)-smoothECG(i);	
	%     else	
	%         ecgDataProcessed(i)=median(smoothECG(i:i+BASE_LINE_WINDOW,1)-smoothECG(i-BASE_LINE_WINDOW:i,1));	
	%     end	
	% end	
		
	%ecgDataProcessed = -1*ecgDataProcessed;	
	ecgBaseWonderF = FRAMING_SIZE/2;	
	%waitbar(6/10,h,'ECG Filter: Smoothing using median filter');	
	kk = medfilt1(smoothECG,ecgBaseWonderF);	
	ecgDataProcessed=smoothECG-kk;	
		
		
		
		
		
		
		
	%% for display	
		
		
	%ecgDataProcessed=ecgDataProcessed+mean(smoothECG)	
		
	%ecgMinPeackDist = FRAMING_SIZE/2;	
	%waitbar(7/10,h,'ECG Filter: Finding R Peaks....');	
	[~,ecgLocRwave] = findpeaks(ecgDataProcessed,'MinPeakHeight',ecgRPeakHeight,'MinPeakDistance',ecgMinPeackDist);	
		
	%ecgLocRwave=[peakLoc ones(size(peakLoc,1),1)];	
		
	% %% we cannot use this method to find S waves as in some cases S wave are smaller than Q wave..	
	% [~,locs_Swave] = findpeaks(-ecgDataProcessed,'MinPeakHeight',100,'MinPeakDistance',FRAMING_SIZE/2);	
	% plot(locs_Swave,ecgDataProcessed(locs_Swave),'rv','MarkerFaceColor','b')	
		
		
	%waitbar(8/10,h,'ECG Filter: Finding S Peaks....');	
		
		
	% we cannot use this method to find S waves as in some cases S wave are smaller than Q wave..	
	ecgLocSwave = ecgLocRwave(:,1);	
		
	for i=1:size(ecgLocRwave,1)	
	   	
	    %% there may be case that for the last R, cannot find S...	
	    try	
	        currLocation = [];	
	        dist = ecgRSDist;	
	        while (isempty(currLocation))	
	            data = -[ecgDataProcessed(ecgLocRwave(i,1):ecgLocRwave(i,1)+dist,1)];	
	           	
	            [~,currLocation] = findpeaks(data,'MinPeakHeight',mean(data));	
	            dist=dist+10;	
	        end	
	       	
	        if size(currLocation,1)>1	
	            currLocation = currLocation(1);	
	        end	
	       	
	        ecgLocSwave(i,1)=ecgLocRwave(i,1)+currLocation;	
	    catch	
	       	
	    end	
	end	
		
		
		
		
		
		
		
	ecgLocQwave = ecgLocRwave(:,1);	
	%dd = (abs(data(2:end)-data(1:end-1))+abs(data(1:end-1)-data(2:end)))./2	
	%kk = abs(dd(1:end-1)-dd(2:end))	
	%waitbar(9/10,h,'ECG Filter: Finding Q Peaks....');	
	%% finding q wave....	
	for i=1:size(ecgLocRwave,1)	
	    %% there may be case that there is no Q for first R	
	    try	
	        currLocation=[];	
	        tmpDist = ecgQRDist;	
	        %% if peak is not found increase the range of peak	
            count =1;
	        while isempty(currLocation)	
	            data = [ecgDataProcessed(ecgLocRwave(i,1)-tmpDist:ecgLocRwave(i,1),1)];	
	            tmpDist = tmpDist+10;	
	            [~,currLocation] = findpeaks(-data,'MinPeakHeight',mean(-data));	
                
	           	if tmpDist > 3*FRAMING_SIZE
                end
	        end	
	        if size(currLocation,1)>1	
	            currLocation = currLocation(end);	
	           	
	        end	
	        % [~,currLocation] = findpeaks(data,'MinPeakDistance',size(data,1)/2+10);	
	        ecgLocQwave(i,1)=ecgLocRwave(i,1)-ecgQRDist+currLocation;	
	    catch	
	    end	
	end	
		
		
		
		
		
		
	%waitbar(10/10,h,'ECG Filter: Finding T Peaks....');	
		
	ecgLocTwave = ecgLocSwave(:,1);	
	%% finding T wave....	
	for i=1:size(ecgLocSwave,1)	
	    %% there may be case that there is no T was for the last R peak	
	    try	
	        currLocation=[];	
	        %tmpDist = ecgSTDist+ecgSTDistMin;	
	        %% in the cse tmpDist cross the size of data, recorrect tmpDist.	
	       	
	        try	
	            %% if there is no i+1, there is error	
	            diff = ecgLocQwave(i+1,1)-ecgLocQwave(i,1);	
	            ecgSTDist = ceil((diff * ecgSTDistInPer)/100)	
	            maxTmpDist =  min(ecgLocQwave(i+1,1),ecgSTDist+ecgLocSwave(i,1))-ecgLocSwave(i,1);	
	        catch	
	            diff = size(ecgDataProcessed,1)-ecgLocQwave(i,1);	
	            ecgSTDist = ceil((diff * ecgSTDistInPer)/100)	
	            maxTmpDist =  min(size(ecgDataProcessed,1),ecgSTDist+ecgLocSwave(i,1))-ecgLocSwave(i,1);	
	        end	
	%         if (ecgLocSwave(i,1)+tmpDist)>size(ecgDataProcessed,1)	
	%             tmpDist=size(ecgDataProcessed,1) - ecgLocSwave(i,1) -2 %% 1is offset	
	%         end	
	        %% if peak is not found increase the range of peak  	
	               	
	        %% avoid ecgSTDistMin distance (too close)	
	       	
	        while isempty(currLocation)	
	           	
	            data = [ecgDataProcessed(ecgLocSwave(i,1)+ecgSTDistMin:ecgLocSwave(i,1)+maxTmpDist,1)];	
	            maxTmpDist = maxTmpDist+10;	
	            %% if inverst T is on peak T is trought	
	            if(get(handles.chkInverseT,'Value')==1)	
	               	
	                [~,currLocation] = findpeaks(-data,'MinPeakHeight',mean(-data),'MinPeakDistance',size(data,1)/2);	
	                if size(currLocation ,1)>1	
	                    [~,ind]=max(-data(currLocation))	
	                    currLocation = currLocation(ind);	
	                end	
	            else	
	                [~,currLocation] = findpeaks(data,'MinPeakHeight',mean(data),'MinPeakDistance',size(data,1)/2);	
	                if size(currLocation ,1)>1	
	                    [~,ind]=max(data(currLocation))	
	                    currLocation = currLocation(ind);	
	                end	
	            end	
	        end	
	        % [~,currLocation] = findpeaks(data,'MinPeakDistance',size(data,1)/2+10);	
	       	
		
		
	        ecgLocTwave(i,1)=ecgLocSwave(i,1)+currLocation+ecgSTDistMin;	
	    catch	
	    end	
	end	
	deleteECGPeaksWithinRange()	
	global slopeECG	
	slopeECG=zeros(size(ecgDataProcessed));	
		
	for i=5:size(slopeECG,1)-5	
	    slopeECG(i)=-(sum(slopeECG(i-4:i))-sum(slopeECG(i:i+4)))/4;	
	end	
		
	%set(handles.txtTpeakECG,'String',strcat('# Peaks:',num2str(size(ecgLocRwave,1))))	
	set(findall(handles.uiPanelMain, '-property', 'enable'), 'enable', 'on')	
%%close(h)

function deleteECGPeaksWithinRange()
global DeletedPeaksCol
global ecgLocQwave
global ecgLocRwave
global ecgLocSwave
global ecgLocTwave
if size(DeletedPeaksCol,1)>0
    allPeaks = cell2mat(DeletedPeaksCol(:,3));
    for i = 1:size(allPeaks,1)
      
       delPeakInd = find (ecgLocRwave==allPeaks(i,2))
       ecgLocQwave(delPeakInd,:)=[];
       ecgLocRwave(delPeakInd,:)=[];
       ecgLocSwave(delPeakInd,:)=[];
       ecgLocTwave(delPeakInd,:)=[];
      
    end
end

function deleteBVP1PeaksWithinRange()
global DeletedPeaksCol
global bvpLocQwave
global bvpLocRwave
global bvpLocSwave



if size(DeletedPeaksCol,1)>0
    allPeaks = cell2mat(DeletedPeaksCol(:,4));
    for i = 1:size(allPeaks,1)
      
       delPeakInd = find (bvpLocRwave==allPeaks(i,2))
       bvpLocQwave(delPeakInd,:)=[];
       bvpLocRwave(delPeakInd,:)=[];
       bvpLocSwave(delPeakInd,:)=[];
       
      
    end
end






function deleteBVP2PeaksWithinRange()
global DeletedPeaksCol
global bvpLocQwave2
global bvpLocRwave2
global bvpLocSwave2



if size(DeletedPeaksCol,1)>0
    allPeaks = cell2mat(DeletedPeaksCol(:,5));
    for i = 1:size(allPeaks,1)
      
       delPeakInd = find (bvpLocRwave2==allPeaks(i,2))
      
       bvpLocQwave2(delPeakInd,:)=[];
       bvpLocRwave2(delPeakInd,:)=[];
       bvpLocSwave2(delPeakInd,:)=[];
       
      
    end
end





function filterBVPData(handles)
%bvpDataProcessed=bvpDataProcessed';
global dataCuttOff;


global bvpLocRwave
global bvpLocSwave
global bvpLocQwave

global bvpDataProcessed

try
    bvpDataRaw = dataCuttOff(:,2);
catch
     bvpDataRaw = dataCuttOff(:,1);
end

%set(findall(handles.uiPanelMain, '-property', 'enable'), 'enable', 'off')
%h = waitbar(0,'BVP1 Filter ......');

bvpMedianFilter_FN = str2num(get(handles.bvpMedianFilter_FN,'String'))
%waitbar(1/10,h,'BVP1 Filter: Applying Median Filter');
bvpData=medfilt1(bvpDataRaw,bvpMedianFilter_FN);
%waitbar(2/10,h,'BVP1 Filter: Applying Mexican Hat Wavelet Filter');
bvpDataWDEN = wden(bvpData,'sqtwolog','s','mln',4,'sym4');  %Mexican Hat Wavelet: mexh

FRAMING_SIZE=str2num(get(handles.editFrameSize ,'String'));

%%% bvpData wondering
bvpBaseLineWF = FRAMING_SIZE*4;
%waitbar(3/10,h,'BVP1 Filter: Applying Baseline Filter');
kk = medfilt1(bvpDataWDEN,bvpBaseLineWF);
bvpBLWander=bvpDataWDEN-kk;

% %%smoothing BVP signals using mean.....
% bvpSmoothingCoff = 50;
 bvpDataProcessed = bvpBLWander;
% %waitbar(4/10,h,'BVP1 Filter: Smoothing using median');
% for i=1:size(bvpBLWander,1)
%     if i<size(bvpBLWander,1)-bvpSmoothingCoff && i>bvpSmoothingCoff
%         bvpDataProcessed(i,1)=mean(bvpBLWander(i-bvpSmoothingCoff:i+bvpSmoothingCoff,1));
%     end
% end
% 
% 
% %% take out first two frames
 bvpDataProcessed = bvpDataProcessed(3:end,1);

bvpMinPeakHeight = str2num(get(handles.editBVPMinPeakHeight,'String'));
bvpMinPeakDist = str2num(get(handles.editBVPMinPeakDist,'String'));

% bvpDataProcessed = bvpDataProcessed(StartDisplayFrame:EndDisplayFrame,1);

%waitbar(5/10,h,'BVP1 Processing: Finding Hill peak');
%[~,bvpLocRwave] = findpeaks(bvpBLWander(StartDisplayFrame:EndDisplayFrame,1),'MinPeakHeight',mean(bvpBLWander),'MinPeakDistance',FRAMING_SIZE/2);
%[~,rPeak] = findpeaks(bvpDataProcessed,'MinPeakHeight',bvpMinPeakHeight,'MinPeakDistance',bvpMinPeakDist);
[~,bvpLocRwave] = findpeaks(bvpDataProcessed,'MinPeakHeight',bvpMinPeakHeight,'MinPeakDistance',bvpMinPeakDist);
%bvpLocRwave=[rPeak ones(size(rPeak,1),1)];

% we cannot use this method to find S waves as in some cases S wave are smaller than Q wave..
%waitbar(7/10,h,'BVP1 Processing: Finding foot ');
bvpLocSwave = bvpLocRwave(:,1);
bvpRSDist = str2num(get(handles.editBVPRSDist,'String'));%150;
for i=1:size(bvpLocRwave,1)
    try
        currLocation = [];
        dist = bvpRSDist;
        while (isempty(currLocation))
            data = -[bvpDataProcessed(bvpLocRwave(i,1):bvpLocRwave(i,1)+dist,1)];
            [~,currLocation] = findpeaks(data,'MinPeakHeight',mean(data),'MinPeakDistance',size(data,1)/2);
            dist=dist+10;
        end
        
        if size(currLocation,1)>1
            currLocation = currLocation(1);
        end
        
        bvpLocSwave(i,1)=bvpLocRwave(i,1)+currLocation;
    catch
    end
end

%%plot the bvp signals......
bvpLocQwave = bvpLocRwave(:,1);


%waitbar(9/10,h,'BVP1 Processing: Finding end');

bvpQRDist = str2num(get(handles.editBVPQRDist,'String'));%150;


%% finding q wave....
for i=1:size(bvpLocRwave,1)
    try
        currLocation=[];
        tmpDist = bvpQRDist;
        %% if peak is not found increase the range of peak
        while isempty(currLocation)
            data = [bvpDataProcessed(bvpLocRwave(i,1)-tmpDist:bvpLocRwave(i,1),1)];
            tmpDist = tmpDist+10;
            [~,currLocation] = findpeaks(-data,'MinPeakHeight',mean(-data),'MinPeakDistance',size(data,1)/2);
        end
        % [~,currLocation] = findpeaks(data,'MinPeakDistance',size(data,1)/2+10);
        if size(currLocation ,1)>1
            currLocation = currLocation(1);
        end
        bvpLocQwave(i,1)=bvpLocRwave(i,1)-bvpQRDist+currLocation;
    catch
    end
end
%set(handles.txtTpeakBVP1,'String',strcat('# Peaks:',num2str(size(bvpLocRwave,1))));
deleteBVP1PeaksWithinRange()
set(findall(handles.uiPanelMain, '-property', 'enable'), 'enable', 'on')
%%close(h)



function filterBVPData1(handles)
%bvpDataProcessed=bvpDataProcessed';
global dataCuttOff


global bvpLocRwave2
global bvpLocSwave2
global bvpLocQwave2

global bvpDataProcessed2

bvpDataProcessed2=dataCuttOff(:,3);
%set(findall(handles.uiPanelMain, '-property', 'enable'), 'enable', 'off')
%h = waitbar(0,'BVP2 data Processing');
bvpMedianFilter_FN = str2num(get(handles.bvp1MedianFilter_FN,'String'));
%waitbar(1/10,h,'BVP2 Processing: Applying median filter');
bvpData=medfilt1(bvpDataProcessed2,bvpMedianFilter_FN);
%waitbar(2/10,h,'BVP2 Processing: Applying Mexican Hat Wavelet Filter');
bvpDataWDEN = wden(bvpData,'sqtwolog','s','mln',4,'sym4');  %Mexican Hat Wavelet: mexh

FRAMING_SIZE=str2num(get(handles.editFrameSize ,'String'));
%waitbar(3/10,h,'BVP2 Processing: Applying Wondering');
%%% bvpData wondering
bvpBaseLineWF = FRAMING_SIZE*4;
kk = medfilt1(bvpDataWDEN,bvpBaseLineWF);
bvpBLWander=bvpDataWDEN-kk;

%%smoothing BVP signals using mean.....
bvpSmoothingCoff = 50;
bvpDataProcessed2 = bvpBLWander;
%waitbar(4/10,h,'BVP2 Processing: Smoothing BVP2 using mean..');
for i=1:size(bvpBLWander,1)
    if i<size(bvpBLWander,1)-bvpSmoothingCoff && i>bvpSmoothingCoff
        bvpDataProcessed2(i,1)=mean(bvpBLWander(i-bvpSmoothingCoff:i+bvpSmoothingCoff,1));
    end
end

%% take out first two frames



bvpMinPeakHeight = str2num(get(handles.editBVP1MinPeakHeight,'String'));
bvpMinPeakDist = str2num(get(handles.bvp1MinPeakDist,'String'));

% bvpDataProcessed = bvpDataProcessed(StartDisplayFrame:EndDisplayFrame,1);
%waitbar(5/10,h,'BVP2 Processing: Finding Peaks');

%[~,bvpLocRwave] = findpeaks(bvpBLWander(StartDisplayFrame:EndDisplayFrame,1),'MinPeakHeight',mean(bvpBLWander),'MinPeakDistance',FRAMING_SIZE/2);
[~,bvpLocRwave2] = findpeaks(bvpDataProcessed2,'MinPeakHeight',bvpMinPeakHeight,'MinPeakDistance',bvpMinPeakDist);

%bvpLocRwave2=[rPeak ones(size(rPeak,1),1)];
%waitbar(7/10,h,'BVP2 Processing: Finding foots');
% we cannot use this method to find S waves as in some cases S wave are smaller than Q wave..
bvpLocSwave2 = bvpLocRwave2(:,1);
bvpRSDist = str2num(get(handles.editBVP1QRDist,'String'));%150;
for i=1:size(bvpLocRwave2,1)
    try
        currLocation = [];
        dist = bvpRSDist;
        while (isempty(currLocation))
            data = -[bvpDataProcessed2(bvpLocRwave2(i,1):bvpLocRwave2(i,1)+dist,1)];
            [~,currLocation] = findpeaks(data,'MinPeakHeight',mean(data),'MinPeakDistance',size(data,1)/2);
            dist=dist+10;
        end
        
        if size(currLocation,1)>1
            currLocation = currLocation(1);
        end
        
        bvpLocSwave2(i,1)=bvpLocRwave2(i,1)+currLocation;
    catch
    end
end

%%plot the bvp signals......
bvpLocQwave2 = bvpLocRwave2(:,1);
bvpQRDist = str2num(get(handles.editBVP1QRDist,'String'));%150;

%waitbar(9/10,h,'BVP2 Processing: Finding ends..');
%% finding q wave....
for i=1:size(bvpLocRwave2,1)
    try
        currLocation=[];
        tmpDist = bvpQRDist;
        %% if peak is not found increase the range of peak
        while isempty(currLocation)
            data = [bvpDataProcessed2(bvpLocRwave2(i,1)-tmpDist:bvpLocRwave2(i,1),1)];
            tmpDist = tmpDist+10;
            [~,currLocation] = findpeaks(-data,'MinPeakHeight',mean(-data),'MinPeakDistance',size(data,1)/2);
        end
        % [~,currLocation] = findpeaks(data,'MinPeakDistance',size(data,1)/2+10);
        if size(currLocation ,1)>1
            currLocation = currLocation(1);
        end
        bvpLocQwave2(i,1)=bvpLocRwave2(i,1)-bvpQRDist+currLocation;
    catch
    end
end
%set(handles.txtTpeakBVP2,'String',strcat('# Peaks:',num2str(size(bvpLocRwave2,1))));
deleteBVP2PeaksWithinRange()
set(findall(handles.uiPanelMain, '-property', 'enable'), 'enable', 'on')
%%close(h)


function displaySignal(handles)

global ecgLocQwave
global ecgLocRwave
global ecgLocSwave
global ecgLocTwave

global bvpLocQwave
global bvpLocRwave
global bvpLocSwave


global bvpLocQwave2
global bvpLocRwave2
global bvpLocSwave2

global ecgDataProcessed





global StartDisplayFrame
global chkRange


clearPlots()

%axes(handles.axes1);

%set('CurrentAxes',handles.axes1)
if get(handles.chkDataECG,'Value')==1
    displayECGData(handles);
    uitablehandle = findall(handles.uiPDataTable,'type','uitable');
end

if get(handles.chkDataBVP1,'Value')==1
    displayBVPData(handles);
    uitablehandleBVP = findall(handles.uiPDataTableBVP,'type','uitable');
end

if get(handles.chkDataBVP2,'Value')==1
    displayBVPData1(handles);
    uitablehandleBVP1 = findall(handles.uiPDataTableBVP1,'type','uitable');
end






EndDisplayFrame = StartDisplayFrame+str2num(get(handles.editDWindowSize,'String'))-1;


set(handles.editSignalLoc,'String',strcat(num2str(StartDisplayFrame),'   -   ',num2str(EndDisplayFrame)));


%dataQ = [ecgLocQwave(ecgLocQwave(:,1)>= StartDisplayFrame & ecgLocQwave(:,1)<= EndDisplayFrame,1)];
try
    dataR = [ecgLocRwave(ecgLocRwave(:,1)>= StartDisplayFrame & ecgLocRwave(:,1)<= EndDisplayFrame,1)];
catch
    dataR=[];
end
try
    dataRBVP = [bvpLocRwave(bvpLocRwave(:,1)>= StartDisplayFrame & bvpLocRwave(:,1)<= EndDisplayFrame,1)];
catch
    dataRBVP=[];
end

try
    dataRBVP1 = [bvpLocRwave2(bvpLocRwave2(:,1)>= StartDisplayFrame & bvpLocRwave2(:,1)<= EndDisplayFrame,1)];
catch
    dataRBVP1=[];
end

%dataS = [ecgLocSwave(ecgLocSwave(:,1)>= StartDisplayFrame & ecgLocSwave(:,1)<= EndDisplayFrame,1)];
%dataT = [ecgLocTwave(ecgLocTwave(:,1)>= StartDisplayFrame & ecgLocTwave(:,1)<= EndDisplayFrame,1)];

%% make sure data are in order of QRST, R peak is taken as reference point
%% we take only a complete frequency with all QRST peaks...
% dataR= dataR(dataR(:,1)>=dataQ(1,1) & dataR(:,1) <=dataT(end,1),1);

%dataQ= dataQ(dataQ(:,1)<=dataR(end,1),1);

%dataS= dataS(dataS(:,1)<=dataT(end,1),1);

%dataT= dataT(dataT(:,1)>=dataR(1,1),1);

%% make sure data have same size...

%minNoPoints = min([size(dataQ(:,1),1), size(dataR(:,1),1),size(dataS(:,1),1), size(dataT(:,1),1)])

%dataQ = dataQ(1:minNoPoints,1);
%dataR = dataR(1:minNoPoints,1);
%dataS = dataS(1:minNoPoints,1);
%dataT = dataT(1:minNoPoints,1);
dataIndex = nan(size(dataR,1),1);
dataIndexBVP = nan(size(dataRBVP,1),1);
dataIndexBVP2 = nan(size(dataRBVP1,1),1);
try
    for i=1:size(dataIndex,1)
        [dataIndex(i,1),kk] = find(ecgLocRwave(:,1)==dataR(i,1));
    end
    set(uitablehandle,'Data',[ecgLocQwave(dataIndex,1) ecgLocRwave(dataIndex,1) ecgLocSwave(dataIndex,1) ecgLocTwave(dataIndex,1) dataIndex],'RowName', num2str(dataIndex));
catch
    x=1;
end
try
    for i=1:size(dataIndexBVP,1)
        [dataIndexBVP(i,1),kk] = find(bvpLocRwave(:,1)==dataRBVP(i,1));
    end
    set(uitablehandleBVP,'Data',[bvpLocQwave(dataIndexBVP,1) bvpLocRwave(dataIndexBVP,1) bvpLocSwave(dataIndexBVP,1) dataIndexBVP] ,'RowName', num2str(dataIndexBVP));
catch
end
try
    for i=1:size(dataIndexBVP2,1)
        [dataIndexBVP2(i,1),kk] = find(bvpLocRwave2(:,1)==dataRBVP1(i,1));
    end
    set(uitablehandleBVP1,'Data',[bvpLocQwave2(dataIndexBVP2,1) bvpLocRwave2(dataIndexBVP2,1) bvpLocSwave2(dataIndexBVP2,1) dataIndexBVP2] ,'RowName', num2str(dataIndexBVP2));
catch
    x=1;
end

% try
%
%     set(uitablehandle,'Data',[ecgLocQwave(dataIndex,1) ecgLocRwave(dataIndex,1) ecgLocSwave(dataIndex,1) ecgLocTwave(dataIndex,1) dataIndex],'RowName', num2str(dataIndex));
%
%
%     set(uitablehandleBVP1,'Data',[bvpLocQwave2(dataIndexBVP2,1) bvpLocRwave2(dataIndexBVP2,1) bvpLocSwave2(dataIndexBVP2,1) dataIndexBVP2] ,'RowName', num2str(dataIndex));
% catch
%     x=1
% end
chkRange = false;
set(handles.pbDeleteSignals,'Enable','off');
set(handles.pbDeleteSignals,'BackgroundColor',[0.94,0.6,0.46]);
set(handles.txtTpeakECG,'String',strcat('# Peaks:',num2str(size(ecgLocRwave,1))));
set(handles.txtTpeakBVP1,'String',strcat('# Peaks:',num2str(size(bvpLocRwave,1))));
set(handles.txtTpeakBVP2,'String',strcat('# Peaks:',num2str(size(bvpLocRwave2,1))));









%%% Checking for validity of peaks with BVP2

if size(bvpLocRwave,1)>0 & size(ecgLocRwave,1)>0
    errNumMsg='#BVP = #ECG';
    set(handles.txtErrNumBVP1,'BackgroundColor','Green');
    if(size(bvpLocRwave,1)~=size(ecgLocRwave,1))
        errNumMsg='#BVP <> #ECG';
        set(handles.txtErrNumBVP1,'BackgroundColor','Red');
    end
    %% find the first location where ecg peak lack behind bvp peak
    minSize=min(size(bvpLocRwave,1),size(ecgLocRwave,1));
    flag=[ecgLocRwave(1:minSize,1)<bvpLocRwave(1:minSize,1)];
    errLoc = find(flag==0);
    set(handles.txtErrLocBVP1,'BackgroundColor','Green');
    errLocMsg = 'Miss Loc: OK';
    if(size(errLoc)>0)
        errLocMsg = strcat('Miss Loc:',num2str(errLoc(1,1)));
        set(handles.txtErrLocBVP1,'BackgroundColor','Red');
    end
    
    
    
    %%% Check for peak Overlapping
    %%  BVP R location must be before next ECG R Location
    nextECGrPeakLoc = ecgLocRwave(2:end,1);
    
    
    
    minSize=min(size(bvpLocRwave,1),size(nextECGrPeakLoc,1));
    flag=[nextECGrPeakLoc(1:minSize,1)>bvpLocRwave(1:minSize,1)];
    errLoc = find(flag==0);
    set(handles.txtOverlapLocBVP1,'BackgroundColor','Green');
    errOverLapLocMsg = 'OverLap: OK';
    if(size(errLoc)>0)
        errOverLapLocMsg = strcat('OverLap At:',num2str(errLoc(1,1)));
        set(handles.txtOverlapLocBVP1,'BackgroundColor','Red');
    end
    
    
    
    
    
    
    
    set(handles.txtErrNumBVP1,'String',errNumMsg);
    set(handles.txtErrLocBVP1,'String',errLocMsg);
    set(handles.txtOverlapLocBVP1,'String',errOverLapLocMsg);
end


%%% Checking for validity of peaks with BVP2

if size(bvpLocRwave2,1)>0 & size(ecgLocRwave,1)>0
    errNumMsg='#BVP = #ECG';
    set(handles.txtErrNumBVP2,'BackgroundColor','Green');
    if(size(bvpLocRwave2,1)~=size(ecgLocRwave,1))
        errNumMsg='#BVP <> #ECG';
        set(handles.txtErrNumBVP2,'BackgroundColor','Red');
    end
    %% find the first location where ecg peak lack behind bvp peak
    minSize=min(size(bvpLocRwave2,1),size(ecgLocRwave,1));
    flag=[ecgLocRwave(1:minSize,1)<bvpLocRwave2(1:minSize,1)];
    errLoc = find(flag==0);
    set(handles.txtErrLocBVP2,'BackgroundColor','Green');
    errLocMsg = 'Miss Loc: OK';
    if(size(errLoc)>0)
        errLocMsg = strcat('Miss Loc:',num2str(errLoc(1,1)));
        set(handles.txtErrLocBVP2,'BackgroundColor','Red');
    end
    
    %%% Check for peak Overlapping
    %%  BVP R location must be before next ECG R Location
    nextECGrPeakLoc = ecgLocRwave(2:end,1);
    
    
    
    minSize=min(size(bvpLocRwave2,1),size(nextECGrPeakLoc,1));
    flag=[nextECGrPeakLoc(1:minSize,1)>bvpLocRwave2(1:minSize,1)];
    errLoc = find(flag==0);
    set(handles.txtOverlapLocBVP2,'BackgroundColor','Green');
    errOverLapLocMsg = 'OverLap: OK';
    if(size(errLoc)>0)
        errOverLapLocMsg = strcat('OverLap At:',num2str(errLoc(1,1)));
        set(handles.txtOverlapLocBVP2,'BackgroundColor','Red');
    end
    
    
    
    
    
    
    
    set(handles.txtErrNumBVP2,'String',errNumMsg);
    set(handles.txtErrLocBVP2,'String',errLocMsg);
    set(handles.txtOverlapLocBVP2,'String',errOverLapLocMsg);
end


%% Checking for validity of P peaks within ECG..


%%% Check for peak Overlapping
%%  T overlapping with next S
if get(handles.chkDataECG,'Value') == 1
    nextECGqPeakLoc = ecgLocQwave(2:end,1);
    flag=[ecgLocTwave(1:end-1,1)<nextECGqPeakLoc(:,1)];
    errLoc = find(flag==0);
    totalOverLap = sum(flag==0);
    set(handles.txtPOverlap,'BackgroundColor','Green');
    errOverLapLocMsg = 'P OverLap: OK';
    if(size(errLoc)>0)
        errOverLapLocMsg = strcat('P OL:',num2str(errLoc(1,1)),'/',num2str(totalOverLap));
        set(handles.txtPOverlap,'BackgroundColor','Red');
    end
end
if get(handles.chkDataECG,'Value') == 1

    %% finding the ST with mininum distance
    [~,sortIndex] = sort(ecgLocTwave-ecgLocSwave,'ascend');
    sortIndex = num2cell(sortIndex);
    sortIndex = ['Min Dist';sortIndex];
    if get(handles.pMenuMinPDist,'Value')>size(ecgLocRwave,1)
        set(handles.pMenuMinPDist,'Value',1)
    end
    set(handles.pMenuMinPDist,'String',sortIndex);
    %set(handles.txtGoToMinP,'String',ecgMinDistLoc);
    set(handles.txtPOverlap,'String',errOverLapLocMsg);
    
    
    %% finding the R with maximum deviation for its mean height value
    
    RHeight = ecgDataProcessed(ecgLocRwave,1);
    
    diffRHeight = abs(RHeight-repmat(mean(RHeight),size(RHeight,1),1));
    
    
    
    [~,sortIndex] = sort(diffRHeight,'descend');
    sortIndex = num2cell(sortIndex);
    sortIndex = ['Max Deviation';sortIndex];
    if get(handles.pMenuRMaxDev,'Value')>size(ecgLocRwave,1)
        set(handles.pMenuRMaxDev,'Value',1)
    end
    set(handles.pMenuRMaxDev,'String',sortIndex);
    %set(handles.txtGoToMinP,'String',ecgMinDistLoc);
    %set(handles.txtPOverlap,'String',errOverLapLocMsg);
    
    
    
    
    
    
    
    
    %% finding the Slope of R
    
    qHeight = ecgDataProcessed(ecgLocQwave,1);
    rHeight = ecgDataProcessed(ecgLocRwave,1);
    theta1 = atand((ecgLocRwave-ecgLocQwave)./(rHeight-qHeight));   
        
    sHeight = ecgDataProcessed(ecgLocSwave,1);    
   
    theta2 = atand((ecgLocSwave-ecgLocRwave)./(rHeight-sHeight));
    
    
    theta = radtodeg(theta1 + theta2);
     [thetaSorted,sortIndex] = sort(theta,'descend');
    sortIndex = strcat(num2str(sortIndex),'-theta ',num2str(thetaSorted));
    sortIndex=cellstr(sortIndex)
    sortIndex = ['Max Angle';sortIndex];
%     if get(handles.pMenuMinPDist,'Value')>size(ecgLocRwave,1)
%         set(handles.pMenuMinPDist,'Value',1)
%     end
    set(handles.pMenuMaxRSlope,'String',sortIndex);
    %set(handles.txtGoToMinP,'String',ecgMinDistLoc);
   % set(handles.txtPOverlap,'String',errOverLapLocMsg);
   
   
   %%% finding the R with closest distance..
   
   
    prevR = [nan ;ecgLocRwave(1:end-1)];
    nextR = [ecgLocRwave(2:end);nan];
    
    absDiff = [ abs(prevR-ecgLocRwave) abs(nextR-ecgLocRwave)]
    minAbsDiff = nanmin(absDiff,[],2)
    maxAbsDiff = nanmax(absDiff,[],2)
    
    
    
    
    
     %%% finding the closest neighbour R
    [~,sortIndex] = sort(minAbsDiff,'ascend');
    sortIndex = num2cell(sortIndex);
    sortIndex = ['Closest R';sortIndex];
    if get(handles.pMenuClosestR,'Value')>size(ecgLocRwave,1)
        set(handles.pMenuClosestR,'Value',1)
    end
    set(handles.pMenuClosestR,'String',sortIndex);
    %set(handles.txtGoToMinP,'String',ecgMinDistLoc);
    %set(handles.txtPOverlap,'String',errOverLapLocMsg);
    
    
     %%% finding the widest neighbour R
    [~,sortIndex] = sort(maxAbsDiff,'descend');
    sortIndex = num2cell(sortIndex);
    sortIndex = ['Widest R';sortIndex];
    if get(handles.pMenuWidestR,'Value')>size(ecgLocRwave,1)
        set(handles.pMenuWidestR,'Value',1)
    end
    set(handles.pMenuWidestR,'String',sortIndex);
    
    
    
end

global bvpDataProcessed



if get(handles.chkDataBVP1,'Value')==1
    
    
    RHeight = bvpDataProcessed(bvpLocRwave,1);
    
    diffRHeight = abs(RHeight-repmat(mean(RHeight),size(RHeight,1),1));
    
    
    
    [~,sortIndex] = sort(diffRHeight,'descend');
    sortIndex = num2cell(sortIndex);
    sortIndex = ['Max Deviation';sortIndex];
    if get(handles.pMenuBVP1PeakDev,'Value')>size(bvpLocRwave,1)
        set(handles.pMenuBVP1PeakDev,'Value',1)
    end
    set(handles.pMenuBVP1PeakDev,'String',sortIndex); 
    
    
    
    
    
    
    
    
    
    
    
    
    
    %% finding the Slope of R
    
    qHeight = bvpDataProcessed(bvpLocQwave,1);
    rHeight = bvpDataProcessed(bvpLocRwave,1);
    theta1 = atand((bvpLocRwave-bvpLocQwave)./(rHeight-qHeight));   
        
    sHeight = bvpDataProcessed(bvpLocSwave,1);    
   
    theta2 = atand((bvpLocSwave-bvpLocRwave)./(rHeight-sHeight));
    
    
    theta = radtodeg(theta1 + theta2);
     [thetaSorted,sortIndex] = sort(theta,'descend');
    sortIndex = strcat(num2str(sortIndex),'-theta ',num2str(thetaSorted));
    sortIndex=cellstr(sortIndex)
    sortIndex = ['Max Angle';sortIndex];
%     if get(handles.pMenuMinPDist,'Value')>size(ecgLocRwave,1)
%         set(handles.pMenuMinPDist,'Value',1)
%     end
    set(handles.pMenuMaxBVP1RSlope,'String',sortIndex);
    %set(handles.txtGoToMinP,'String',ecgMinDistLoc);
   % set(handles.txtPOverlap,'String',errOverLapLocMsg);
   
   
   %%% finding the R with closest distance..
   
   
    prevR = [nan ;bvpLocRwave(1:end-1)];
    nextR = [bvpLocRwave(2:end);nan];
    
    absDiff = [ abs(prevR-bvpLocRwave) abs(nextR-bvpLocRwave)]
    minAbsDiff = nanmin(absDiff,[],2)
    maxAbsDiff = nanmax(absDiff,[],2)
    
    
    
    
    
     %%% finding the closest neighbour R
    [~,sortIndex] = sort(minAbsDiff,'ascend');
    sortIndex = num2cell(sortIndex);
    sortIndex = ['Closest R';sortIndex];
    if get(handles.pMenuBVP1ClosestR,'Value')>size(bvpLocRwave,1)
        set(handles.pMenuBVP1ClosestR,'Value',1)
    end
    set(handles.pMenuBVP1ClosestR,'String',sortIndex);
    %set(handles.txtGoToMinP,'String',ecgMinDistLoc);
    %set(handles.txtPOverlap,'String',errOverLapLocMsg);
    
    
     %%% finding the widest neighbour R
    [~,sortIndex] = sort(maxAbsDiff,'descend');
    sortIndex = num2cell(sortIndex);
    sortIndex = ['Widest R';sortIndex];
    if get(handles.pMenuBVP1WidestR,'Value')>size(bvpLocRwave,1)
        set(handles.pMenuBVP1WidestR,'Value',1)
    end
    set(handles.pMenuBVP1WidestR,'String',sortIndex);
    
    
    
    
    
    
    
    
    
    
end
if get(handles.chkDataECG,'Value')==1 & get(handles.chkDataBVP1,'Value')==1
    %% Checking the Lap time between ECG and BVP1
    minPointsN = min(size(ecgLocRwave,1),size(bvpLocRwave,1) );
    [~,sortIndex] = sort(ecgLocRwave(1:minPointsN,1)-bvpLocRwave(1:minPointsN,1),'descend');
    sortIndex = num2cell(sortIndex);
    sortIndex = ['Min BVP1 Lap';sortIndex];
    if get(handles.pMenuBVP1Lag,'Value')>minPointsN
        set(handles.pMenuBVP1Lag,'Value',1)
    end
    set(handles.pMenuBVP1Lag,'String',sortIndex);
    
    
    
    
end

if get(handles.chkDataECG,'Value')==1 & get(handles.chkDataBVP2,'Value')==1
    %% Checking the Lap time between ECG and BVP2
    minPointsN = min(size(ecgLocRwave,1),size(bvpLocRwave2,1) );
    [~,sortIndex] = sort(ecgLocRwave(1:minPointsN,1)-bvpLocRwave2(1:minPointsN,1),'descend');
    sortIndex = num2cell(sortIndex);
    sortIndex = ['Min BVP2 Lap';sortIndex];
    if get(handles.pMenuBVP2Lag,'Value')>minPointsN
        set(handles.pMenuBVP2Lag,'Value',1)
    end
    set(handles.pMenuBVP2Lag,'String',sortIndex);
end








function  displayECGData(handles)	
	global dataCuttOff	
		
	global ecgDataProcessed	
	global ecgLocQwave	
	global ecgLocRwave	
	global ecgLocSwave	
	global ecgLocTwave	
	global StartDisplayFrame	
	global DataRaw	
	global ax1	
	global ax2	
	global chkRange	
		
	global chkPeakFrame	
		
	global slopeECG	
		
		
		
	global DeletedPeaksCol	
		
		
	global FONT_SIZE_PLOT_NO	
		
	try	
	    %DeletedSignals = [ones(size(DeletedPeaksCol,1),1) cell2mat(DeletedPeaksCol(:,1)) cell2mat(DeletedPeaksCol(:,2))];	
	    DeletedSignals = [cell2mat(DeletedPeaksCol(:,1)) cell2mat(DeletedPeaksCol(:,2))];	
	catch	
	    DeletedSignals=[];	
	end	
		
	global displayMatrix	
		
	totalSubGraph = sum(displayMatrix);	
		
	%there may be case data is not processed yet	
	if isempty(ecgDataProcessed)	
	    ecgDataRaw = DataRaw(:,1);	
	else	
	    ecgDataRaw=dataCuttOff(:,1);	
	end	
		
	%% can not access more than size of ecgData size..	
	EndDisplayFrame = min( StartDisplayFrame+str2num(get(handles.editDWindowSize,'String')),size(ecgDataRaw,1));	
		
		
	%%deepak	
		
		
	%% find any deleted signals within the range....	
	try	
	   	
	   	
	    %currDeletedSignals = DeletedSignals((DeletedSignals(:,2)>=StartDisplayFrame & DeletedSignals(:,2)<=EndDisplayFrame) | (DeletedSignals(:,3)>=StartDisplayFrame & DeletedSignals(:,3)<=EndDisplayFrame),:)	
	    currDeletedSignals = DeletedSignals((DeletedSignals(:,1)>=StartDisplayFrame & DeletedSignals(:,1)<=EndDisplayFrame) | (DeletedSignals(:,2)>=StartDisplayFrame & DeletedSignals(:,2)<=EndDisplayFrame),:)	
	    %% rEOMVOE THE DELETED POINTS OUT OF FRAME PROBLEM	
	    %currDeletedSignals(currDeletedSignals(:,2)<StartDisplayFrame,2)=StartDisplayFrame;	
	    currDeletedSignals(currDeletedSignals(:,1)<StartDisplayFrame,1)=StartDisplayFrame;	
	    %currDeletedSignals(currDeletedSignals(:,3)>EndDisplayFrame,3)=EndDisplayFrame;	
	    currDeletedSignals(currDeletedSignals(:,2)>EndDisplayFrame,2)=EndDisplayFrame;	
	catch	
	    currDeletedSignals=[];	
	end	
		
		
		
		
	if displayMatrix(1,1)==1	
	   	
	   	
	   	
	   	
	    ax1 = subplot(totalSubGraph,1,1);	
	    if StartDisplayFrame<1	
	        StartDisplayFrame=1;	
	    end	
	   	
	    try	
	        %% plot startDisplayFrame-1 to avoid long strip at beginning	
% 	        try	
% 	            plot([[1:StartDisplayFrame-1 ecgDataRaw(StartDisplayFrame-1,1)- mean(ecgDataRaw) ]';ecgDataRaw(StartDisplayFrame:EndDisplayFrame,1)] - mean(ecgDataRaw),'color','blue');	
% 	        catch	
% 	            %% when startDisplayFrame = 1	
% 	            plot([[1:StartDisplayFrame ]';ecgDataRaw(StartDisplayFrame:EndDisplayFrame,1)] - mean(ecgDataRaw),'color','blue');	
%                 plot(ecgDataRaw(1:EndDisplayFrame,1) - mean(ecgDataRaw),'color','blue');	
% 	        end	
	       	plot(ecgDataRaw(1:EndDisplayFrame,1) - mean(ecgDataRaw),'color','blue');	
	       	
	       	
	        %         myPos = get(handles.sliderPeak,'Position')	
	        %         myPos(1)=ax1.Position(1)	
	        %         myPos(3)=ax1.Position(3)	
	        %	
	        %         set(handles.sliderPeak,'Position',myPos);	
	       	
	        hold on	
	        %% get min and max value to draw vertical line	
	        minVal = min(ecgDataRaw(StartDisplayFrame:EndDisplayFrame,1) - mean(ecgDataRaw));	
	        maxVal = max(ecgDataRaw(StartDisplayFrame:EndDisplayFrame,1) - mean(ecgDataRaw));	
	       	
	       	
	    catch	
	        x=1	
	    end	
	    ylabel('ECG Raw');	
	    if minVal ==maxVal	
	        maxVal = 2*minVal + 10;	
	    end	
	    axis([StartDisplayFrame,EndDisplayFrame,minVal*0.90,maxVal*1.10])	
	    %set(ax1,'Units','Pixels','Position',getpixelposition(ax1)-[0,0,FIGURE_FRAME_Y,0])	
	    gap = ceil((EndDisplayFrame-StartDisplayFrame)/30);	
	   	
	    set(gca,'XTick',[StartDisplayFrame:gap:EndDisplayFrame])	
	end	
		
		
		
		
	if displayMatrix(1,2)==1	
	    %hold on	
	    %plot(ecgDataProcessed(StartDisplayFrame:EndDisplayFrame,1),'color','green');	
	    currPlotID = sum(displayMatrix(1,1:1))+1;	
	    ax2=subplot(totalSubGraph,1,currPlotID);	
	   	
	    %set(ax2,'ButtonDownFcn',@(~,~)msgbox('Hi I am here'),'HitTest','off')	
	   	
	   	
	   	
	    %% plot StartDispalyFrame-1 to void line strip at beginning..	
% 	    try	
% 	        plot([[1:StartDisplayFrame-2 ecgDataProcessed(StartDisplayFrame-1,1) ]';ecgDataProcessed(StartDisplayFrame:EndDisplayFrame,1)],'color','green', 'ButtonDownFcn', {@TestMe,handles, 10})	
% 	    catch	
% 	        StartDisplayFrame=1	
% 	        plot([[1:StartDisplayFrame-1 ]';ecgDataProcessed(StartDisplayFrame:EndDisplayFrame,1)],'color','green','ButtonDownFcn', {@TestMe, handles,10})	
% 	    end	
        
	   	plot(ecgDataProcessed(1:EndDisplayFrame,1),'color','green','ButtonDownFcn', {@TestMe, handles,10})	
	 	
	    hold on	
	    %plot([[1:StartDisplayFrame-1 ]';slopeECG(StartDisplayFrame:EndDisplayFrame,1)],'color','blue',)	
	   	
	    minVal = min(ecgDataProcessed(StartDisplayFrame:EndDisplayFrame,1) );	
	    maxVal = max(ecgDataProcessed(StartDisplayFrame:EndDisplayFrame,1));	
	   	
	    %% if plot peak is on	
	    if get(handles.chkPlotPeak,'Value')==1	
	       	
	        hold on	
	       	
	       	
	       	
	        tmpLoc = [ecgLocQwave(ecgLocQwave(:,1)>= StartDisplayFrame & ecgLocQwave(:,1)<= EndDisplayFrame,1)];	
	        plot(tmpLoc,ecgDataProcessed(tmpLoc),'rs', 'ButtonDownFcn', {@TestMe,handles, 11});%,'MarkerFaceColor','g')	
	       	
	       	
	       	
	        tmpLoc = [ecgLocRwave(ecgLocRwave(:,1)>= StartDisplayFrame & ecgLocRwave(:,1)<= EndDisplayFrame,1)];	
	        % rIndex =find(ecgLocRwave(ecgLocRwave(:,1)>= StartDisplayFrame & ecgLocRwave(:,1)<= EndDisplayFrame,1));	
	       	
	       	
		
	       	
	       	
	       	
	        rIndex=zeros(size(tmpLoc,1),1);	
	        for i=1:size(tmpLoc,1)	
	            rIndex(i,1) = find(ecgLocRwave(:,1)==tmpLoc(i,1));	
	        end	
	       	
	        text(tmpLoc',ecgDataProcessed(tmpLoc),num2str(rIndex),'vert','bottom','horiz','center','FontSize',FONT_SIZE_PLOT_NO);	
	       	
	       	
	        %{@TestMe, handles,10}	
	       	
	         plot(tmpLoc,ecgDataProcessed(tmpLoc),'b*', 'ButtonDownFcn', {@TestMe, handles, 12});%'MarkerFaceColor','r');	
	       	
	       	
	       	
	       	
	       	
	       	
	       	
	        tmpLoc = [ecgLocSwave(ecgLocSwave(:,1)>= StartDisplayFrame & ecgLocSwave(:,1)<= EndDisplayFrame,1)];	
	        plot(tmpLoc,ecgDataProcessed(tmpLoc),'rv', 'ButtonDownFcn', {@TestMe,handles, 13});%,'MarkerFaceColor','b');	
	       	
	        tmpLoc = [ecgLocTwave(ecgLocTwave(:,1)>= StartDisplayFrame & ecgLocTwave(:,1)<= EndDisplayFrame,1)];	
	        plot(tmpLoc,ecgDataProcessed(tmpLoc),'ko', 'ButtonDownFcn', {@TestMe,handles, 14});%,'MarkerFaceColor','g')	
	       	
	       	
	       	
	        if chkRange	
	           	
	            fromFrame = str2num(get(handles.editDeleteSignalFrom,'String'));	
	            toFrame = str2num(get(handles.editDeleteSignalTo,'String'))	
	           	
	            plot([fromFrame fromFrame],[minVal maxVal],'color','red')	
	            plot([toFrame toFrame],[minVal maxVal],'color','red')	
	           	
	        end	
	       	
	       	
	        %%% display the previously deleted signal	
	        for i=1:size(currDeletedSignals,1)	
	            fprintf('\nDraw a line......::%d',i)	
	            fromFrame = currDeletedSignals(i,1);	
	            toFrame =  currDeletedSignals(i,2);	
	           	
	            plot([fromFrame fromFrame],[minVal maxVal],'color','black','MarkerSize',5)	
	            plot([toFrame toFrame],[minVal maxVal],'color','black','MarkerSize',5)	
	           	
	            %% cross line	
	            plot([fromFrame toFrame],[maxVal minVal],'color','black')	
	            plot([fromFrame toFrame],[minVal maxVal],'color','black')	
	        end	
	       	
	        %% display check addPeak	
	        if get(handles.chkECG,'Value')==1 & chkPeakFrame	
	            peakPos = ceil(str2num(get(handles.editAddPeak,'String')));	
	           	
	           	
	            plot([peakPos peakPos],[maxVal minVal],'-.','color','black')	
	        end	
	        ylabel('ECG P');	
	    end	
	   	
	%    set(ax2,'Units','Pixels','Position',getpixelposition(ax2)-[0,0,FIGURE_FRAME_Y,0])	
	   	
	    axis([StartDisplayFrame,EndDisplayFrame,minVal*1.10,maxVal*1.10])	
	    gap = ceil((EndDisplayFrame-StartDisplayFrame)/30);	
	    set(gca,'XTick',[StartDisplayFrame:gap:EndDisplayFrame])	
  end

function TestMe(ObjectH, EventData,handles,id)
global mPeakID
global mPeakIDCord
global peakNum
global ax2
global ax4
global ax6

global ecgDataProcessed
global bvpDataProcessed
global bvpDataProcessed2

global ecgLocQwave
global ecgLocRwave
global ecgLocSwave
global ecgLocTwave



global bvpLocRwave
global bvpLocSwave
global bvpLocQwave

global bvpLocRwave2
global bvpLocSwave2
global bvpLocQwave2

global chkPeakFrame

%% display check Peak frame only if check Peak button is clicked.
chkPeakFrame = false;
set(handles.pbAddPeak,'enable','off');
% persistent chk
% 
% doubleClick = false;
% 
% 
% 
% if isempty(chk)
%       chk = 1;
%       pause(0.5); %Add a delay to distinguish single click from a double click
%       if chk == 1
%           fprintf(1,'\nI am doing a single-click.\n\n');
%           chk = [];
%           doubleClick = false;
%       end
% else
%       chk = [];
%       fprintf(1,'\nI am doing a double-click.\n\n');
%       doubleClick = true;
%       %% move record of previous click
%       mPeakIDCord=floor(mPeakIDCord/10);
% end
%% if only ecg checked
try
    coordinates = get(ax2,'CurrentPoint');
catch
    try
        %% if bvp1 checked
        coordinates = get(ax4,'CurrentPoint');
    catch
        %% if bvp2 checked
            coordinates = get(ax6,'CurrentPoint');
        
    end
end
set(handles.txtECGInfo,'String','ECG Info:');
set(handles.txtBVP1Info,'String','BVP1 Info:');
set(handles.txtBVP2Info,'String','BVP2 Info:');

try
    mPeakIDCord=[ceil(coordinates(1,1)) ceil(coordinates(1,2))];
catch
    x=1;
end

if isempty(mPeakIDCord )
    return
end
if id == 10 
    
    set(handles.txtECGInfo,'String','ECG Info: Clicked....');
    %% add click info to add peak
    set(handles.editAddPeak,'String',num2str(mPeakIDCord(1,1)));
    %% add click info to delete by range 
    set(handles.chkECG,'Value',1);
    set(handles.chkBVP1,'Value',0);   
    set(handles.chkBVP2,'Value',0);
    %% adding range delete click info
    %currLowerLimit = str2num(get(handles.editDeleteSignalFrom,'String'));%
    
    if get(handles.chkLowerRange,'Value')==1
        set(handles.editDeleteSignalFrom,'String',num2str(mPeakIDCord(1,1)));
    else
        set(handles.editDeleteSignalTo,'String',num2str(mPeakIDCord(1,1)));
    end
   
    if mPeakID ==11
        lowerLimit = 1;
        if peakNum > 1
            lowerLimit = ecgLocTwave(peakNum-1,1);
        end
        upperLimit = ecgLocRwave(peakNum,1);
        newLoc =mPeakIDCord(1,1);
        if newLoc >=lowerLimit & newLoc < upperLimit
            ecgLocQwave(peakNum,1)=newLoc;
            set(handles.txtECGInfo,'String',strcat('ECG Info: Q peak',num2str(peakNum),' Moved.'));
        else
            set(handles.txtECGInfo,'String',strcat('ECG Info: Q peak ',num2str(peakNum),' Invalid Location'));
        end
    elseif mPeakID ==12
        
        
        lowerLimit = ecgLocQwave(peakNum,1)
        upperLimit = ecgLocSwave(peakNum,1);
        newLoc =ceil(mPeakIDCord(1,1));
        if newLoc >=lowerLimit & newLoc < upperLimit
            ecgLocRwave(peakNum,1)=newLoc;
            set(handles.txtECGInfo,'String',strcat('ECG Info: R peak',num2str(peakNum),' Moved.'));
        else
            set(handles.txtECGInfo,'String',strcat('ECG Info: R peak ',num2str(peakNum),' Invalid Location'));
        end
        
        
        
    elseif mPeakID ==13
        
        lowerLimit = ecgLocRwave(peakNum,1)
        
        upperLimit = ecgLocTwave(peakNum,1);
        newLoc =ceil(mPeakIDCord(1,1));
        if newLoc >=lowerLimit & newLoc < upperLimit
            ecgLocSwave(peakNum,1)=newLoc;
            set(handles.txtECGInfo,'String',strcat('ECG Info: S peak',num2str(peakNum),' Moved.'));
        else
            set(handles.txtECGInfo,'String',strcat('ECG Info: S peak ',num2str(peakNum),' Invalid Location'));
        end
        
        
    elseif mPeakID ==14
        
        lowerLimit = ecgLocSwave(peakNum,1)
        if peakNum < size(ecgLocTwave,1)
            upperLimit = ecgLocTwave(peakNum+1,1);
        else
            upperLimit = size(ecgDataProcessed,1);
        end
        newLoc =ceil(mPeakIDCord(1,1));
        if newLoc >=lowerLimit & newLoc < upperLimit
            ecgLocTwave(peakNum,1)=newLoc;
            set(handles.txtECGInfo,'String',strcat('ECG Info: S peak',num2str(peakNum),' Moved.'));
        else
            set(handles.txtECGInfo,'String',strcat('ECG Info: S peak ',num2str(peakNum),' Invalid Location'));
        end
    end
    %% clear moving peak ID
    mPeakID=-1;   
elseif id < 20
    mPeakID = id;
    
    
    if id == 11
        set(handles.txtECGInfo,'String','ECG Info: Moving Q Peak....');
        
        %mPeakIDCord=[coordinates(1,1) coordinates(1,2)];
        
        diff = abs(ecgLocQwave- mPeakIDCord(1,1));
        [~,peakNum]=min(diff);
        set(handles.txtECGInfo,'String',strcat('ECG Info: Moving Q Peak ',num2str(peakNum),'.....'));
    elseif id == 12
        diff = abs(ecgLocRwave- mPeakIDCord(1,1));
        [~,peakNum]=min(diff);
        set(handles.txtECGInfo,'String',strcat('ECG Info: Moving R Peak ',num2str(peakNum),'.....'));
        
        
    elseif id == 13
        diff = abs(ecgLocSwave- mPeakIDCord(1,1));
        [~,peakNum]=min(diff);
        set(handles.txtECGInfo,'String',strcat('ECG Info: Moving S Peak ',num2str(peakNum),'.....'));
        
        
    elseif id == 14
        diff = abs(ecgLocTwave- mPeakIDCord(1,1));
        [~,peakNum]=min(diff);
        set(handles.txtECGInfo,'String',strcat('ECG Info: Moving T Peak ',num2str(peakNum),'.....'));
        
    end
    set(handles.editDeleteRowID,'String',num2str(peakNum));
    
    
    
    
    
    
    
    
    
%% working with bvp1
 
elseif id == 20
    
    set(handles.txtBVP1Info,'String','BVP Info: Clicked....');
    %% add click info to add peak
    set(handles.editAddPeak,'String',num2str(mPeakIDCord(1,1)));
    %% add click info to delete by range 
    set(handles.chkECG,'Value',0);
    set(handles.chkBVP1,'Value',1);   
    set(handles.chkBVP2,'Value',0);
    %% adding range delete click info
    %currLowerLimit = str2num(get(handles.editDeleteSignalFrom,'String'));%
    
    if get(handles.chkLowerRange,'Value')==1
        set(handles.editDeleteSignalFrom,'String',num2str(mPeakIDCord(1,1)));
    else
        set(handles.editDeleteSignalTo,'String',num2str(mPeakIDCord(1,1)));
    end
   
    if mPeakID ==21
        lowerLimit = 1;
        if peakNum > 1
            lowerLimit = bvpLocSwave(peakNum-1,1);
        end
        upperLimit = bvpLocRwave(peakNum,1);
        newLoc =mPeakIDCord(1,1);
        if newLoc >=lowerLimit & newLoc < upperLimit
            bvpLocQwave(peakNum,1)=newLoc;
            set(handles.txtBVP1Info,'String',strcat('BVP1 Info: feet ',num2str(peakNum),' Moved.'));
        else
            set(handles.txtBVP1Info,'String',strcat('BVP1 Info: feet ',num2str(peakNum),' Invalid Location'));
        end
    elseif mPeakID ==22
        
        
        lowerLimit = bvpLocQwave(peakNum,1)
        upperLimit = bvpLocSwave(peakNum,1);
        newLoc =ceil(mPeakIDCord(1,1));
        if newLoc >lowerLimit & newLoc < upperLimit
            bvpLocRwave(peakNum,1)=newLoc;
            set(handles.txtBVP1Info,'String',strcat('BVP1 Info: peak ',num2str(peakNum),' Moved.'));
        else
            set(handles.txtBVP1Info,'String',strcat('BVP1 Info: peak ',num2str(peakNum),' Invalid Location'));
        end
             
        
    elseif mPeakID ==23
        
        lowerLimit = bvpLocRwave(peakNum,1)
        if peakNum < size(bvpLocSwave,1)
            upperLimit = bvpLocQwave(peakNum+1,1);
        else
            upperLimit = size(bvpDataProcessed,1);
        end
        newLoc =ceil(mPeakIDCord(1,1));
        if newLoc >=lowerLimit & newLoc < upperLimit
            bvpLocSwave(peakNum,1)=newLoc;
            set(handles.txtBVP1Info,'String',strcat('BVP1 Info: end ',num2str(peakNum),' Moved.'));
        else
            set(handles.txtBVP1Info,'String',strcat('BVP1 Info: end ',num2str(peakNum),' Invalid Location'));
        end
    end
    
    %% clear moving peak ID
    mPeakID=-1;
        
elseif id > 20 & id <30
    mPeakID = id;
    
    
    if id == 21
        set(handles.txtBVP1Info,'String','BVP Info: Moving feet....');
        
        %mPeakIDCord=[coordinates(1,1) coordinates(1,2)];
        
        diff = abs(bvpLocQwave- mPeakIDCord(1,1));
        [~,peakNum]=min(diff);
        set(handles.txtBVP1Info,'String',strcat('BVP1 Info: Moving feet ',num2str(peakNum),'.....'));
    elseif id == 22
        diff = abs(bvpLocRwave- mPeakIDCord(1,1));
        [~,peakNum]=min(diff);
        set(handles.txtBVP1Info,'String',strcat('BVP1 Info: Moving peak',num2str(peakNum),'.....'));
    
    elseif id == 23
        diff = abs(bvpLocSwave- mPeakIDCord(1,1));
        [~,peakNum]=min(diff);
        set(handles.txtBVP1Info,'String',strcat('BVP1 Info: Moving end ',num2str(peakNum),'.....'));
        
    end
    set(handles.editDeleteRowIDBVP,'String',num2str(peakNum));
    
  

    
    
    
    
    
    
    
    
    
 %% bvp2
 %% working with bvp1
 
elseif id == 30
    
    set(handles.txtBVP2Info,'String','BVP Info: Clicked....');
    %% add click info to add peak
    set(handles.editAddPeak,'String',num2str(mPeakIDCord(1,1)));
    %% add click info to delete by range 
    set(handles.chkECG,'Value',0);
    set(handles.chkBVP1,'Value',0);   
    set(handles.chkBVP2,'Value',1);
    %% adding range delete click info
    %currLowerLimit = str2num(get(handles.editDeleteSignalFrom,'String'));%
    
    if get(handles.chkLowerRange,'Value')==1
        set(handles.editDeleteSignalFrom,'String',num2str(mPeakIDCord(1,1)));
    else
        set(handles.editDeleteSignalTo,'String',num2str(mPeakIDCord(1,1)));
    end
   
    if mPeakID ==31
        lowerLimit = 1;
        if peakNum > 1
            lowerLimit = bvpLocSwave2(peakNum-1,1);
        end
        upperLimit = bvpLocRwave2(peakNum,1);
        newLoc =mPeakIDCord(1,1);
        if newLoc >=lowerLimit & newLoc < upperLimit
            bvpLocQwave2(peakNum,1)=newLoc;
            set(handles.txtBVP2Info,'String',strcat('BVP2 Info: feet ',num2str(peakNum),' Moved.'));
        else
            set(handles.txtBVP2Info,'String',strcat('BVP2 Info: feet ',num2str(peakNum),' Invalid Location'));
        end
    elseif mPeakID ==32
        
        
        lowerLimit = bvpLocQwave2(peakNum,1)
        upperLimit = bvpLocSwave2(peakNum,1);
        newLoc =ceil(mPeakIDCord(1,1));
        if newLoc >lowerLimit & newLoc < upperLimit
            bvpLocRwave2(peakNum,1)=newLoc;
            set(handles.txtBVP2Info,'String',strcat('BVP2 Info: peak ',num2str(peakNum),' Moved.'));
        else
            set(handles.txtBVP2Info,'String',strcat('BVP2 Info: peak ',num2str(peakNum),' Invalid Location'));
        end
             
        
    elseif mPeakID ==33
        
        lowerLimit = bvpLocRwave2(peakNum,1)
        if peakNum < size(bvpLocSwave2,1)
            upperLimit = bvpLocQwave2(peakNum+1,1);
        else
            upperLimit = size(bvpDataProcessed2,1);
        end
        newLoc =ceil(mPeakIDCord(1,1));
        if newLoc >=lowerLimit & newLoc < upperLimit
            bvpLocSwave2(peakNum,1)=newLoc;
            set(handles.txtBVP2Info,'String',strcat('BVP2 Info: end ',num2str(peakNum),' Moved.'));
        else
            set(handles.txtBVP2Info,'String',strcat('BVP2 Info: end ',num2str(peakNum),' Invalid Location'));
        end
    end
    
    %% clear moving peak ID
    mPeakID=-1;   
elseif id > 30 & id <40
    mPeakID = id;
    
    
    if id == 31
        set(handles.txtBVP2Info,'String','BVP2 Info: Moving feet....');
        
        %mPeakIDCord=[coordinates(1,1) coordinates(1,2)];
        
        diff = abs(bvpLocQwave2- mPeakIDCord(1,1));
        [~,peakNum]=min(diff);
        set(handles.txtBVP2Info,'String',strcat('BVP2 Info: Moving feet ',num2str(peakNum),'.....'));
    elseif id == 32
        diff = abs(bvpLocRwave2- mPeakIDCord(1,1));
        [~,peakNum]=min(diff);
        set(handles.txtBVP2Info,'String',strcat('BVP2 Info: Moving peak',num2str(peakNum),'.....'));
    
    elseif id == 33
        diff = abs(bvpLocSwave2- mPeakIDCord(1,1));
        [~,peakNum]=min(diff);
        set(handles.txtBVP2Info,'String',strcat('BVP1 Info: Moving end ',num2str(peakNum),'.....'));
        
    end
    set(handles.editDeleteRowIDBVP1,'String',num2str(peakNum));
  
end

displaySignal(handles)





function displayBVPData(handles)
%%% display BVP
global dataCuttOff
global bvpDataProcessed
global DataRaw;
global bvpLocRwave
global bvpLocSwave
global bvpLocQwave
global ax3
global ax4
global StartDisplayFrame;
global displayMatrix

global chkRange
%global DeletedSignals
global chkPeakFrame
global DeletedPeaksCol



totalSubGraph = sum(displayMatrix);

EndDisplayFrame = StartDisplayFrame+str2num(get(handles.editDWindowSize,'String'))-1;


try
    DeletedSignals = [cell2mat(DeletedPeaksCol(:,1)) cell2mat(DeletedPeaksCol(:,2))];
catch
    DeletedSignals=[];
end


if isempty(bvpDataProcessed)
    try
        bvpDataRaw = DataRaw(:,2);
    catch
        bvpDataRaw = DataRaw(:,1);
    end
    EndDisplayFrame = min(EndDisplayFrame,size(bvpDataRaw,1));
else
    try
        bvpDataRaw = dataCuttOff(:,2);
    catch
        bvpDataRaw = dataCuttOff(:,1);
    end
    EndDisplayFrame = min(EndDisplayFrame,size(bvpDataProcessed,1));
end


%% find any deleted signals within the range....
try
    
    
    %currDeletedSignals = DeletedSignals((DeletedSignals(:,2)>=StartDisplayFrame & DeletedSignals(:,2)<=EndDisplayFrame) | (DeletedSignals(:,3)>=StartDisplayFrame & DeletedSignals(:,3)<=EndDisplayFrame),:)
    currDeletedSignals = DeletedSignals((DeletedSignals(:,1)>=StartDisplayFrame & DeletedSignals(:,1)<=EndDisplayFrame) | (DeletedSignals(:,2)>=StartDisplayFrame & DeletedSignals(:,2)<=EndDisplayFrame),:)
    %% rEOMVOE THE DELETED POINTS OUT OF FRAME PROBLEM
    %currDeletedSignals(currDeletedSignals(:,2)<StartDisplayFrame,2)=StartDisplayFrame;
    currDeletedSignals(currDeletedSignals(:,1)<StartDisplayFrame,1)=StartDisplayFrame;
    %currDeletedSignals(currDeletedSignals(:,3)>EndDisplayFrame,3)=EndDisplayFrame;
    currDeletedSignals(currDeletedSignals(:,2)>EndDisplayFrame,2)=EndDisplayFrame;
catch
    currDeletedSignals=[];
end




if displayMatrix(1,3)==1
    
    
    
    
    currPlotID = sum(displayMatrix(1,1:2))+1;
    ax3 = subplot(totalSubGraph,1,currPlotID);
%     try
%         plot([[1:StartDisplayFrame-2 bvpDataRaw(StartDisplayFrame-1,1)]';bvpDataRaw(StartDisplayFrame:EndDisplayFrame,1)],'color','blue')
%     catch
%         plot([[1:StartDisplayFrame-1]';bvpDataRaw(StartDisplayFrame:EndDisplayFrame,1)],'color','blue')
%         
%     end
    plot(bvpDataRaw(1:EndDisplayFrame,1),'color','blue')
    hold on
    
    minVal = min(bvpDataRaw(StartDisplayFrame:EndDisplayFrame,1));
    maxVal = max(bvpDataRaw(StartDisplayFrame:EndDisplayFrame,1));
    %     if chkRange
    %
    %         fromFrame = str2num(get(handles.editDeleteSignalFrom,'String'));
    %         toFrame = str2num(get(handles.editDeleteSignalTo,'String'))
    %         plot([fromFrame fromFrame],[minVal maxVal],'color','red')
    %         plot([toFrame toFrame],[minVal maxVal],'color','red')
    %
    %         %% cross line
    %         plot([fromFrame toFrame],[maxVal minVal],'color','red')
    %         plot([fromFrame toFrame],[minVal maxVal],'color','red')
    %
    %     end
    %
    %
    %     %%% display the previously deleted signal
    %     for i=1:size(currDeletedSignals,1)
    %
    %         fromFrame = currDeletedSignals(i,2)-StartDisplayFrame;
    %         toFrame =  currDeletedSignals(i,3)-StartDisplayFrame;
    %         plot([fromFrame fromFrame],[minVal maxVal],'color','black','MarkerSize',5)
    %         plot([toFrame toFrame],[minVal maxVal],'color','black','MarkerSize',5)
    %
    %         %% cross line
    %         plot([fromFrame toFrame],[maxVal minVal],'color','black')
    %         plot([fromFrame toFrame],[minVal maxVal],'color','black')
    %     end
    ylabel('BVP1 Raw');
    
    axis([StartDisplayFrame,EndDisplayFrame,minVal,maxVal])
    %set(ax3,'Units','Pixels','Position',getpixelposition(ax3)-[0,0,FIGURE_FRAME_Y,0])
    
    gap = ceil((EndDisplayFrame-StartDisplayFrame)/30);
    set(gca,'XTick',[StartDisplayFrame:gap:EndDisplayFrame])
    
    
    
end


if displayMatrix(1,4)==1
    currPlotID = sum(displayMatrix(1,1:3))+1;
    ax4 = subplot(totalSubGraph,1,currPlotID);
%     try
%         plot([[1:StartDisplayFrame-2 bvpDataProcessed(StartDisplayFrame-1,1)]';bvpDataProcessed(StartDisplayFrame:EndDisplayFrame,1)],'ButtonDownFcn','color','green',{@TestMe, handles, 20});
%     catch
%         plot([[1:StartDisplayFrame-1]';bvpDataProcessed(StartDisplayFrame:EndDisplayFrame,1)],'color','green','ButtonDownFcn',{@TestMe, handles, 20});
%         
%     end
    plot(bvpDataProcessed(1:EndDisplayFrame,1),'color','green','ButtonDownFcn',{@TestMe, handles, 20});
    minVal = min(bvpDataProcessed(StartDisplayFrame:EndDisplayFrame,1));
    maxVal = max(bvpDataProcessed(StartDisplayFrame:EndDisplayFrame,1));
    
    %% if plot peak is on
    if get(handles.chkPlotPeak,'Value')==1
        hold on
        
        tmpLoc = [bvpLocQwave(bvpLocQwave(:,1)>= StartDisplayFrame & bvpLocQwave(:,1)<= EndDisplayFrame,1)];
        plot(tmpLoc,bvpDataProcessed(tmpLoc),'rs','ButtonDownFcn',{@TestMe, handles, 21});%,'MarkerFaceColor','g');
       
        
        
        % plot(bvpDataProcessed,'color','green');
        tmpLoc = [bvpLocRwave(bvpLocRwave(:,1)>= StartDisplayFrame & bvpLocRwave(:,1)<= EndDisplayFrame,1)];
        
        
        %% display number of plot
        rIndex=zeros(size(tmpLoc,1),1);
        for i=1:size(tmpLoc,1)
            rIndex(i,1) = find(bvpLocRwave(:,1)==tmpLoc(i,1));
        end
        
        global FONT_SIZE_PLOT_NO
        text(tmpLoc',bvpDataProcessed(tmpLoc),num2str(rIndex),'vert','bottom','horiz','center','FontSize', FONT_SIZE_PLOT_NO);
        plot(tmpLoc,bvpDataProcessed(tmpLoc),'rs','ButtonDownFcn',{@TestMe, handles,22});%,'MarkerFaceColor','r');
        
        
        
        
      
        tmpLoc = [bvpLocSwave(bvpLocSwave(:,1)>= StartDisplayFrame & bvpLocSwave(:,1)<= EndDisplayFrame,1)];
        plot(tmpLoc,bvpDataProcessed(tmpLoc),'rv','ButtonDownFcn',{@TestMe, handles, 23});%,'MarkerFaceColor','b');
        
        
        
        
        if chkRange
            
            fromFrame = str2num(get(handles.editDeleteSignalFrom,'String'));
            toFrame = str2num(get(handles.editDeleteSignalTo,'String'))
            
            plot([fromFrame fromFrame],[minVal maxVal],'color','red')
            plot([toFrame toFrame],[minVal maxVal],'color','red')
            
            %% cross line
            %plot([fromFrame toFrame],[maxVal minVal],'color','red')
            %plot([fromFrame toFrame],[minVal maxVal],'color','red')
            
        end
        
        %%% display the previously deleted signal
        for i=1:size(currDeletedSignals,1)
            
            fromFrame = currDeletedSignals(i,1);
            toFrame =  currDeletedSignals(i,2);
            plot([fromFrame fromFrame],[minVal maxVal],'color','black','MarkerSize',5)
            plot([toFrame toFrame],[minVal maxVal],'color','black','MarkerSize',5)
            
            %% cross line
            plot([fromFrame toFrame],[maxVal minVal],'color','black')
            plot([fromFrame toFrame],[minVal maxVal],'color','black')
        end
        
        
        %% display check addPeak
        if get(handles.chkBVP1,'Value')==1 & chkPeakFrame
           peakPos = ceil(str2num(get(handles.editAddPeak,'String')));
            
            %% no need to check the peak height
            %         %peakPosInPD = chkPeakFrame+StartDisplayFrame;
            %         QRdist = str2num(get(handles.editBVPQRDist,'String'));
            %         try
            %             %[ ~,adjFrames] = max(bvpDataProcessed(peakPosInPD-QRdist:peakPos+QRdist,1));
            %             [ ~,adjFrames] = max(bvpDataProcessed(peakPos-QRdist:peakPos+QRdist,1));
            %             if isempty(adjFrames)
            %                 peakAdjPos = peakPos;
            %             else
            %                 peakAdjPos=peakPos-QRdist+adjFrames;
            %             end
            %         catch
            %             peakAdjPos=peakPos;
            %         end
            %         set(handles.editAddPeak,'String',peakAdjPos)
            %         try
            %             plot([peakAdjPos peakAdjPos],[maxVal minVal],'-.','color','black')
            %         catch
            %             x=1
            %         end
            plot([peakPos peakPos],[maxVal minVal],'-.','color','black')
        end




    end
    ylabel('BVP1 P');
    %set(ax4,'Units','Pixels','Position',getpixelposition(ax4)-[0,0,FIGURE_FRAME_Y,0])
    axis([StartDisplayFrame,EndDisplayFrame,minVal*0.90,maxVal*1.10])
    
    
    gap = ceil((EndDisplayFrame-StartDisplayFrame)/30);
    set(gca,'XTick',[StartDisplayFrame:gap:EndDisplayFrame])
    
    
    
end


function displayBVPData1(handles)
%%% display BVP
global DataRaw
global bvpDataProcessed2
global dataCuttOff
global bvpLocRwave2
global bvpLocSwave2
global bvpLocQwave2
global StartDisplayFrame;
global ax5
global ax6
global chkRange

global displayMatrix
global DeletedPeaksCol;

global chkPeakFrame
global FONT_SIZE_PLOT_NO


totalSubGraph = sum(displayMatrix,2);

if isempty(bvpDataProcessed2)
    bvpDataRaw = DataRaw(:,3);
else
    bvpDataRaw = dataCuttOff(:,3);
end


try
    DeletedSignals = [ cell2mat(DeletedPeaksCol(:,1)) cell2mat(DeletedPeaksCol(:,2))];
catch
    DeletedSignals=[];
end


EndDisplayFrame = StartDisplayFrame+str2num(get(handles.editDWindowSize,'String'))-1;
EndDisplayFrame = min(EndDisplayFrame,size(bvpDataRaw,1));


%% find any deleted signals within the range....
try
    
    
    %currDeletedSignals = DeletedSignals((DeletedSignals(:,2)>=StartDisplayFrame & DeletedSignals(:,2)<=EndDisplayFrame) | (DeletedSignals(:,3)>=StartDisplayFrame & DeletedSignals(:,3)<=EndDisplayFrame),:)
    currDeletedSignals = DeletedSignals((DeletedSignals(:,1)>=StartDisplayFrame & DeletedSignals(:,1)<=EndDisplayFrame) | (DeletedSignals(:,2)>=StartDisplayFrame & DeletedSignals(:,2)<=EndDisplayFrame),:)
    %% rEOMVOE THE DELETED POINTS OUT OF FRAME PROBLEM
    %currDeletedSignals(currDeletedSignals(:,2)<StartDisplayFrame,2)=StartDisplayFrame;
    currDeletedSignals(currDeletedSignals(:,1)<StartDisplayFrame,1)=StartDisplayFrame;
    %currDeletedSignals(currDeletedSignals(:,3)>EndDisplayFrame,3)=EndDisplayFrame;
    currDeletedSignals(currDeletedSignals(:,2)>EndDisplayFrame,2)=EndDisplayFrame;
catch
    currDeletedSignals=[];
end





if displayMatrix(1,5)==1
    currPlotID = sum(displayMatrix(1,1:4))+1;
    
    ax5 = subplot(totalSubGraph,1,currPlotID);
    %  plot([[1:StartDisplayFrame-1]';bvpDataRaw(StartDisplayFrame:EndDisplayFrame,1)],'color','blue')
%     try
%         plot([[1:StartDisplayFrame-2 bvpDataRaw(StartDisplayFrame-1,1)]';bvpDataRaw(StartDisplayFrame:EndDisplayFrame,1)],'color','blue');
%     catch
%         plot([[1:StartDisplayFrame-1]';bvpDataRaw(StartDisplayFrame:EndDisplayFrame,1)],'color','blue');
%        
%     end
    plot(bvpDataRaw(1:EndDisplayFrame,1),'color','blue');
    hold on
    minVal = min(bvpDataRaw(StartDisplayFrame:EndDisplayFrame,1));
    maxVal = max(bvpDataRaw(StartDisplayFrame:EndDisplayFrame,1));
    
    
    %set(ax5,'Units','Pixels','Position',getpixelposition(ax5)-[0,0,FIGURE_FRAME_Y,0])
    ylabel('BVP2 Raw');
    axis([StartDisplayFrame,EndDisplayFrame,minVal*0.90,maxVal])
    
    gap = ceil((EndDisplayFrame-StartDisplayFrame)/30);
    set(gca,'XTick',[StartDisplayFrame:gap:EndDisplayFrame])
    
end

if displayMatrix(1,6)==1
    currPlotID = sum(displayMatrix(1,1:5))+1;
    
    %hold on
    %plot(bvpDataProcessed2(StartDisplayFrame:EndDisplayFrame,1),'color','green');
    ax6 = subplot(totalSubGraph,1,currPlotID);
%     try
%         plot([[1:StartDisplayFrame-2 bvpDataProcessed2(StartDisplayFrame-1,1)]';bvpDataProcessed2(StartDisplayFrame:EndDisplayFrame,1)],'color','green','ButtonDownFcn',{@TestMe, handles,30});
%     catch
%         plot([[1:StartDisplayFrame-1]';bvpDataProcessed2(StartDisplayFrame:EndDisplayFrame,1)],'color','green','ButtonDownFcn',{@TestMe, handles,30});
%         
%     end
    plot(bvpDataProcessed2(1:EndDisplayFrame,1),'color','green','ButtonDownFcn',{@TestMe, handles,30});
    minVal = min(bvpDataProcessed2(StartDisplayFrame:EndDisplayFrame,1));
    maxVal = max(bvpDataProcessed2(StartDisplayFrame:EndDisplayFrame,1));
    %% if plot peak is on
    if get(handles.chkPlotPeak,'Value')==1
        hold on
        
        
        tmpLoc = [bvpLocQwave2(bvpLocQwave2(:,1)>= StartDisplayFrame & bvpLocQwave2(:,1)<= EndDisplayFrame,1)];
        plot(tmpLoc,bvpDataProcessed2(tmpLoc),'rs','ButtonDownFcn',{@TestMe, handles,31});%,'MarkerFaceColor','g');
        
        
        
        % plot(bvpDataProcessed,'color','green');
        tmpLoc = [bvpLocRwave2(bvpLocRwave2(:,1)>= StartDisplayFrame & bvpLocRwave2(:,1)<= EndDisplayFrame,1)];
        
        
        rIndex=zeros(size(tmpLoc,1),1);
        for i=1:size(tmpLoc,1)
            rIndex(i,1) = find(bvpLocRwave2(:,1)==tmpLoc(i,1));
        end
        
        text(tmpLoc',bvpDataProcessed2(tmpLoc),num2str(rIndex),'vert','bottom','horiz','center','FontSize',FONT_SIZE_PLOT_NO);
        plot(tmpLoc,bvpDataProcessed2(tmpLoc),'rs','ButtonDownFcn',{@TestMe, handles,32});%,'MarkerFaceColor','r');
        
        
        tmpLoc = [bvpLocSwave2(bvpLocSwave2(:,1)>= StartDisplayFrame & bvpLocSwave2(:,1)<= EndDisplayFrame,1)];
        plot(tmpLoc,bvpDataProcessed2(tmpLoc),'rv','ButtonDownFcn',{@TestMe, handles,33});%,'MarkerFaceColor','b');
       
        
        
        
        if chkRange
            
            fromFrame = str2num(get(handles.editDeleteSignalFrom,'String'));
            toFrame = str2num(get(handles.editDeleteSignalTo,'String'))
            plot([fromFrame fromFrame],[minVal maxVal],'color','red')
            plot([toFrame toFrame],[minVal maxVal],'color','red')
            
            %         %% cross line
            %         plot([fromFrame toFrame],[maxVal minVal],'color','red')
            %         plot([fromFrame toFrame],[minVal maxVal],'color','red')
            
        end
        
        
        %%% display the previously deleted signal
        for i=1:size(currDeletedSignals,1)
            
            fromFrame = currDeletedSignals(i,1);
            toFrame =  currDeletedSignals(i,2);
            plot([fromFrame fromFrame],[minVal maxVal],'color','black','MarkerSize',5)
            plot([toFrame toFrame],[minVal maxVal],'color','black','MarkerSize',5)
            
            %% cross line
            plot([fromFrame toFrame],[maxVal minVal],'color','black')
            plot([fromFrame toFrame],[minVal maxVal],'color','black')
        end
        
        
        %% display check addPeak
        if get(handles.chkBVP2,'Value')==1 & chkPeakFrame
            peakPos = ceil(str2num(get(handles.editAddPeak,'String')));
            
            %peakPosInPD = chkPeakFrame+StartDisplayFrame;
            %% no need to check the peak height
            %         QRdist = str2num(get(handles.editBVP1QRDist,'String'));
            %         try
            %             %[ ~,adjFrames] = max(bvpDataProcessed2(peakPosInPD-QRdist:peakPos+QRdist,1));
            %             [ ~,adjFrames] = max(bvpDataProcessed2(peakPos-QRdist:peakPos+QRdist,1));
            %             if isempty(adjFrames)
            %                 peakAdjPos = peakPos;
            %             else
            %                 peakAdjPos=peakPos-QRdist+adjFrames;
            %             end
            %         catch
            %             peakAdjPos=peakPos;
            %         end
            %         set(handles.editAddPeak,'String',peakAdjPos)
            %         try
            %             plot([peakAdjPos peakAdjPos],[maxVal minVal],'-.','color','black')
            %         catch
            %             x=1
            %         end
            plot([peakPos peakPos],[maxVal minVal],'-.','color','black')
        end
    end
    ylabel('BVP2 P');
    axis([StartDisplayFrame,EndDisplayFrame,minVal,maxVal])
    
    gap = ceil((EndDisplayFrame-StartDisplayFrame)/30);
    set(gca,'XTick',[StartDisplayFrame:gap:EndDisplayFrame])
    
    %set(ax6,'Units','Pixels','Position',getpixelposition(ax6)-[0,0,FIGURE_FRAME_Y,0])
end








function edit26_Callback(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit26 as text
%        str2double(get(hObject,'String')) returns contents of edit26 as a double


% --- Executes during object creation, after setting all properties.
function edit26_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecgMedianFilter_FN_Callback(hObject, eventdata, handles)
% hObject    handle to ecgMedianFilter_FN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgMedianFilter_FN as text
%        str2double(get(hObject,'String')) returns contents of ecgMedianFilter_FN as a double


% --- Executes during object creation, after setting all properties.
function ecgMedianFilter_FN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecgMedianFilter_FN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecgRPeakHeight_Callback(hObject, eventdata, handles)
% hObject    handle to text011 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of text011 as text
%        str2double(get(hObject,'String')) returns contents of text011 as a double


% --- Executes during object creation, after setting all properties.
function text011_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text011 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function ecgRPeakHeight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecgRPeakHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on button press in pbPlayIt.
function pbPlayIt_Callback(hObject, eventdata, handles)
% hObject    handle to pbPlayIt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global t

if strcmp(get(hObject,'String'),'Play')
    t = timer;
    t.Period = 1;
    t.ExecutionMode = 'fixedRate';
    t.TimerFcn= @(~,thisEvent)displaySignal(handles);
    start(t)
    set(hObject,'String','Stop');
else
    delete(t);
    set(hObject,'String','Play');
end


% % --- Executes on mouse press over axes background.
% function axes1_ButtonDownFcn(hObject, eventdata, handles)
% % hObject    handle to axes1 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% global displayMatrix
% global movePoint
% disp( get(gca,'CurrentPoint'))
%
% if not(movePoint)
%     movePoint = True
%     prevMousePosition  = get(gca,'CurrentPoint')
% else
%     currMousePosition = get(gca,'CurrentPoint')
%     %% if this is ecg data
%     if (displayMatrix(1,7)==1)
%
%     end
%
% end
% if displayMatrix(1,7)==1
%     set()
% end
%
% disp('hi')



% --- Executes on key press with focus on pMenuSignalFiles and none of its controls.
function pMenuSignalFiles_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to pMenuSignalFiles (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)

viewRawData(handles)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over pMenuSignalFiles.
function pMenuSignalFiles_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to pMenuSignalFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
viewRawData(handles)


% --- Executes on button press in pbDisplayLast.
function pbDisplayLast_Callback(hObject, eventdata, handles)
% hObject    handle to pbDisplayLast (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global dataCuttOff
global StartDisplayFrame



StartDisplayFrame = size(dataCuttOff,1) - ceil(str2num(get(handles.editDWindowSize,'String')));
displaySignal(handles)
clearDeletedPeak()
set(handles.pbAddPeak,'enable','off')




% --- Executes on button press in pbDisplayFirst.
function pbDisplayFirst_Callback(hObject, eventdata, handles)
% hObject    handle to pbDisplayFirst (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global StartDisplayFrame

StartDisplayFrame = str2num( get(handles.editStartFrame,'String'));
displaySignal(handles)
clearDeletedPeak()
set(handles.pbAddPeak,'enable','off')


% --- Executes on button press in pbECGAResults.
function pbECGAResults_Callback(hObject, eventdata, handles)
% hObject    handle to pbECGAResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%run('ALterPointes.m')


% --- Executes on button press in pbAlterAVPPoints.
function pbAlterAVPPoints_Callback(hObject, eventdata, handles)
% hObject    handle to pbAlterAVPPoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% %
% %
% % % --- Executes on button press in pbECGViewChange.
% % function pbECGViewChange_Callback(hObject, eventdata, handles)
% % % hObject    handle to pbECGViewChange (see GCBO)
% % % eventdata  reserved - to be defined in a future version of MATLAB
% % % handles    structure with handles and user data (see GUIDATA)
% %
% %
% % global ecgDataProcessed
% %
% % uitablehandle = findall(handles.uiPDataTable,'type','uitable');
% %
% % tableData = get(uitablehandle,'Data')
% %
% %
% % if not(isempty(tableData))
% %
% %     ecgLocQwave=tableData(:,1);
% %     try
% %         ecgLocRwave = tableData(:,2);
% %         if size(ecgLocRwave,2)>1
% %             x=1
% %         end
% %     catch
% %         x=1
% %     end
% %     ecgLocSwave=tableData(:,3);
% %     ecgLocTwave=tableData(:,4);
% %
% %
% %
% %     global StartDisplayFrame
% %     global fig
% %     %% can not access more than size of ecgData size..
% %     EndDisplayFrame = min( StartDisplayFrame+str2num(get(handles.editDWindowSize,'String')),size(ecgDataProcessed,1));
% %
% %     try
% %         close(fig)
% %     catch
% %     end
% %     %fig = figure(1)
% %
% %
% %
% %     %% plot signals
% %     plot(ecgDataProcessed(StartDisplayFrame:EndDisplayFrame,1),'color','green');
% %
% %     hold on
% %
% %
% %
% %     %% plot QRST peaks...
% %
% %     plot(ecgLocQwave-StartDisplayFrame+1,ecgDataProcessed(ecgLocQwave),'rs','MarkerFaceColor','g')
% %     plot(ecgLocRwave-StartDisplayFrame+1,ecgDataProcessed(ecgLocRwave),'rs','MarkerFaceColor','r');
% %     plot(ecgLocSwave-StartDisplayFrame+1,ecgDataProcessed(ecgLocSwave),'rv','MarkerFaceColor','b');
% %     plot(ecgLocTwave-StartDisplayFrame+1,ecgDataProcessed(ecgLocTwave),'ro','MarkerFaceColor','g')
% % end
% %
% %
% %
% % % --- Executes on button press in pbECGViewChange.
% % function pbBVPViewChange_Callback(hObject, eventdata, handles)
% % % hObject    handle to pbECGViewChange (see GCBO)
% % % eventdata  reserved - to be defined in a future version of MATLAB
% % % handles    structure with handles and user data (see GUIDATA)
% %
% % global displayMatrix
% % global bvpDataProcessed
% % global StartDisplayFrame
% % global fig
% % %axes(handles.axes1)
% % displayMatrix = [0 0 0 0 0 0 0 1 0];
% % clearPlots();
% % uitablehandleBVP = findall(handles.uiPDataTableBVP,'type','uitable');
% % tableData = get(uitablehandleBVP,'Data')
% %
% %
% % if not(isempty(tableData))
% %
% %     bvpLocQwave=tableData(:,1);
% %     try
% %         bvpLocRwave = tableData(:,2);
% %         if size(bvpLocRwave,2)>1
% %             x=1
% %         end
% %     catch
% %         x=1
% %     end
% %     bvpLocSwave=tableData(:,3);
% %
% %
% %
% %
% %
% %     %% can not access more than size of ecgData size..
% %     EndDisplayFrame = min( StartDisplayFrame+str2num(get(handles.editDWindowSize,'String')),size(bvpDataProcessed,1));
% %
% %     try
% %         close(fig)
% %     catch
% %     end
% %
% %    % fig = figure(1)
% %     %% plot signals
% %     %h=findobj(handles.axes1,'axes','Tag','axes1');
% %
% %     plot(bvpDataProcessed(StartDisplayFrame:EndDisplayFrame,1),'color','green');
% %    % handles.axis1 = fig
% %     hold on
% %
% %
% %
% %     %% plot QRST peaks...
% %
% % %     plot(bvpLocQwave-StartDisplayFrame+1,bvpDataProcessed(bvpLocQwave),'rs','MarkerFaceColor','g')
% % %     plot(bvpLocRwave-StartDisplayFrame+1,bvpDataProcessed(bvpLocRwave),'rs','MarkerFaceColor','r');
% % %     plot(bvpLocSwave-StartDisplayFrame+1,bvpDataProcessed(bvpLocSwave),'rv','MarkerFaceColor','b');
% % %
% % end

% --- Executes on button press in pbECGViewChange.
function pbECGViewChange_Callback(hObject, eventdata, handles)
% hObject    handle to pbECGViewChange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


global ecgDataProcessed

uitablehandle = findall(handles.uiPDataTable,'type','uitable');

tableData = get(uitablehandle,'Data')


if not(isempty(tableData))
    
    ecgLocQwave=tableData(:,1);
    try
        ecgLocRwave = tableData(:,2);
        if size(ecgLocRwave,2)>1
            x=1
        end
    catch
        x=1
    end
    ecgLocSwave=tableData(:,3);
    ecgLocTwave=tableData(:,4);
    
    
    
    global StartDisplayFrame
    global fig
    %% can not access more than size of ecgData size..
    EndDisplayFrame = min( StartDisplayFrame+str2num(get(handles.editDWindowSize,'String')),size(ecgDataProcessed,1));
    
    try
        close(fig)
    catch
    end
    fig = figure(1)
    
    %% plot signals
    plot(ecgDataProcessed(StartDisplayFrame:EndDisplayFrame,1),'color','green');
    
    hold on
    
    
    
    %% plot QRST peaks...
    
    plot(ecgLocQwave-StartDisplayFrame+1,ecgDataProcessed(ecgLocQwave),'rs');%,'MarkerFaceColor','g')
    plot(ecgLocRwave-StartDisplayFrame+1,ecgDataProcessed(ecgLocRwave),'rs');%,'MarkerFaceColor','r');
    plot(ecgLocSwave-StartDisplayFrame+1,ecgDataProcessed(ecgLocSwave),'rv');%,'MarkerFaceColor','b');
    plot(ecgLocTwave-StartDisplayFrame+1,ecgDataProcessed(ecgLocTwave),'ro');%,'MarkerFaceColor','g')
end



% --- Executes on button press in pbECGViewChange.
function pbBVPViewChange_Callback(hObject, eventdata, handles)
% hObject    handle to pbECGViewChange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


global bvpDataProcessed

uitablehandleBVP = findall(handles.uiPDataTableBVP,'type','uitable');

tableData = get(uitablehandleBVP,'Data')


if not(isempty(tableData))
    
    bvpLocQwave=tableData(:,1);
    try
        bvpLocRwave = tableData(:,2);
        if size(bvpLocRwave,2)>1
            x=1
        end
    catch
        x=1
    end
    bvpLocSwave=tableData(:,3);
    
    
    
    
    global StartDisplayFrame
    global fig
    %% can not access more than size of ecgData size..
    EndDisplayFrame = min( StartDisplayFrame+str2num(get(handles.editDWindowSize,'String')),size(bvpDataProcessed,1));
    
    try
        close(fig)
    catch
    end
    
    fig = figure(1)
    %% plot signals
    plot(bvpDataProcessed(StartDisplayFrame:EndDisplayFrame,1),'color','green');
    
    hold on
    
    
    
    %% plot QRST peaks...
    
    plot(bvpLocQwave-StartDisplayFrame+1,bvpDataProcessed(bvpLocQwave),'rs');%,'MarkerFaceColor','g')
    plot(bvpLocRwave-StartDisplayFrame+1,bvpDataProcessed(bvpLocRwave),'rs');%,'MarkerFaceColor','r');
    plot(bvpLocSwave-StartDisplayFrame+1,bvpDataProcessed(bvpLocSwave),'rv');%,'MarkerFaceColor','b');
    
end




% --- Executes on button press in pbECGKeepChange.
function pbECGKeepChange_Callback(hObject, eventdata, handles)
% hObject    handle to pbECGKeepChange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global ecgLocQwave
global ecgLocRwave
global ecgLocSwave
global ecgLocTwave

uitablehandle = findall(handles.uiPDataTable,'type','uitable');
tableData = get(uitablehandle,'Data')


%%% update the value in table to peaks..

for i=1:size(tableData,1)
    %% the fifth column contain row ID value...
    try
        ecgLocQwave(tableData(i,5),1)=tableData(i,1);
        ecgLocRwave(tableData(i,5),1)=tableData(i,2);
        ecgLocSwave(tableData(i,5),1)=tableData(i,3);
        ecgLocTwave(tableData(i,5),1)=tableData(i,4);
    catch
        x=1
    end
    
end
displaySignal(handles)




% --- Executes on button press in pbECGKeepChange.
function pbBVPKeepChange_Callback(hObject, eventdata, handles)
% hObject    handle to pbECGKeepChange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


global bvpLocQwave
global bvpLocRwave
global bvpLocSwave


uitablehandleBVP = findall(handles.uiPDataTableBVP,'type','uitable');
tableData = get(uitablehandleBVP,'Data')


%%% update the value in table to peaks..

for i=1:size(tableData,1)
    %% the forth column contain row ID value...
    try
        bvpLocQwave(tableData(i,4),1)=tableData(i,1);
        bvpLocRwave(tableData(i,4),1)=tableData(i,2);
        bvpLocSwave(tableData(i,4),1)=tableData(i,3);
        
    catch
        x=1
    end
    
end
displaySignal(handles)








% --- Executes on button press in pbECGDeleteRow.
function pbECGDeleteRow_Callback(hObject, eventdata, handles)
% hObject    handle to pbECGDeleteRow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


global ecgLocQwave
global ecgLocRwave
global ecgLocSwave
global ecgLocTwave
global deletedECGPeak


uitablehandle = findall(handles.uiPDataTable,'type','uitable');
tableData = get(uitablehandle,'Data')


rowID = str2num(get(handles.editDeleteRowID,'String'));

%% delete the row
if rowID >=tableData(1,5) & rowID <=tableData(end,5)
    %     ecgLocQwave(tableData(rowID,5),:)=[];
    %     ecgLocRwave(tableData(rowID,5),:)=[];
    %     ecgLocSwave(tableData(rowID,5),:)=[];
    %     ecgLocTwave(tableData(rowID,5),:)=[];
    
    %% keep record of currently deleted peak
    deletedECGPeak=[ecgLocQwave(rowID,:) ecgLocRwave(rowID,:) ecgLocSwave(rowID,:) ecgLocTwave(rowID,:);deletedECGPeak];
    ecgLocQwave(rowID,:)=[];
    ecgLocRwave(rowID,:)=[];
    ecgLocSwave(rowID,:)=[];
    ecgLocTwave(rowID,:)=[];
    
    
    
    
    displaySignal(handles)
else
    msgbox('Row ID out of view point');
end

% --- Executes on button press in pbBVPDeleteRow.
function pbBVPDeleteRow_Callback(hObject, eventdata, handles)
% hObject    handle to pbBVPDeleteRow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bvpLocQwave
global bvpLocRwave
global bvpLocSwave
global deletedBVP1Peak


uitablehandleBVP = findall(handles.uiPDataTableBVP,'type','uitable');
tableData = get(uitablehandleBVP,'Data');
rowID = str2num(get(handles.editDeleteRowIDBVP,'String'));


if rowID > 0 & rowID >=tableData(1,4) & rowID <=tableData(end,4)
    deletedBVP1Peak=[bvpLocQwave(rowID,:) bvpLocRwave(rowID,:) bvpLocSwave(rowID,:) ;deletedBVP1Peak];
    bvpLocQwave(rowID,:)=[];
    bvpLocRwave(rowID,:)=[];
    bvpLocSwave(rowID,:)=[];
    displaySignal(handles);
else
    msgbox('Row ID out of view point');
end


% if rowID >=tableData(1,5) & rowID <=tableData(end,5)
% %     ecgLocQwave(tableData(rowID,5),:)=[];
% %     ecgLocRwave(tableData(rowID,5),:)=[];
% %     ecgLocSwave(tableData(rowID,5),:)=[];
% %     ecgLocTwave(tableData(rowID,5),:)=[];
%
%      %% keep record of currently deleted peak
%      deletedECGPeak=[ecgLocQwave(rowID,:) ecgLocRwave(rowID,:) ecgLocSwave(rowID,:) ecgLocTwave(rowID,:);deletedECGPeak];
%      ecgLocQwave(rowID,:)=[];
%      ecgLocRwave(rowID,:)=[];
%      ecgLocSwave(rowID,:)=[];
%      ecgLocTwave(rowID,:)=[];
%
%
%
%
%     displaySignal(handles)
% else
%     msgbox('Row ID out of view point');
% end






function editDeleteRowID_Callback(hObject, eventdata, handles)
% hObject    handle to editDeleteRowID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editDeleteRowID as text
%        str2double(get(hObject,'String')) returns contents of editDeleteRowID as a double


% --- Executes during object creation, after setting all properties.
function editDeleteRowID_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDeleteRowID (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton23.
function pushbutton23_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit34_Callback(hObject, eventdata, handles)
% hObject    handle to edit34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit34 as text
%        str2double(get(hObject,'String')) returns contents of edit34 as a double


% --- Executes during object creation, after setting all properties.
function edit34_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editDeleteRowIDBVP_Callback(~, eventdata, handles)
% hObject    handle to editDeleteRowIDBVP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editDeleteRowIDBVP as text
%        str2double(get(hObject,'String')) returns contents of editDeleteRowIDBVP as a double


% --- Executes during object creation, after setting all properties.
function editDeleteRowIDBVP_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDeleteRowIDBVP (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end










% --- Executes on button press in pushbutton27.
function pushbutton27_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit36_Callback(hObject, eventdata, handles)
% hObject    handle to edit36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit36 as text
%        str2double(get(hObject,'String')) returns contents of edit36 as a double


% --- Executes during object creation, after setting all properties.
function edit36_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editDeleteRowIDBVP1_Callback(hObject, eventdata, handles)
% hObject    handle to editDeleteRowIDBVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editDeleteRowIDBVP1 as text
%        str2double(get(hObject,'String')) returns contents of editDeleteRowIDBVP1 as a double


% --- Executes during object creation, after setting all properties.
function editDeleteRowIDBVP1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDeleteRowIDBVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbBVP1DeleteRow.
function pbBVP1DeleteRow_Callback(hObject, eventdata, handles)
% hObject    handle to pbBVP1DeleteRow (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bvpLocQwave2
global bvpLocRwave2
global bvpLocSwave2
global deletedBVP2Peak





uitablehandleBVP1 = findall(handles.uiPDataTableBVP1,'type','uitable');
tableData = get(uitablehandleBVP1,'Data');
rowID = str2num(get(handles.editDeleteRowIDBVP1,'String'));

% %% delete the row
% if rowID >=1 & rowID <=size(tableData,1)
%     bvpLocQwave2(tableData(rowID,4),:)=[];
%     bvpLocRwave2(tableData(rowID,4),:)=[];
%     bvpLocSwave2(tableData(rowID,4),:)=[];
%     displaySignal(handles)
% end


if rowID > 0 & rowID >=tableData(1,4) & rowID <=tableData(end,4)
    deletedBVP2Peak=[bvpLocQwave2(rowID,:) bvpLocRwave2(rowID,:) bvpLocSwave2(rowID,:);deletedBVP2Peak ];
    bvpLocQwave2(rowID,:)=[];
    bvpLocRwave2(rowID,:)=[];
    bvpLocSwave2(rowID,:)=[];
    
    displaySignal(handles)
else
    msgbox('Row ID out of view point');
end






% --- Executes on button press in pbBVP1KeepChange.
function pbBVP1KeepChange_Callback(hObject, eventdata, handles)
% hObject    handle to pbBVP1KeepChange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% hObject    handle to pbECGKeepChange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


global bvpLocQwave2
global bvpLocRwave2
global bvpLocSwave2


uitablehandleBVP1 = findall(handles.uiPDataTableBVP1,'type','uitable');
tableData = get(uitablehandleBVP1,'Data')


%%% update the value in table to peaks..

for i=1:size(tableData,1)
    %% the forth column contain row ID value...
    try
        bvpLocQwave2(tableData(i,4),1)=tableData(i,1);
        bvpLocRwave2(tableData(i,4),1)=tableData(i,2);
        bvpLocSwave2(tableData(i,4),1)=tableData(i,3);
        
    catch
        x=1
    end
    
end
displaySignal(handles)



function sayHi(hObject,eventdata,handles,eventID)
global prevPoints
global prevEventID

global bvpLocQwave2
global bvpLocRwave2
global bvpLocSwave2
global StartDisplayFrame
global bvpDataProcessed2
global h0 h1 h2 h3


uitablehandleBVP = findall(handles.uiPDataTableBVP1,'type','uitable');
tableData = get(uitablehandleBVP,'Data')


disp('Hi bro')
currPoints = get (gca, 'CurrentPoint')
x = currPoints(1,1)
y = currPoints(1,2)

CURSOR_TYPE = {'arrow','fleur','circle','cross','crosshair'};

if eventID == 0
    if prevEventID == 1
        disp('Move Q')
        disp([prevPoints x y])
        
        
        
    end
    if prevEventID == 2
        disp('Move R')
        disp([prevPoints x y])
        %% find the closest point to move
        %% substrat the startPoint as well
        %            [~,ind]=min(abs(bvpLocRwave2(:,1)-(StartDisplayFrame+prevPoints(1,1))))
        %            tblInd = find(tableData(:,2)==bvpLocRwave2(ind,1))
        %            bvpLocRwave2(ind,1)=x+StartDisplayFrame;
        %            tableData(tblInd,2)=x+StartDisplayFrame;
        %            set(uitablehandleBVP,'Data',tableData)
        global RList
        
        RList(abs(RList-prevPoints(1,1))<10,1)=x;
        cla(h2)
        h2=plot(RList,bvpDataProcessed2(RList+StartDisplayFrame),'rs');%,'MarkerFaceColor','r');
        % h2 = plot(RList,bvpDataProcessed2(RList+StartDisplayFrame)
        %  pbBVP1ViewChange_Callback(hObject, eventdata, handles)
    end
    if prevEventID == 3
        disp('Move S')
        disp([prevPoints x y])
        
    end
    if prevEventID == 4
        disp('Move T')
        disp([prevPoints x y])
        
    end
    set(gcf,'Pointer',CURSOR_TYPE{1});
    
    %       displaySignal(handles)
    
    
    
    
    
    
    
    
    
    
    
    
    
else
    prevPoints = [x,y];
    prevEventID = eventID;
    
    
    set(gcf,'Pointer',CURSOR_TYPE{eventID+1})
    
    
    
    
    
    
end









% % % --- Executes on button press in pbBVP1ViewChange.
% % function pbBVP1ViewChange_Callback(hObject, eventdata, handles)
% % % hObject    handle to pbBVP1ViewChange (see GCBO)
% % % eventdata  reserved - to be defined in a future version of MATLAB
% % % handles    structure with handles and user data (see GUIDATA)
% %
% % global bvpDataProcessed2
% %
% % global prevEventID
% %
% % prevEventID = -1;
% %
% % uitablehandleBVP1 = findall(handles.uiPDataTableBVP1,'type','uitable');
% %
% % tableData = get(uitablehandleBVP1,'Data')
% %
% %
% % if not(isempty(tableData))
% %
% %     bvpLocQwave=tableData(:,1);
% %     try
% %         bvpLocRwave = tableData(:,2);
% %         if size(bvpLocRwave,2)>1
% %             x=1
% %         end
% %     catch
% %         x=1
% %     end
% %     bvpLocSwave=tableData(:,3);
% %
% %
% %
% %
% %     global StartDisplayFrame
% %     %global fig
% %     %% can not access more than size of ecgData size..
% %     EndDisplayFrame = min( StartDisplayFrame+str2num(get(handles.editDWindowSize,'String')),size(bvpDataProcessed2,1));
% %
% %     try
% %         close(fig)
% %     catch
% %     end
% %
% %     %fig = figure(1)
% %    % ax = axes;
% %
% %     %addlistener(fig,'ButtonDownFcn',@sayHi)
% %     %% plot signals
% %     global RList
% %     global h1 h2 h3 h0
% %     subplot(1,1,1)
% %     h0=plot(bvpDataProcessed2(StartDisplayFrame:EndDisplayFrame,1),'color','green');
% %
% %     hold on
% %
% %
% %
% %     %% plot QRST peaks...
% %
% %
% %     RList = bvpLocRwave-StartDisplayFrame+1;
% %
% %     h1=plot(bvpLocQwave-StartDisplayFrame+1,bvpDataProcessed2(bvpLocQwave),'rs','MarkerFaceColor','g')
% %     h2=plot(bvpLocRwave-StartDisplayFrame+1,bvpDataProcessed2(bvpLocRwave),'rs','MarkerFaceColor','r');
% %     h3=plot(bvpLocSwave-StartDisplayFrame+1,bvpDataProcessed2(bvpLocSwave),'rv','MarkerFaceColor','b');
% %
% %
% %
% %
% %
% %    % set(fig,'ButtonDownFcn',@(~,~)disp('figure'))
% %     %%set(h0,'ButtonDownFcn',@(ho,eventdata)sayHi(ho,eventdata))
% %     %%set(h1,'ButtonDownFcn',@(~,~)disp('h1'))
% %
% %     %set(h0,'ButtonDownFcn',{@sayHi,handles,0})
% %     %set(h1, 'ButtonDownFcn', {@sayHi,handles,1});
% %     %set(h2, 'ButtonDownFcn', {@sayHi,handles,2});
% %     %set(h3, 'ButtonDownFcn', {@sayHi,handles,3});
% %
% %
% %
% % end
% %




% --- Executes on button press in pbBVP1ViewChange.
function pbBVP1ViewChange_Callback(hObject, eventdata, handles)
% hObject    handle to pbBVP1ViewChange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global bvpDataProcessed2

uitablehandleBVP1 = findall(handles.uiPDataTableBVP1,'type','uitable');

tableData = get(uitablehandleBVP1,'Data')


if not(isempty(tableData))
    
    bvpLocQwave=tableData(:,1);
    try
        bvpLocRwave = tableData(:,2);
        if size(bvpLocRwave,2)>1
            x=1
        end
    catch
        x==1
    end
    bvpLocSwave=tableData(:,3);
    
    
    
    
    global StartDisplayFrame
    global fig
    %% can not access more than size of ecgData size..
    EndDisplayFrame = min( StartDisplayFrame+str2num(get(handles.editDWindowSize,'String')),size(bvpDataProcessed2,1));
    
    try
        close(fig)
    catch
    end
    
    fig = figure(1)
    %% plot signals
    plot(bvpDataProcessed2(StartDisplayFrame:EndDisplayFrame,1),'color','green');
    
    hold on
    
    
    
    %% plot QRST peaks...
    
    plot(bvpLocQwave-StartDisplayFrame+1,bvpDataProcessed2(bvpLocQwave),'rs');%,'MarkerFaceColor','g')
    plot(bvpLocRwave-StartDisplayFrame+1,bvpDataProcessed2(bvpLocRwave),'rs');%,'MarkerFaceColor','r');
    plot(bvpLocSwave-StartDisplayFrame+1,bvpDataProcessed2(bvpLocSwave),'rv');%,'MarkerFaceColor','b');
    
end




% --- Executes on button press in pushbutton31.
function pushbutton31_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit42_Callback(hObject, eventdata, handles)
% hObject    handle to edit42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit42 as text
%        str2double(get(hObject,'String')) returns contents of edit42 as a double


% --- Executes during object creation, after setting all properties.
function edit42_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function bvp1MedianFilter_FN_Callback(hObject, eventdata, handles)
% hObject    handle to bvp1MedianFilter_FN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bvp1MedianFilter_FN as text
%        str2double(get(hObject,'String')) returns contents of bvp1MedianFilter_FN as a double


% --- Executes during object creation, after setting all properties.
function bvp1MedianFilter_FN_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bvp1MedianFilter_FN (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editBVP1RSDist_Callback(hObject, ~, handles)
% hObject    handle to editBVP1RSDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBVP1RSDist as text
%        str2double(get(hObject,'String')) returns contents of editBVP1RSDist as a double


% --- Executes during object creation, after setting all properties.
function editBVP1RSDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBVP1RSDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function bvp1MinPeakDist_Callback(hObject, eventdata, handles)
% hObject    handle to bvp1MinPeakDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of bvp1MinPeakDist as text
%        str2double(get(hObject,'String')) returns contents of bvp1MinPeakDist as a double


% --- Executes during object creation, after setting all properties.
function bvp1MinPeakDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bvp1MinPeakDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editBVP1QRDist_Callback(hObject, eventdata, handles)
% hObject    handle to editBVP1QRDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBVP1QRDist as text
%        str2double(get(hObject,'String')) returns contents of editBVP1QRDist as a double


% --- Executes during object creation, after setting all properties.
function editBVP1QRDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBVP1QRDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editBVP1MinPeakHeight_Callback(hObject, eventdata, handles)
% hObject    handle to editBVP1MinPeakHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBVP1MinPeakHeight as text
%        str2double(get(hObject,'String')) returns contents of editBVP1MinPeakHeight as a double


% --- Executes during object creation, after setting all properties.
function editBVP1MinPeakHeight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBVP1MinPeakHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editBVPMinPeakHeight_Callback(hObject, eventdata, handles)
% hObject    handle to editBVPMinPeakHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editBVPMinPeakHeight as text
%        str2double(get(hObject,'String')) returns contents of editBVPMinPeakHeight as a double


% --- Executes during object creation, after setting all properties.
function editBVPMinPeakHeight_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editBVPMinPeakHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton32.
function pushbutton32_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton34.
function pushbutton34_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over axes background.
function updateAxes_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to updateAxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pbDisplayRawECG.
function pbDisplayRawECG_Callback(hObject, eventdata, handles)
% hObject    handle to pbDisplayRawECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global displayMatrix
displayMatrix=displayMatrix+[1 0 0 0 0 0 ];
displayMatrix = mod(displayMatrix , 2);
%displayData(handles)
setDisplayColor(handles)
displaySignal(handles)


% --- Executes on button press in pbDisplayProcessECG.
function pbDisplayProcessECG_Callback(hObject, eventdata, handles)
% hObject    handle to pbDisplayProcessECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global displayMatrix
displayMatrix=displayMatrix+[0 1 0 0 0 0 ];
displayMatrix = mod(displayMatrix , 2);
setDisplayColor(handles)
displaySignal(handles)



% --- Executes on button press in pbDisplayRawBVP1.
function pbDisplayRawBVP1_Callback(hObject, eventdata, handles)
% hObject    handle to pbDisplayRawBVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global displayMatrix
displayMatrix=displayMatrix+[0 0 1 0 0 0];
displayMatrix = mod(displayMatrix , 2);
setDisplayColor(handles);
displaySignal(handles);




% --- Executes on button press in pbDisplayProcessBVP1.
function pbDisplayProcessBVP1_Callback(~, eventdata, handles)
% hObject    handle to pbDisplayProcessBVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global displayMatrix
displayMatrix=displayMatrix+[0 0 0 1 0 0];
displayMatrix = mod(displayMatrix , 2);
setDisplayColor(handles)
displaySignal(handles)



% --- Executes on button press in pbCheckRange.
function pbCheckRange_Callback(hObject, eventdata, handles)
% hObject    handle to pbCheckRange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global displayMatrix
global chkRange
dFrameFrom = str2num(get(handles.editDeleteSignalFrom,'String'));
dFrameTo = str2num(get(handles.editDeleteSignalTo,'String'));
chkRange = true;
displaySignal(handles)

set(handles.pbDeleteSignals,'Enable','on');
set(handles.pbDeleteSignals,'BackgroundColor',[0.39,0.83,0.07]);




% --- Executes on button press in pbDisplayRawBVP2.
function pbDisplayRawBVP2_Callback(hObject, eventdata, handles)
% hObject    handle to pbDisplayRawBVP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global displayMatrix
displayMatrix=displayMatrix+[0 0 0 0 1 0 ];
displayMatrix = mod(displayMatrix , 2);
setDisplayColor(handles)
displaySignal(handles)


% --- Executes on button press in pbDisplayProcessBVP2.
function pbDisplayProcessBVP2_Callback(hObject, eventdata, handles)
% hObject    handle to pbDisplayProcessBVP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global displayMatrix
displayMatrix=displayMatrix+[0 0 0 0 0 1];
displayMatrix = mod(displayMatrix , 2);
setDisplayColor(handles)
displaySignal(handles)



function editDeleteSignalFrom_Callback(hObject, eventdata, handles)
% hObject    handle to editDeleteSignalFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editDeleteSignalFrom as text
%        str2double(get(hObject,'String')) returns contents of editDeleteSignalFrom as a double


% --- Executes during object creation, after setting all properties.
function editDeleteSignalFrom_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDeleteSignalFrom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editDeleteSignalTo_Callback(hObject, eventdata, handles)
% hObject    handle to editDeleteSignalTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editDeleteSignalTo as text
%        str2double(get(hObject,'String')) returns contents of editDeleteSignalTo as a double



% --- Executes during object creation, after setting all properties.
function editBVPRSDist_Callback(hObject, eventdata, handles)
% hObject    handle to editDeleteSignalTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



% --- Executes during object creation, after setting all properties.
function editDeleteSignalTo_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editDeleteSignalTo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbDeleteSignals.
function pbDeleteSignals_Callback(hObject, eventdata, handles)
% hObject    handle to pbDeleteSignals (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ecgLocRwave
global ecgLocQwave
global ecgLocTwave
global ecgLocSwave

global bvpLocRwave
global bvpLocSwave
global bvpLocQwave

global bvpLocRwave2
global bvpLocSwave2
global bvpLocQwave2

global DeletedPeaksCol

% global DeletedECGWave
% global DeletedBVPWave
% global DeletedBVPWave2

fromFrame = str2num(get(handles.editDeleteSignalFrom,'String'));
toFrame = str2num(get(handles.editDeleteSignalTo,'String'));


% DeletedSignals=[DeletedSignals;size(DeletedSignals,1)+1 fromFrame toFrame];
%
% [~,idx] = sort(DeletedSignals(:,2)); % sort just the first column
% %% sort the deleted signals
% DeletedSignals = [[1:size(DeletedSignals,1)]' DeletedSignals(idx,[2,3])];


DeletedPeaksCol{size(DeletedPeaksCol,1)+1,1}=fromFrame;
DeletedPeaksCol{size(DeletedPeaksCol,1),2}=toFrame;

% DeletedECGWave{size(DeletedSignals,1),1}=ecgLocQwave(ecgLocQwave(:,1)>=fromFrame+StartDisplayFrame & ecgLocQwave(:,1)<=toFrame+StartDisplayFrame ,:);
% DeletedECGWave{size(DeletedSignals,1),2}=ecgLocRwave(ecgLocRwave(:,1)>=fromFrame+StartDisplayFrame & ecgLocRwave(:,1)<=toFrame+StartDisplayFrame ,:);
% DeletedECGWave{size(DeletedSignals,1),3}=ecgLocSwave(ecgLocSwave(:,1)>=fromFrame+StartDisplayFrame & ecgLocSwave(:,1)<=toFrame+StartDisplayFrame ,:);
% DeletedECGWave{size(DeletedSignals,1),4}=ecgLocTwave(ecgLocTwave(:,1)>=fromFrame+StartDisplayFrame & ecgLocTwave(:,1)<=toFrame+StartDisplayFrame ,:);
%

%% delete data from ECG
if size(ecgLocRwave,1)>0
    delECGIndex= find(ecgLocRwave(:,1)>=fromFrame & ecgLocRwave(:,1)<=toFrame);
    DeletedPeaksCol{size(DeletedPeaksCol,1),3}=[ecgLocQwave(delECGIndex,1) ecgLocRwave(delECGIndex,1) ecgLocSwave(delECGIndex,1) ecgLocTwave(delECGIndex,1)];
    ecgLocQwave(delECGIndex,:)=[];
    ecgLocRwave(delECGIndex,:)=[];
    ecgLocSwave(delECGIndex,:)=[];
    ecgLocTwave(delECGIndex,:)=[];
end



% DeletedBVPWave{size(DeletedSignals,1),1}=bvpLocQwave(bvpLocQwave(:,1)>=fromFrame+StartDisplayFrame & bvpLocQwave(:,1)<=toFrame+StartDisplayFrame ,:);
% DeletedBVPWave{size(DeletedSignals,1),2}=bvpLocRwave(bvpLocRwave(:,1)>=fromFrame+StartDisplayFrame & bvpLocRwave(:,1)<=toFrame+StartDisplayFrame ,:);
% DeletedBVPWave{size(DeletedSignals,1),3}=bvpLocSwave(bvpLocSwave(:,1)>=fromFrame+StartDisplayFrame & bvpLocSwave(:,1)<=toFrame+StartDisplayFrame ,:);

if size(bvpLocRwave,1)>0

    delBVP1Index= find(bvpLocRwave(:,1)>=fromFrame & bvpLocRwave(:,1)<=toFrame);
    DeletedPeaksCol{size(DeletedPeaksCol,1),4}=[bvpLocQwave(delBVP1Index,1) bvpLocRwave(delBVP1Index,1) bvpLocSwave(delBVP1Index,1)];
    bvpLocQwave(delBVP1Index,:)=[];
    bvpLocRwave(delBVP1Index,:)=[];
    bvpLocSwave(delBVP1Index,:)=[];
end

% DeletedBVPWave2{size(DeletedSignals,1),1}=bvpLocQwave2(bvpLocQwave2(:,1)>=fromFrame+StartDisplayFrame & bvpLocQwave2(:,1)<=toFrame+StartDisplayFrame ,:);
% DeletedBVPWave2{size(DeletedSignals,1),2}=bvpLocRwave2(bvpLocRwave2(:,1)>=fromFrame+StartDisplayFrame & bvpLocRwave2(:,1)<=toFrame+StartDisplayFrame ,:);
% DeletedBVPWave2{size(DeletedSignals,1),3}=bvpLocSwave2(bvpLocSwave2(:,1)>=fromFrame+StartDisplayFrame & bvpLocSwave2(:,1)<=toFrame+StartDisplayFrame ,:);

if size(bvpLocRwave2,1)>0
    delBVP2Index= find(bvpLocRwave2(:,1)>=fromFrame & bvpLocRwave2(:,1)<=toFrame);
    DeletedPeaksCol{size(DeletedPeaksCol,1),5}=[bvpLocQwave2(delBVP2Index,1) bvpLocRwave2(delBVP2Index,1) bvpLocSwave2(delBVP2Index,1)];
    bvpLocQwave2(delBVP2Index,:)=[];
    bvpLocRwave2(delBVP2Index,:)=[];
    bvpLocSwave2(delBVP2Index,:)=[];
end



%% sort the deleted col

[~, idx] = sort(cell2mat(DeletedPeaksCol(:,2)));
DeletedPeaksCol = DeletedPeaksCol(idx,:);

set(handles.pbDeleteSignals,'Enable','off');
set(handles.pbDeleteSignals,'BackgroundColor',[0.94,0.6,0.46])

displaySignal(handles)
addRangeInMenu(handles)

function addRangeInMenu(handles)
%global DeletedSignals

global DeletedPeaksCol
DeletedSignals = [cell2mat(DeletedPeaksCol(:,1)) cell2mat(DeletedPeaksCol(:,2))];

DelVal={'Select one of Range'};
%for i=1:size(DeletedSignals,1)
for i=1:size(DeletedPeaksCol,1)
    %DelVal{1,i+1}= strcat(num2str(DeletedSignals(i,2)),'___',num2str(DeletedSignals(i,3)));
    DelVal{1,i+1}=strcat(num2str(DeletedPeaksCol{i,1}),'___',num2str(DeletedPeaksCol{i,2}))
end
set(handles.pMenuDeletedRange,'Value',1)
set(handles.pMenuDeletedRange,'String',DelVal)



% --- Executes on button press in chkECG.
function chkECG_Callback(hObject, eventdata, handles)
% hObject    handle to chkECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkECG

set(handles.chkBVP1,'Value',false);
set(handles.chkBVP2,'Value',false);
drawnow


% --- Executes on button press in chkBVP1.
function chkBVP1_Callback(hObject, eventdata, handles)
% hObject    handle to chkBVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkBVP1

set(handles.chkECG,'Value',false);
set(handles.chkBVP2,'Value',false);
drawnow


% --- Executes on button press in chkBVP2.
function chkBVP2_Callback(hObject, eventdata, handles)
% hObject    handle to chkBVP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkBVP2
set(handles.chkBVP1,'Value',false);
set(handles.chkECG,'Value',false);
drawnow


function editAddPeak_Callback(hObject, eventdata, handles)
% hObject    handle to editAddPeak (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editAddPeak as text
%        str2double(get(hObject,'String')) returns contents of editAddPeak as a double


% --- Executes during object creation, after setting all properties.
function editAddPeak_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editAddPeak (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbCheckPeak.
function pbCheckPeak_Callback(hObject, eventdata, handles)
% hObject    handle to pbCheckPeak (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% display checkPeak

global chkPeakFrame
global StartDisplayFrame



windowSize =str2double(get(handles.editDWindowSize,'String'));
addPeakAt = str2double(get(handles.editAddPeak,'String'));
%% convert decimal to whole number
set(handles.editAddPeak,'String',num2str(ceil(str2num( get(handles.editAddPeak,'String')))));
if addPeakAt <StartDisplayFrame |addPeakAt > windowSize+StartDisplayFrame
    chkPeakFrame=false;
    set(handles.pbAddPeak,'enable','off');
else
    chkPeakFrame=true;
    set(handles.pbAddPeak,'enable','on');
end



% if chkPeakFrame <StartDisplayFrame | chkPeakFrame > windowSize+StartDisplayFrame
%     chkPeakFrame = windowSize/2+StartDisplayFrame
% else
%     chkPeakFrame = str2double(get(handles.editAddPeak,'String'));
% end
displaySignal(handles)

%% moving the slide based on the input value
%chkPeakFrame = str2double(get(handles.editAddPeak,'String'));
% set(handles.sliderPeak,'SliderStep',[1/windowSize,0.1]);
% slideValue = (chkPeakFrame - StartDisplayFrame+1)/windowSize
% set(handles.sliderPeak,'Value',slideValue)

%slideValue=chkPeakFrame-StartDisplayFrame;
%set(handles.sliderPeak,'Value',slideValue)



%windowSize = str2double(get(handles.editDWindowSize,'String'));
%set(handles.sliderPeak,'SliderStep',[1/windowSize,0.1]);
%slideValue = get(handles.sliderPeak,'Value')*windowSize;
%slideValue=slideValue+StartDisplayFrame;
%set(handles.editAddPeak,'String',num2str(slideValue));











% --- Executes on button press in pbAddPeak.
function pbAddPeak_Callback(hObject, eventdata, handles)
% hObject    handle to pbAddPeak (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global StartDisplayFrame
addPeakAt = ceil(str2num(get(handles.editAddPeak,'String')));
windowSize =str2num(get(handles.editDWindowSize,'String'));
if ~(addPeakAt <StartDisplayFrame |addPeakAt > windowSize+StartDisplayFrame)
    if (get(handles.chkECG,'Value'))
        addNewECGPeak(handles)
        set(handles.pbAddPeak,'enable','off')
    elseif get(handles.chkBVP1,'Value')
        addNewBVPPeak(handles)
        set(handles.pbAddPeak,'enable','off')
    elseif get(handles.chkBVP2,'Value')
        addNewBVPPeak2(handles);
        set(handles.pbAddPeak,'enable','off')
    end
    displaySignal(handles)
end


% --- Executes on button press in pbAddPeakBack.
function pbAddPeakBack_Callback(hObject, eventdata, handles)
% hObject    handle to pbAddPeakBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%global chkPeakFrame
global StartDisplayFrame
global chkPeakFrame
stepSize = ceil(str2num(get(handles.editAddSteps,'String')));

addPeakAt = ceil(str2num(get(handles.editAddPeak,'String')));
addPeakAt = max(addPeakAt-stepSize,StartDisplayFrame);
set(handles.editAddPeak,'String', num2str(addPeakAt));

% %% move slide
% set(handles.sliderPeak,'SliderStep',[1/windowSize,0.1]);
% slideValue = (chkPeakFrame - StartDisplayFrame+1)/windowSize
% set(handles.sliderPeak,'Value',slideValue)
chkPeakFrame = true
drawnow
displaySignal(handles)



% --- Executes on button press in pbAddPeakForward.
function pbAddPeakForward_Callback(hObject, eventdata, handles)
% hObject    handle to pbAddPeakForward (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



global StartDisplayFrame
global chkPeakFrame
windowSize = str2double(get(handles.editDWindowSize,'String'));
addPeakAt = ceil(str2num(get(handles.editAddPeak,'String')));
%%should not exeed size of display Window.
stepSize = ceil(str2num(get(handles.editAddSteps,'String')));
addPeakAt = min(addPeakAt+stepSize,windowSize+StartDisplayFrame);

%move the slide
% set(handles.sliderPeak,'SliderStep',[1/windowSize,0.1]);
% slideValue = (chkPeakFrame - StartDisplayFrame+1)/windowSize
% set(handles.sliderPeak,'Value',slideValue)
%% move the dot line
set(handles.editAddPeak,'String', num2str(addPeakAt));
chkPeakFrame = true;
displaySignal(handles)
drawnow


function  addNewECGPeak(handles)

global ecgDataProcessed

global ecgLocQwave
global ecgLocRwave
global ecgLocSwave
global ecgLocTwave

%%global DeletedSignals


rWave =  ceil(str2num( get(handles.editAddPeak,'String')));

%% check if rWave is in delete range or not...
%% new to work further in this condition as DeletedSignals datastructure is changed..
% if isempty(DeletedSignals)
%     chkIfDel=[];
% else
%     chkIfDel = find(DeletedSignals(:,2)>=rWave & DeletedSignals(:,3) <=rWave)
% end


ecgRSDist  =str2num(get(handles.ecgRSDist  ,'String'));
ecgQRDist   =str2num(get(handles.ecgQRDist ,'String'));
ecgSTDistInPer =  str2num(get(handles.ecgSTDist  ,'String'));



addingIndex=find(ecgLocRwave(:,1)<rWave);

if isempty(addingIndex)
    limitQ = 0;
    limitS =  ecgLocQwave(1);
    limitT = limitS;
    addingIndex = -1;
else
    addingIndex=addingIndex(end)
    %% if adding location is at after last item
    if  size(addingIndex,1)== size(ecgLocRwave,1)
        limitQ = ecgLocTwave(end,1);
        limitS = size(ecgDataProcessed,1);
        limitT = limitS;
    else
        posR = addingIndex(end,1);

        limitQ = ecgLocTwave(posR,1);
        try
            limitS = ecgLocQwave(posR+1,1);
        catch
            limitS=size(ecgDataProcessed,1);
        end
        limitT = limitS;
    end
end

currLocation = [];

try
    diff = ecgLocQwave(addingIndex+1,1)-ecgLocSwave(addingIndex,1);

catch
    diff = ecgLocQwave(1,1);
end
ecgSTDist = ceil((diff * ecgSTDistInPer)/100)
dist = ecgRSDist;

while (isempty(currLocation))
    data = -[ecgDataProcessed(rWave:rWave+dist,1)];
    ecgSPeakHeight = mean(data);
    [~,currLocation] =findpeaks(data,'MinPeakHeight',ecgSPeakHeight,'MinPeakDistance',size(data,1)/2);
    dist=dist+10;
    if dist >  limitQ
        currLocation = dist-10;
        break;
    end
end

sWave =rWave+ currLocation(1)


currLocation=[];
tmpDist = ecgQRDist;
%% if peak is not found increase the range of peak
while isempty(currLocation)
    data = [ecgDataProcessed(rWave-tmpDist:rWave,1)];
    tmpDist = tmpDist+10;
    [~,currLocation] =findpeaks(-data,'MinPeakHeight',mean(-data),'MinPeakDistance',size(data,1)/2);
    if tmpDist < limitQ
        currLocation = tmpDist-10 ;
        break;
    end
end

% [~,currLocation] = findpeaks(data,'MinPeakDistance',size(data,1)/2+10);
qWave =rWave-currLocation(end);


currLocation=[];
tmpDist = ecgSTDist;


while isempty(currLocation)
    try
        data = [ecgDataProcessed(sWave:sWave+tmpDist,1)];
    catch
        data =[ecgDataProcessed(sWave:end,1)];
    end
    tmpDist = tmpDist+10;
    if get(handles.chkInverseT,'Value')==1
        [~,currLocation] =findpeaks(-data,'MinPeakHeight',mean(-data),'MinPeakDistance',size(data,1)/2);
    else
        [~,currLocation] =findpeaks(-data,'MinPeakHeight',mean(-data),'MinPeakDistance',size(data,1)/2);
    end
    if tmpDist >  limitT
        currLocation = tmpDist-10;
        break;
    end
end

tWave=sWave+currLocation(1);






%%case of first peak
if isempty(addingIndex)

    ecgLocQwave=[qWave;ecgLocQwave];
    ecgLocRwave=[rWave;ecgLocRwave];
    ecgLocSwave=[sWave;ecgLocSwave];
    ecgLocTwave=[tWave;ecgLocTwave];

else
    if size(addingIndex,1)>1
        addingIndex = addingIndex(end,1);
    end

    %%adding at end
    if addingIndex > size(ecgLocRwave,1)
        ecgLocQwave=[ecgLocQwave;qWave];
        ecgLocRwave=[ecgLocRwave;rWave];
        ecgLocSwave=[ecgLocSwave;sWave];
        ecgLocTwave=[ecgLocTwave;tWave];
    else  %% adding in middle
        ecgLocQwave=[ecgLocQwave(1:addingIndex,:);qWave;ecgLocQwave(addingIndex+1:end,:)];
        ecgLocRwave=[ecgLocRwave(1:addingIndex,:);rWave;ecgLocRwave(addingIndex+1:end,:)];
        ecgLocSwave=[ecgLocSwave(1:addingIndex,:);sWave;ecgLocSwave(addingIndex+1:end,:)];
        ecgLocTwave=[ecgLocTwave(1:addingIndex,:);tWave;ecgLocTwave(addingIndex+1:end,:)];
    end

end



function  addNewBVPPeak(handles)

global bvpDataProcessed

global bvpLocQwave
global bvpLocRwave
global bvpLocSwave



rWave = ceil(str2num( get(handles.editAddPeak,'String')));

bvpRSDist  =str2num(get(handles.editBVPRSDist  ,'String'));
bvpQRDist   =str2num(get(handles.editBVPQRDist ,'String'));


addingIndex=find(bvpLocRwave(:,1)<rWave);
if isempty(addingIndex)
    limitQ = 0;
    limitS =  bvpLocQwave(1);
    
else
    %% if there are more than one index take the last one
    if size(addingIndex,1)>1
        addingIndex = addingIndex(end,1);
    end
    
    %% if adding location is at after last item
    if  size(addingIndex,1)== size(bvpLocRwave,1)
        limitQ = bvpLocSwave(end,1);
        limitS = size(bvpDataProcessed,1);
        
    else
        posR = addingIndex(end,1);
        
        limitQ = bvpLocSwave(posR,1);
        limitS = bvpLocQwave(posR+1,1);
        
    end
end

currLocation = [];
dist = bvpRSDist;
while (isempty(currLocation))
    data = -[bvpDataProcessed(rWave:rWave+dist,1)];
    bvpSPeakHeight = mean(data);
    [~,currLocation] = findpeaks(data,'MinPeakHeight',bvpSPeakHeight,'MinPeakDistance',size(data,1)/2);
    dist=dist+10;
    if dist >  limitQ
        currLocation = dist-10;
        break;
    end
end

sWave =rWave+ currLocation(1)


currLocation=[];
tmpDist = bvpQRDist;
%% if peak is not found increase the range of peak
while isempty(currLocation)
    
    data = [bvpDataProcessed(rWave-tmpDist:rWave,1)];
    tmpDist = tmpDist+10;
    [~,currLocation] = findpeaks(-data,'MinPeakHeight',0,'MinPeakDistance',size(data,1)/2);
    if tmpDist < limitQ
        currLocation = tmpDist-10 ;
        break;
    end
end

% [~,currLocation] = findpeaks(data,'MinPeakDistance',size(data,1)/2+10);
qWave =rWave-currLocation(1);


%%case of first peak
if isempty(addingIndex)
    bvpLocQwave=[qWave;bvpLocQwave];
    bvpLocRwave=[rWave;bvpLocRwave];
    bvpLocSwave=[sWave;bvpLocSwave];
    
else
    if size(addingIndex,1)>1
        addingIndex = addingIndex(end,1);
    end
    
    %%adding at end
    if addingIndex > size(bvpLocRwave,1)
        bvpLocQwave=[bvpLocQwave;qWave];
        bvpLocRwave=[bvpLocRwave;rWave];
        bvpLocSwave=[bvpLocSwave;sWave];
    else %% adding in middle..
        bvpLocQwave=[bvpLocQwave(1:addingIndex,:);qWave;bvpLocQwave(addingIndex+1:end,:)];
        bvpLocRwave=[bvpLocRwave(1:addingIndex,:);rWave;bvpLocRwave(addingIndex+1:end,:)];
        bvpLocSwave=[bvpLocSwave(1:addingIndex,:);sWave;bvpLocSwave(addingIndex+1:end,:)];
        
    end
    
end

%chkAddPeak = false;



function  addNewBVPPeak2(handles)

global bvpDataProcessed2

global bvpLocQwave2
global bvpLocRwave2
global bvpLocSwave2

% global chkAddPeak
global StartDisplayFrame

rWave = ceil(str2num( get(handles.editAddPeak,'String')));

bvpRSDist  =str2num(get(handles.editBVP1RSDist  ,'String'));
bvpQRDist   =str2num(get(handles.editBVP1QRDist ,'String'));


addingIndex=find(bvpLocRwave2(:,1)<rWave);
if isempty(addingIndex)
    limitQ = 0;
    limitS =  bvpLocQwave2(1);
    
else
    %% if there are more than one index take the last one
    if size(addingIndex,1)>1
        addingIndex = addingIndex(end,1);
    end
    %% if adding location after last item
    if  size(addingIndex,1)== size(bvpLocRwave2,1)
        limitQ = bvpLocSwave2(end,1);
        %% size of data
        limitS = size(bvpDataProcessed2,1);
        
    else  %% between
        posR = addingIndex(end,1);
        
        limitQ = bvpLocSwave2(posR,1);
        
        limitS = bvpLocQwave2(posR+1,1);
        
        
    end
end

currLocation = [];
dist = bvpRSDist;
while (isempty(currLocation))
    data = -[bvpDataProcessed2(rWave:rWave+dist,1)];
    bvpSPeakHeight = mean(data);
    [~,currLocation] = findpeaks(data,'MinPeakHeight',bvpSPeakHeight,'MinPeakDistance',size(data,1)/2);
    dist=dist+10;
    if dist >  limitQ
        currLocation = dist-10;
        break;
    end
end

sWave =rWave+ currLocation(1)


currLocation=[];
tmpDist = bvpQRDist;
%% if peak is not found increase the range of peak
while isempty(currLocation)
    data = [bvpDataProcessed2(rWave-tmpDist:rWave,1)];
    tmpDist = tmpDist+10;
    [~,currLocation] = findpeaks(-data,'MinPeakHeight',0,'MinPeakDistance',size(data,1)/2);
    if tmpDist < limitQ
        currLocation = tmpDist-10 ;
        break;
    end
end

% [~,currLocation] = findpeaks(data,'MinPeakDistance',size(data,1)/2+10);
qWave =rWave-currLocation(1);


%%case of first peak
if isempty(addingIndex)
    bvpLocQwave2=[qWave;bvpLocQwave2];
    bvpLocRwave2=[rWave;bvpLocRwave2];
    bvpLocSwave2=[sWave;bvpLocSwave2];
    
else
    if size(addingIndex,1)>1
        addingIndex = addingIndex(end,1);
    end
    
    %%adding at end
    if addingIndex > size(bvpLocRwave2,1)
        bvpLocQwave2=[bvpLocQwave2;qWave];
        bvpLocRwave2=[bvpLocRwave2;rWave];
        bvpLocSwave2=[bvpLocSwave2;sWave];
    else %% adding in middle..
        bvpLocQwave2=[bvpLocQwave2(1:addingIndex,:);qWave;bvpLocQwave2(addingIndex+1:end,:)];
        bvpLocRwave2=[bvpLocRwave2(1:addingIndex,:);rWave ;bvpLocRwave2(addingIndex+1:end,:)];
        bvpLocSwave2=[bvpLocSwave2(1:addingIndex,:);sWave;bvpLocSwave2(addingIndex+1:end,:)];
        
    end
    
end

%chkAddPeak = false;









% --- Executes on key press with focus on chkECG and none of its controls.
function chkECG_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to chkECG (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on figure1 or any of its controls.
function figure1_WindowKeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
% switch eventdata.Key
%     case 'rightarrow'
%         pbAddPeakForward_Callback(hObject, eventdata, handles)
%
%     case 'leftarrow'
%         pbAddPeakBack_Callback(hObject, eventdata, handles)
% end



function editVersion_Callback(hObject, eventdata, handles)
% hObject    handle to editVersion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editVersion as text
%        str2double(get(hObject,'String')) returns contents of editVersion as a double


% --- Executes during object creation, after setting all properties.
function editVersion_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editVersion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chkLowPassIIRFilter.
function chkLowPassIIRFilter_Callback(hObject, eventdata, handles)
% hObject    handle to chkLowPassIIRFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkLowPassIIRFilter


% --- Executes on button press in chkBVP2F.
function chkBVP2F_Callback(hObject, eventdata, handles)
% hObject    handle to chkBVP2F (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkBVP2F


% --- Executes on button press in chkBVP2P.
function chkBVP2P_Callback(hObject, eventdata, handles)
% hObject    handle to chkBVP2P (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkBVP2P


% --- Executes on button press in chkBVP2E.
function chkBVP2E_Callback(hObject, eventdata, handles)
% hObject    handle to chkBVP2E (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkBVP2E


% --- Executes on button press in chkSaveBVP1_FH.
function chkSaveBVP1_FH_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP1_FH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP1_FH


% --- Executes on button press in chkSaveBVP1_PH.
function chkSaveBVP1_PH_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP1_PH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP1_PH


% --- Executes on button press in chkSaveBVP1_EH.
function chkSaveBVP1_EH_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP1_EH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP1_EH


% --- Executes on button press in chkSaveECG_QH.
function chkSaveECG_QH_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveECG_QH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveECG_QH


% --- Executes on button press in chkSaveECG_RH.
function chkSaveECG_RH_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveECG_RH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveECG_RH


% --- Executes on button press in chkSaveECG_SH.
function chkSaveECG_SH_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveECG_SH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveECG_SH


% --- Executes on button press in chkSaveECG_TH.
function chkSaveECG_TH_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveECG_TH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveECG_TH


% --- Executes on key press with focus on figure1 and none of its controls.
function figure1_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on mouse press over figure background.
function figure1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key release with focus on figure1 and none of its controls.
function figure1_KeyReleaseFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.FIGURE)
%	Key: name of the key that was released, in lower case
%	Character: character interpretation of the key(s) that was released
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) released
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in chkHighPassIIRFilter.
function chkHighPassIIRFilter_Callback(hObject, eventdata, handles)
% hObject    handle to chkHighPassIIRFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkHighPassIIRFilter


% --- Executes on button press in chkPlotPeak.
function chkPlotPeak_Callback(hObject, eventdata, handles)
% hObject    handle to chkPlotPeak (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkPlotPeak
displaySignal(handles)


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% % --- Executes during object creation, after setting all properties.
% function slider1_CreateFcn(hObject, eventdata, handles)
% % hObject    handle to slider1 (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    empty - handles not created until after all CreateFcns called
% 
% % Hint: slider controls usually have a light gray background.
% if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
%     set(hObject,'BackgroundColor',[.9 .9 .9]);
% end
% 
% 
% % % --- Executes on slider movement.
% % function sliderPeak_Callback(hObject, eventdata, handles)
% % % hObject    handle to sliderPeak (see GCBO)
% % % eventdata  reserved - to be defined in a future version of MATLAB
% % % handles    structure with handles and user data (see GUIDATA)
% % 
% % % Hints: get(hObject,'Value') returns position of slider
% % %        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
% % global StartDisplayFrame
% % % global chkAddPeak
% % global chkPeakFrame
% % 
% % windowSize = str2double(get(handles.editDWindowSize,'String'));
% % set(handles.sliderPeak,'SliderStep',[1/windowSize,0.1]);
% % slideValue = get(handles.sliderPeak,'Value')*windowSize;
% % slideValue=slideValue+StartDisplayFrame;
% % set(handles.editAddPeak,'String',num2str(slideValue));
% 
% % pos = ax1.Position;
% % pos(1)=20;
% % set(handles.sliderPeak,'Position',pos)%   ax1.Position = pos;
% %
% 
% 
% 
% 
% %
% % global StartDisplayFrame
% % %windowSize =str2num(get(handles.editDWindowSize,'String'));
% chkPeakFrame = slideValue%max(chkPeakFrame-10,StartDisplayFrame);
% %chkAddPeak = true;
% %set(handles.editAddPeak,'String', num2str(chkPeakFrame));
% drawnow
% displaySignal(handles)











% % --- Executes during object creation, after setting all properties.
% function sliderPeak_CreateFcn(hObject, eventdata, handles)
% % hObject    handle to sliderPeak (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    empty - handles not created until after all CreateFcns called
% 
% % Hint: slider controls usually have a light gray background.
% if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
%     set(hObject,'BackgroundColor',[.9 .9 .9]);
% end



function ecgLowPassBandF_Callback(hObject, eventdata, handles)
% hObject    handle to ecgLowPassBandF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgLowPassBandF as text
%        str2double(get(hObject,'String')) returns contents of ecgLowPassBandF as a double


% --- Executes during object creation, after setting all properties.
function ecgLowPassBandF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecgLowPassBandF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecgHighPassBandF_Callback(hObject, eventdata, handles)
% hObject    handle to ecgHighPassBandF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgHighPassBandF as text
%        str2double(get(hObject,'String')) returns contents of ecgHighPassBandF as a double


% --- Executes during object creation, after setting all properties.
function ecgHighPassBandF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecgHighPassBandF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbECGUndoDelete.
function pbECGUndoDelete_Callback(hObject, eventdata, handles)
% hObject    handle to pbECGUndoDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global ecgLocQwave
global ecgLocRwave
global ecgLocSwave
global ecgLocTwave
global deletedECGPeak


if ~ isempty(deletedECGPeak)
    % rowID = deletedECGPeak(1,1);
    rowID = find(ecgLocRwave(:,1)<deletedECGPeak(1,2));
    %% if deleted item is first item than rowID will be null..
    if isempty(rowID)
        rowID=0;
    end
    
    %% get the last one
    rowID = rowID(end,1);
    ecgLocQwave=[ecgLocQwave(1:rowID,:);deletedECGPeak(1,1);ecgLocQwave(rowID+1:end,:)];
    ecgLocRwave=[ecgLocRwave(1:rowID,:);deletedECGPeak(1,2);ecgLocRwave(rowID+1:end,:)];
    ecgLocSwave=[ecgLocSwave(1:rowID,:);deletedECGPeak(1,3);ecgLocSwave(rowID+1:end,:)];
    ecgLocTwave=[ecgLocTwave(1:rowID,:);deletedECGPeak(1,4);ecgLocTwave(rowID+1:end,:)];
    deletedECGPeak(1,:)=[];
    displaySignal(handles)
end


% --- Executes on button press in pbECG1UndoDelete.
function pbECG1UndoDelete_Callback(hObject, eventdata, handles)
% hObject    handle to pbECG1UndoDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global bvpLocQwave
global bvpLocRwave
global bvpLocSwave

global deletedBVP1Peak


if ~ isempty(deletedBVP1Peak)
    rowID = find(bvpLocRwave(:,1)<deletedBVP1Peak(1,2));
    %% if deleted item is first item than rowID will be null..
    if isempty(rowID)
        rowID=0;
    end
    
    bvpLocQwave=[bvpLocQwave(1:rowID,:);deletedBVP1Peak(1,1);bvpLocQwave(rowID+1:end,:)];
    bvpLocRwave=[bvpLocRwave(1:rowID,:);deletedBVP1Peak(1,2);bvpLocRwave(rowID+1:end,:)];
    bvpLocSwave=[bvpLocSwave(1:rowID,:);deletedBVP1Peak(1,3);bvpLocSwave(rowID+1:end,:)];
    
    deletedBVP1Peak(1,:)=[];
    displaySignal(handles)
end












% --- Executes on button press in pbBVP2UndoDelete.
function pbBVP2UndoDelete_Callback(hObject, eventdata, handles)
% hObject    handle to pbBVP2UndoDelete (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)




% if ~ isempty(deletedBVP2Peak)
%     rowID = find(bvpLocRwave2(:,1)<deletedBVP2Peak(1,2));
%     %% if deleted item is first item than rowID will be null..
%     if isempty(rowID)
%         rowID=0;
%     end
%
%     bvpLocQwave2=[bvpLocQwave2(1:rowID,:);deletedBVP2Peak(1,2);bvpLocQwave2(rowID+1:end,:)];
%     bvpLocRwave2=[bvpLocRwave2(1:rowID,:);deletedBVP2Peak(1,3:4);bvpLocRwave2(rowID+1:end,:)];
%     bvpLocSwave2=[bvpLocSwave2(1:rowID,:);deletedBVP2Peak(1,5);bvpLocSwave2(rowID+1:end,:)];
%
%     deletedBVP2Peak(1,:)=[];
%     displaySignal(handles)
% end


global bvpLocQwave2
global bvpLocRwave2
global bvpLocSwave2

global deletedBVP2Peak


if ~ isempty(deletedBVP2Peak)
    rowID = find(bvpLocRwave2(:,1)<deletedBVP2Peak(1,2));
    %% if deleted item is first item than rowID will be null..
    if isempty(rowID)
        rowID=0;
    end
    
    bvpLocQwave2=[bvpLocQwave2(1:rowID,:);deletedBVP2Peak(1,1);bvpLocQwave2(rowID+1:end,:)];
    bvpLocRwave2=[bvpLocRwave2(1:rowID,:);deletedBVP2Peak(1,2);bvpLocRwave2(rowID+1:end,:)];
    bvpLocSwave2=[bvpLocSwave2(1:rowID,:);deletedBVP2Peak(1,3);bvpLocSwave2(rowID+1:end,:)];
    
    deletedBVP2Peak(1,:)=[];
    displaySignal(handles)
end


% --- Executes on selection change in pMenuDeletedRange.
function pMenuDeletedRange_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuDeletedRange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuDeletedRange contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuDeletedRange
global StartDisplayFrame
selIndex = get(handles.pMenuDeletedRange,'Value');
if selIndex>1
    deletedRangeInfo = get(handles.pMenuDeletedRange,'String');
    info=strsplit(deletedRangeInfo{selIndex,1},'___');
    fromFrame = str2num(info{1});
    toFrame = str2num(info{2});
    midFrame = (fromFrame+toFrame)/2;
    
    lowRange = midFrame - 0.5*str2double(get(handles.editDWindowSize,'String'));
    
    StartDisplayFrame =  max(0,lowRange);
    displaySignal(handles)
    
end




% --- Executes during object creation, after setting all properties.
function pMenuDeletedRange_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuDeletedRange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbRecoverRange.
function pbRecoverRange_Callback(hObject, eventdata, handles)
% hObject    handle to pbRecoverRange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%global DeletedSignals
global ecgLocQwave
global ecgLocRwave
global ecgLocSwave
global ecgLocTwave
global bvpLocQwave
global bvpLocRwave
global bvpLocSwave
global bvpLocQwave2
global bvpLocRwave2
global bvpLocSwave2

global DeletedPeaksCol

valIndex = get(handles.pMenuDeletedRange,'Value');
if valIndex > 1
    delRangeInfo = get(handles.pMenuDeletedRange,'String');
    
    info=strsplit(delRangeInfo{valIndex,1},'___')
    fromFrame = str2num(info{1});
    toFrame = str2num(info{2});
    
    tmpToFrm = [cell2mat(DeletedPeaksCol(:,1)) cell2mat(DeletedPeaksCol(:,2)) ];
    
    recoverInd = find(tmpToFrm(:,1)>=fromFrame & tmpToFrm(:,2)<=toFrame);
    ecgPeaks = DeletedPeaksCol{recoverInd,3};
    bvp1Peaks = DeletedPeaksCol{recoverInd,4};
    bvp2Peaks = DeletedPeaksCol{recoverInd,4};
    DeletedPeaksCol(recoverInd,:)=[];
    
    ecgLocQwave=[ecgLocQwave(ecgLocQwave(:,1)<ecgPeaks(1,1),:);ecgPeaks(:,1);ecgLocQwave(ecgLocQwave(:,1)>ecgPeaks(end,1),:)];
    %ecgLocRwave=[ecgLocRwave(ecgLocRwave(:,1)<ecgPeaks(1,2),:);[ecgPeaks(:,2) ones(size(ecgPeaks,1))];ecgLocRwave(ecgLocRwave(:,1)>ecgPeaks(end,2),:)]
    ecgLocRwave=[ecgLocRwave(ecgLocRwave(:,1)<ecgPeaks(1,2),:);ecgPeaks(:,2);ecgLocRwave(ecgLocRwave(:,1)>ecgPeaks(end,2),:)]
    ecgLocSwave=[ecgLocSwave(ecgLocSwave(:,1)<ecgPeaks(1,3),:);ecgPeaks(:,3);ecgLocSwave(ecgLocSwave(:,1)>ecgPeaks(end,3),:)];
    ecgLocTwave=[ecgLocTwave(ecgLocTwave(:,1)<ecgPeaks(1,4),:);ecgPeaks(:,4);ecgLocTwave(ecgLocTwave(:,1)>ecgPeaks(end,4),:)];
    
    
    bvpLocQwave=[bvpLocQwave(bvpLocQwave(:,1)<bvp1Peaks(1,1),:);bvp1Peaks(:,1);bvpLocQwave(bvpLocQwave(:,1)>bvp1Peaks(end,1),:)];
    %bvpLocRwave=[bvpLocRwave(bvpLocRwave(:,1)<bvp1Peaks(1,2),:);[bvp1Peaks(:,2) ones(size(bvp1Peaks,1))];bvpLocRwave(bvpLocRwave(:,1)>bvp1Peaks(end,2),:)]
    bvpLocRwave=[bvpLocRwave(bvpLocRwave(:,1)<bvp1Peaks(1,2),:);bvp1Peaks(:,2) ;bvpLocRwave(bvpLocRwave(:,1)>bvp1Peaks(end,2),:)]
    bvpLocSwave=[bvpLocSwave(bvpLocSwave(:,1)<bvp1Peaks(1,3),:);bvp1Peaks(:,3);bvpLocSwave(bvpLocSwave(:,1)>bvp1Peaks(end,3),:)];
    
    
    bvpLocQwave2=[bvpLocQwave2(bvpLocQwave2(:,1)<bvp2Peaks(1,1),:);bvp2Peaks(:,1);bvpLocQwave2(bvpLocQwave2(:,1)>bvp2Peaks(end,1),:)];
    %bvpLocRwave2=[bvpLocRwave2(bvpLocRwave2(:,1)<bvp2Peaks(1,2),:);[bvp2Peaks(:,2) ones(size(bvp2Peaks,1))];bvpLocRwave2(bvpLocRwave2(:,1)>bvp2Peaks(end,2),:)]
    bvpLocRwave2=[bvpLocRwave2(bvpLocRwave2(:,1)<bvp2Peaks(1,2),:);bvp2Peaks(:,2);bvpLocRwave2(bvpLocRwave2(:,1)>bvp2Peaks(end,2),:)]
    bvpLocSwave2=[bvpLocSwave2(bvpLocSwave2(:,1)<bvp2Peaks(1,3),:);bvp2Peaks(:,3);bvpLocSwave2(bvpLocSwave2(:,1)>bvp2Peaks(end,3),:)];
    
    
    %DeletedSignals(DeletedSignals(:,2)>=fromFrame & DeletedSignals(:,3)<=toFrame,:)=[];
    
    
    
    % ecgLocRwave(ecgLocRwave(:,1)>=fromFrame & ecgLocRwave(:,1)<=toFrame ,:)=[];
    %ecgLocRwave(ecgLocRwave(:,1)>=fromFrame & ecgLocRwave(:,1)<=toFrame ,2) = 1;
    % ecgLocQwave(ecgLocQwave(:,1)>=fromFrame & ecgLocQwave(:,1)<=toFrame,:)=[];
    % ecgLocSwave(ecgLocSwave(:,1)>=fromFrame & ecgLocSwave(:,1)<=toFrame,:)=[];
    % ecgLocTwave(ecgLocTwave(:,1)>=fromFrame & ecgLocTwave(:,1)<=toFrame ,:)=[];
    %
    % bvpLocRwave(bvpLocRwave(:,1)>=fromFrame & bvpLocRwave(:,1)<=toFrame ,:)=[];
    %bvpLocRwave(bvpLocRwave(:,1)>=fromFrame & bvpLocRwave(:,1)<=toFrame ,2)  = 1;
    % bvpLocSwave(bvpLocSwave(:,1)>=fromFrame & bvpLocSwave(:,1)<=toFrame,:)=[];
    % bvpLocQwave(bvpLocQwave(:,1)>=fromFrame & bvpLocQwave(:,1)<=toFrame,:)=[];
    %
    % bvpLocQwave2(bvpLocQwave2(:,1)>=fromFrame & bvpLocQwave2(:,1)<=toFrame,:)=[];
    %bvpLocRwave2(bvpLocRwave2(:,1)>=fromFrame & bvpLocRwave2(:,1)<=toFrame,2) = 1;
    % bvpLocRwave2(bvpLocRwave2(:,1)>=fromFrame & bvpLocRwave2(:,1)<=toFrame ,:)=[];
    % bvpLocSwave2(bvpLocSwave2(:,1)>=fromFrame & bvpLocSwave2(:,1)<=toFrame,:)=[];
    %delRangeInfo(valIndex)=[]
    displaySignal(handles)
    addRangeInMenu(handles)
end



% --- Executes during object creation, after setting all properties.
function pbRecoverRange_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pbRecoverRange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function ecgLowPassBandR_Callback(hObject, eventdata, handles)
% hObject    handle to ecgLowPassBandR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgLowPassBandR as text
%        str2double(get(hObject,'String')) returns contents of ecgLowPassBandR as a double


% --- Executes during object creation, after setting all properties.
function ecgLowPassBandR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecgLowPassBandR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ecgHighPassBandR_Callback(hObject, eventdata, handles)
% hObject    handle to ecgHighPassBandR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgHighPassBandR as text
%        str2double(get(hObject,'String')) returns contents of ecgHighPassBandR as a double


% --- Executes during object creation, after setting all properties.
function ecgHighPassBandR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecgHighPassBandR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chkSaveBVP2_FH.
function chkSaveBVP2_FH_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP2_FH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP2_FH


% --- Executes on button press in chkSaveBVP2_PH.
function chkSaveBVP2_PH_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP2_PH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP2_PH


% --- Executes on button press in chkSaveBVP2_EH.
function chkSaveBVP2_EH_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP2_EH (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP2_EH


% --- Executes on button press in chkSaveBVP2_FL.
function chkSaveBVP2_FL_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP2_FL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP2_FL


% --- Executes on button press in chkSaveBVP2_PL.
function chkSaveBVP2_PL_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP2_PL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP2_PL


% --- Executes on button press in chkSaveBVP2_EL.
function chkSaveBVP2_EL_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP2_EL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP2_EL


% --- Executes on button press in chkSaveBVP1_FL.
function chkSaveBVP1_FL_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP1_FL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP1_FL


% --- Executes on button press in chkSaveBVP1_PL.
function chkSaveBVP1_PL_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP1_PL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP1_PL


% --- Executes on button press in chkSaveBVP1_EL.
function chkSaveBVP1_EL_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveBVP1_EL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveBVP1_EL


% --- Executes on button press in chkSaveECG_QL.
function chkSaveECG_QL_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveECG_QL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveECG_QL


% --- Executes on button press in chkSaveECG_RL.
function chkSaveECG_RL_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveECG_RL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveECG_RL


% --- Executes on button press in chkSaveECG_SL.
function chkSaveECG_SL_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveECG_SL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveECG_SL


% --- Executes on button press in chkSaveECG_TL.
function chkSaveECG_TL_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveECG_TL (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveECG_TL


% --- Executes on button press in chkSaveDelayECG_BVP1.
function chkSaveDelayECG_BVP1_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveDelayECG_BVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveDelayECG_BVP1


% --- Executes on button press in chkSaveDelayECG_BVP2.
function chkSaveDelayECG_BVP2_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveDelayECG_BVP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveDelayECG_BVP2


% --- Executes on button press in chkSaveProcessedECG.
function chkSaveProcessedECG_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveProcessedECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveProcessedECG


% --- Executes on button press in chkSaveProcessedBVP1.
function chkSaveProcessedBVP1_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveProcessedBVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveProcessedBVP1


% --- Executes on button press in chkSaveProcessedBVP2.
function chkSaveProcessedBVP2_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveProcessedBVP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveProcessedBVP2


% --- Executes on button press in chkSaveECG_RSHeight.
function chkSaveECG_RSHeight_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveECG_RSHeight (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveECG_RSHeight



function editExcelFileName_Callback(hObject, eventdata, handles)
% hObject    handle to editExcelFileName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editExcelFileName as text
%        str2double(get(hObject,'String')) returns contents of editExcelFileName as a double


% --- Executes during object creation, after setting all properties.
function editExcelFileName_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editExcelFileName (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chkSaveInExcel.
function chkSaveInExcel_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveInExcel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveInExcel


% --- Executes on button press in chkSaveInMat.
function chkSaveInMat_Callback(hObject, eventdata, handles)
% hObject    handle to chkSaveInMat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkSaveInMat



function editStartFrame_Callback(hObject, eventdata, handles)
% hObject    handle to editStartFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editStartFrame as text
%        str2double(get(hObject,'String')) returns contents of editStartFrame as a double
global StartDisplayFrame
startFrame = str2num(get(hObject,'String'))
if startFrame <=0
    startFrame = 1;
    set(hObject,'String','1')
    
end
StartDisplayFrame = max(startFrame,StartDisplayFrame);


% --- Executes during object creation, after setting all properties.
function editStartFrame_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editStartFrame (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbProcessECG.
function pbProcessECG_Callback(hObject, eventdata, handles)
% hObject    handle to pbProcessECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global displayMatrix
loadDataForProcessing(handles);
filterECGData(handles);
displayMatrix(1,1)=0;
displayMatrix(1,2)=1;
displaySignal(handles);
enabledDataDisplay(handles);




% --- Executes on button press in pbProcessBVP2.
function pbProcessBVP1_Callback(hObject, eventdata, handles)
% hObject    handle to pbProcessBVP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% hObject    handle to pbProcessECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global displayMatrix
loadDataForProcessing(handles);
filterBVPData(handles);
displayMatrix(1,3)=0;
displayMatrix(1,4)=1;
displaySignal(handles);
enabledDataDisplay(handles);






% --- Executes on button press in pbProcessBVP2.
function pbProcessBVP2_Callback(hObject, eventdata, handles)
% hObject    handle to pbProcessBVP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global displayMatrix
loadDataForProcessing(handles);
filterBVPData1(handles);
displayMatrix(1,5)=0;
displayMatrix(1,6)=1;
displaySignal(handles);
enabledDataDisplay(handles);





% --- Executes on button press in rbRawData.
function rbRawData_Callback(hObject, eventdata, handles)
% hObject    handle to rbRawData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbRawData

clearAllData(handles);
clearPlots();
clearDeletedPeak();

if(get(handles.rbRawData,'Value')==0)
    set(handles.rbProcessedData,'Value',1);
    set(handles.rbRawData,'Value',0);
    
    
    set(handles.chkECG,'Enable','on');
    set(handles.chkBVP1,'Enable','on');
    set(handles.chkBVP2,'Enable','on');
    
    
else
    set(handles.rbProcessedData,'Value',0);
    set(handles.rbRawData,'Value',1);
    
    set(handles.chkECG,'Enable','off');
    set(handles.chkBVP1,'Enable','off');
    set(handles.chkBVP2,'Enable','off');
end
% val = get(handles.rbRawData,'Value');
% val = mod(val+1,2)
% set(handles.rbRawData,'Value',val);
% val = mod(val+1,2)
% set(handles.rbProcessedData,'Value',val);


% --- Executes on button press in rbProcessedData.
function rbProcessedData_Callback(hObject, eventdata, handles)
% hObject    handle to rbProcessedData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of rbProcessedData
clearAllData(handles);
clearPlots();
clearDeletedPeak();



if(get(handles.rbProcessedData,'Value')==0)
    set(handles.rbProcessedData,'Value',0);
    set(handles.rbRawData,'Value',1);
    
    set(handles.chkECG,'Enable','off');
    set(handles.chkBVP1,'Enable','off');
    set(handles.chkBVP2,'Enable','off');
    
    
    



else
    set(handles.rbProcessedData,'Value',1);
    set(handles.rbRawData,'Value',0);
    
    set(handles.chkECG,'Enable','on');
    set(handles.chkBVP1,'Enable','on');
    set(handles.chkBVP2,'Enable','on');
     
end


function edit57_Callback(hObject, eventdata, handles)
% hObject    handle to edit57 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit57 as text
%        str2double(get(hObject,'String')) returns contents of edit57 as a double


% --- Executes during object creation, after setting all properties.
function edit57_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit57 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton61.
function pushbutton61_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton61 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in txtErrLocBVP1.
function txtErrLocBVP1_Callback(hObject, eventdata, handles)
% hObject    handle to txtErrLocBVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global StartDisplayFrame
global bvpLocRwave

info = get(handles.txtErrLocBVP1,'String');
info = strsplit(info,':');
errPeakNo=str2num(info{2});
if ~isempty(errPeakNo)
    bvpErrLoc = bvpLocRwave(errPeakNo,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( bvpErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end


% --- Executes on button press in txtOverlapLocBVP1.
function txtOverlapLocBVP1_Callback(hObject, eventdata, handles)
% hObject    handle to txtOverlapLocBVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global StartDisplayFrame
global bvpLocRwave

info = get(handles.txtOverlapLocBVP1,'String');
info = strsplit(info,':');
errPeakNo=str2num(info{2});
if ~isempty(errPeakNo)
    bvpErrLoc = bvpLocRwave(errPeakNo,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( bvpErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end


% --- Executes on button press in txtErrLocBVP2.
function txtErrLocBVP2_Callback(hObject, eventdata, handles)
% hObject    handle to txtErrLocBVP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global StartDisplayFrame
global bvpLocRwave2

info = get(handles.txtErrLocBVP2,'String');
info = strsplit(info,':');
errPeakNo=str2num(info{2});
if ~isempty(errPeakNo)
    bvpErrLoc = bvpLocRwave2(errPeakNo,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( bvpErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end


% --- Executes on button press in txtOverlapLocBVP2.
function txtOverlapLocBVP2_Callback(hObject, eventdata, handles)
% hObject    handle to txtOverlapLocBVP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global StartDisplayFrame
global bvpLocRwave2

info = get(handles.txtOverlapLocBVP2,'String');
info = strsplit(info,':');
errPeakNo=str2num(info{2});
if ~isempty(errPeakNo)
    bvpErrLoc = bvpLocRwave2(errPeakNo,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( bvpErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end


% --- Executes on button press in pbZoonIn.
function pbZoonIn_Callback(hObject, eventdata, handles)
% hObject    handle to pbZoonIn (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global StartDisplayFrame
windowSize = str2num(get(handles.editDWindowSize,'String'));
startFrame = str2num(get(handles.editStartFrame,'String'));
%% move start display frame 12% further to center to give 25 % zoom out
StartDisplayFrame=ceil(max(ceil(StartDisplayFrame+windowSize*0.15),startFrame));
set(handles.editDWindowSize ,'String',num2str(ceil(windowSize*0.70)));
displaySignal(handles)


% --- Executes on button press in pbZoomOut.
function pbZoomOut_Callback(hObject, eventdata, handles)
% hObject    handle to pbZoomOut (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLABpb
% handles    structure with handles and user data (see GUIDATA)
global StartDisplayFrame
windowSize = str2num(get(handles.editDWindowSize,'String'));
startFrame = str2num(get(handles.editStartFrame,'String'));
%% move start display frame 12% further to center to give 25 % zoom out
StartDisplayFrame=ceil(max(StartDisplayFrame-windowSize*0.15,startFrame));
set(handles.editDWindowSize,'String',num2str(ceil(windowSize*1.30)))
displaySignal(handles)


% % --- Executes on button press in txtPOverlap.
% function txtPOverlap_Callback(hObject, eventdata, handles)
% % hObject    handle to txtPOverlap (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
%
% global StartDisplayFrame
% global ecgLocRwave
%
% info = get(handles.txtOverlapLocBVP1,'String');
% info = strsplit(info,':');
% errPeakNo=str2num(info{2});
% if ~isempty(errPeakNo)
%     bvpErrLoc = bvpLocRwave(errPeakNo,1);
%     windowSize = str2num(get(handles.editDWindowSize,'String'));
%     startFrame = str2num(get(handles.editStartFrame,'String'));
%     StartDisplayFrame = ceil(max( bvpErrLoc-windowSize/2,startFrame));
%     displaySignal(handles);
% end


% --- Executes on button press in txtPOverlap.
function txtPOverlap_Callback(hObject, eventdata, handles)
% hObject    handle to txtPOverlap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global StartDisplayFrame
global ecgLocRwave

%% info = T OP:PNo/TotalPeak (overlap)
info = get(handles.txtPOverlap,'String');
%% reomove T OP
info = strsplit(info,':');

info = info{2};
%% get Peak No %% remove total count
info = strsplit(info,'/');
errPeakNo=ceil(str2num(info{1}));

if ~isempty(errPeakNo)
    ecgErrLoc = ecgLocRwave(errPeakNo,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( ecgErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end


% % --- Executes on button press in txtGoToMinP.
% function txtGoToMinP_Callback(hObject, eventdata, handles)
% % hObject    handle to txtGoToMinP (see GCBO)
% % eventdata  reserved - to be defined in a future version of MATLAB
% % handles    structure with handles and user data (see GUIDATA)
% 
% global ecgLocSwave
% global ecgLocTwave
% global StartDisplayFrame
% global ecgLocRwave
% 
% minDist = ecgLocTwave-ecgLocSwave;
% [~,minLoc]=min(ecgLocTwave-ecgLocSwave);
% ecgErrLoc = ecgLocRwave(minLoc,1);
% windowSize = str2num(get(handles.editDWindowSize,'String'));
% startFrame = str2num(get(handles.editStartFrame,'String'));
% StartDisplayFrame = ceil(max( ecgErrLoc-windowSize/2,startFrame));
% displaySignal(handles);


% --- Executes on selection change in pMenuMinPDist.
function pMenuMinPDist_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuMinPDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuMinPDist contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuMinPDist
global ecgLocRwave
global StartDisplayFrame
selIndex = get(handles.pMenuMinPDist,'Value')
if selIndex >1
    minLocs = get(handles.pMenuMinPDist,'String');
    minLoc=str2num(minLocs{selIndex});
    
    ecgErrLoc = ecgLocRwave(minLoc,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( ecgErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end

% minLoc= get(handles.pMenuMinPDist,'String')
%
%
% allFiles = get(handles.pMenuSignalFiles,'String');
%     selectedIndex = get(handles.pMenuSignalFiles,'Value');
%     try
%         fileName = allFiles{selectedIndex};
%     catch
%         %% there is error when program is swich from exel to mat
%         selectedIndex=1;
%         fileName = allFiles{1}
%






% --- Executes during object creation, after setting all properties.
function pMenuMinPDist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuMinPDist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chkLowerRange.
function chkLowerRange_Callback(hObject, eventdata, handles)
% hObject    handle to chkLowerRange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkLowerRange
set(handles.chkLowerRange,'Value',1)
set(handles.chkHigherRange,'Value',0);

% --- Executes on button press in chkHigherRange.
function chkHigherRange_Callback(hObject, eventdata, handles)
% hObject    handle to chkHigherRange (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkHigherRange
set(handles.chkLowerRange,'Value',0)
set(handles.chkHigherRange,'Value',1);


% --- Executes on button press in chkInvertECG1.
function chkInvertECG_Callback(hObject, eventdata, handles)
% hObject    handle to chkInvertECG1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkInvertECG1


% --- Executes on button press in radiobutton3.
function radiobutton3_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton3



function ecgSTDistMin_Callback(hObject, eventdata, handles)
% hObject    handle to ecgSTDistMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ecgSTDistMin as text
%        str2double(get(hObject,'String')) returns contents of ecgSTDistMin as a double


% --- Executes during object creation, after setting all properties.
function ecgSTDistMin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ecgSTDistMin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pMenuECG_BVP1.
function pMenuECG_BVP1_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuECG_BVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuECG_BVP1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuECG_BVP1


% --- Executes during object creation, after setting all properties.
function pMenuECG_BVP1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuECG_BVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pMenuBVP1Lag.
function pMenuBVP1Lag_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuBVP1Lag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuBVP1Lag contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuBVP1Lag


global ecgLocRwave
global StartDisplayFrame
selIndex = get(handles.pMenuBVP1Lag,'Value')
if selIndex >1
    minLocs = get(handles.pMenuBVP1Lag,'String');
    minLoc=str2num(minLocs{selIndex});
    
    ecgErrLoc = ecgLocRwave(minLoc,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( ecgErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end










% --- Executes during object creation, after setting all properties.
function pMenuBVP1Lag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuBVP1Lag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pMenuBVP2Lag.
function pMenuBVP2Lag_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuBVP2Lag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuBVP2Lag contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuBVP2Lag

global ecgLocRwave
global StartDisplayFrame
selIndex = get(handles.pMenuBVP2Lag,'Value')
if selIndex >1
    minLocs = get(handles.pMenuBVP2Lag,'String');
    minLoc=str2num(minLocs{selIndex});
    
    ecgErrLoc = ecgLocRwave(minLoc,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( ecgErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end


% --- Executes during object creation, after setting all properties.
function pMenuBVP2Lag_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuBVP2Lag (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end






% --- Executes during object creation, after setting all properties.
function sliderView_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function editStepsMove_Callback(hObject, eventdata, handles)
% hObject    handle to editStepsMove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editStepsMove as text
%        str2double(get(hObject,'String')) returns contents of editStepsMove as a double


% --- Executes during object creation, after setting all properties.
function editStepsMove_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editStepsMove (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pbProcessedInExcel.
function pbProcessedInExcel_Callback(hObject, eventdata, handles)
% hObject    handle to pbProcessedInExcel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global fileDir;
global files
global SAVE_EXCEL_FILE 
SAVE_EXCEL_FILE = get(handles.editExcelFileName,'String');
TotalFiles = size(files,1);
%%h = waitbar(0,'Moving Data From mat to Excel');
for i=1:TotalFiles
   
    fileName = strsplit(files(i).name,'.m');
    fileName = fileName{1};
    fprintf('\n Working with file :%s   %d / %d',fileName,i,TotalFiles);
    %%waitbar(i/TotalFiles,h,strcat(num2str(i),' / ', num2str(TotalFiles),': Saving Processed file',fileName,' to Excel'));
    fileFullPath=strcat(fileDir,'/',files(i).name);
    clearAllData(handles);
    load(fileFullPath);
    
    try
        saveInExcel(handles,fileName);
    catch
        x=1;
    end
end
%%close(h);



function editAddSteps_Callback(hObject, eventdata, handles)
% hObject    handle to editAddSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editAddSteps as text
%        str2double(get(hObject,'String')) returns contents of editAddSteps as a double


% --- Executes during object creation, after setting all properties.
function editAddSteps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editAddSteps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function axes1_ButtonDownFcn(hObject, eventdata, handles)


% --- Executes on button press in pushbutton70.
function pushbutton70_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton70 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in chkDataECG.
function chkDataECG_Callback(hObject, eventdata, handles)
% hObject    handle to chkDataECG (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkDataECG
if get(handles.rbProcessedData,'Value')==0
    resetTabPannel(handles);
else
    set(handles.chkDataECG,'Value', not(get(handles.chkDataECG,'Value')));
end

% --- Executes on button press in chkDataBVP1.
function chkDataBVP1_Callback(hObject, eventdata, handles)
% hObject    handle to chkDataBVP1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkDataBVP1
if get(handles.rbProcessedData,'Value')==0
    resetTabPannel(handles);
else
    set(handles.chkDataBVP1,'Value', not(get(handles.chkDataBVP1,'Value')));
end




% --- Executes on button press in chkDataBVP2.
function chkDataBVP2_Callback(hObject, eventdata, handles)
% hObject    handle to chkDataBVP2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkDataBVP2
if get(handles.rbProcessedData,'Value')==0
    resetTabPannel(handles);
else
    set(handles.chkDataBVP2,'Value', not(get(handles.chkDataBVP2,'Value')));
end

% --- Executes on selection change in pMenuDataType.
function pMenuDataType_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuDataType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuDataType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuDataType


% --- Executes during object creation, after setting all properties.
function pMenuDataType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuDataType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit67_Callback(hObject, eventdata, handles)
% hObject    handle to editCuttOffBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editCuttOffBack as text
%        str2double(get(hObject,'String')) returns contents of editCuttOffBack as a double


% --- Executes during object creation, after setting all properties.
function edit67_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editCuttOffBack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pMenuMaxRSlope.
function pMenuMaxRSlope_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuMaxRSlope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuMaxRSlope contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuMaxRSlope

global ecgLocRwave
global StartDisplayFrame
selIndex = get(handles.pMenuMaxRSlope,'Value')
if selIndex >1
    minLocs = get(handles.pMenuMaxRSlope,'String');
    minLoc = strsplit(minLocs{selIndex},'-');
    minLoc = str2num(minLoc{1});
    
   
    
    ecgErrLoc = ecgLocRwave(minLoc,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( ecgErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end




% --- Executes during object creation, after setting all properties.
function pMenuMaxRSlope_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuMaxRSlope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pMenuClosestR.
function pMenuClosestR_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuClosestR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuClosestR contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuClosestR


global ecgLocRwave
global StartDisplayFrame
selIndex = get(handles.pMenuClosestR,'Value')
if selIndex >1
    minLocs = get(handles.pMenuClosestR,'String');
    minLoc=str2num(minLocs{selIndex});
    
    ecgErrLoc = ecgLocRwave(minLoc,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( ecgErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end



% --- Executes during object creation, after setting all properties.
function pMenuClosestR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuClosestR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pMenuWidestR.
function pMenuWidestR_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuWidestR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuWidestR contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuWidestR

global ecgLocRwave
global StartDisplayFrame
selIndex = get(handles.pMenuWidestR,'Value')
if selIndex >1
    minLocs = get(handles.pMenuWidestR,'String');
    minLoc=str2num(minLocs{selIndex});
    
    ecgErrLoc = ecgLocRwave(minLoc,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( ecgErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end




% --- Executes during object creation, after setting all properties.
function pMenuWidestR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuWidestR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pMenuRMaxDev.
function pMenuRMaxDev_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuRMaxDev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuRMaxDev contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuRMaxDev
global ecgLocRwave
global StartDisplayFrame
selIndex = get(handles.pMenuRMaxDev,'Value')
if selIndex >1
    minLocs = get(handles.pMenuRMaxDev,'String');
    minLoc=str2num(minLocs{selIndex});
    
    ecgErrLoc = ecgLocRwave(minLoc,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( ecgErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end


% --- Executes during object creation, after setting all properties.
function pMenuRMaxDev_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuRMaxDev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pMenuMaxBVP1RSlope.
function pMenuMaxBVP1RSlope_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuMaxBVP1RSlope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuMaxBVP1RSlope contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuMaxBVP1RSlope

global bvpLocRwave
global StartDisplayFrame
selIndex = get(handles.pMenuMaxBVP1RSlope,'Value')
if selIndex >1
    minLocs = get(handles.pMenuMaxBVP1RSlope,'String');
    minLoc = strsplit(minLocs{selIndex},'-');
    minLoc = str2num(minLoc{1});
    
   
    
    bvpErrLoc = bvpLocRwave(minLoc,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( bvpErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end


% --- Executes during object creation, after setting all properties.
function pMenuMaxBVP1RSlope_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuMaxBVP1RSlope (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pMenuBVP1PeakDev.
function pMenuBVP1PeakDev_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuBVP1PeakDev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuBVP1PeakDev contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuBVP1PeakDev

global  bvpLocRwave
global StartDisplayFrame
selIndex = get(handles.pMenuBVP1PeakDev,'Value')
if selIndex >1
    minLocs = get(handles.pMenuBVP1PeakDev,'String');
    minLoc=str2num(minLocs{selIndex});
    
    bvpErrLoc = bvpLocRwave(minLoc,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( bvpErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end


% --- Executes during object creation, after setting all properties.
function pMenuBVP1PeakDev_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuBVP1PeakDev (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pMenuBVP1ClosestR.
function pMenuBVP1ClosestR_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuBVP1ClosestR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuBVP1ClosestR contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuBVP1ClosestR


global bvpLocRwave
global StartDisplayFrame
selIndex = get(handles.pMenuBVP1ClosestR,'Value')
if selIndex >1
    minLocs = get(handles.pMenuBVP1ClosestR,'String');
    minLoc=str2num(minLocs{selIndex});
    
    bvpErrLoc = bvpLocRwave(minLoc,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( bvpErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end


% --- Executes during object creation, after setting all properties.
function pMenuBVP1ClosestR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuBVP1ClosestR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in pMenuBVP1WidestR.
function pMenuBVP1WidestR_Callback(hObject, eventdata, handles)
% hObject    handle to pMenuBVP1WidestR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns pMenuBVP1WidestR contents as cell array
%        contents{get(hObject,'Value')} returns selected item from pMenuBVP1WidestR

global bvpLocRwave
global StartDisplayFrame
selIndex = get(handles.pMenuBVP1WidestR,'Value')
if selIndex >1
    minLocs = get(handles.pMenuBVP1WidestR,'String');
    minLoc=str2num(minLocs{selIndex});
    
    bvpErrLoc = bvpLocRwave(minLoc,1);
    windowSize = str2num(get(handles.editDWindowSize,'String'));
    startFrame = str2num(get(handles.editStartFrame,'String'));
    StartDisplayFrame = ceil(max( bvpErrLoc-windowSize/2,startFrame));
    displaySignal(handles);
end


% --- Executes during object creation, after setting all properties.
function pMenuBVP1WidestR_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pMenuBVP1WidestR (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in chkInverseT.
function chkInverseT_Callback(hObject, eventdata, handles)
% hObject    handle to chkInverseT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkInverseT


% --- Executes on button press in chkRTInterval.
function chkRTInterval_Callback(hObject, eventdata, handles)
% hObject    handle to chkRTInterval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkRTInterval


% --- Executes on button press in chkTRInterval.
function chkTRInterval_Callback(hObject, eventdata, handles)
% hObject    handle to chkTRInterval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkTRInterval


% --- Executes on button press in chkRRInterval.
function chkRRInterval_Callback(hObject, eventdata, handles)
% hObject    handle to chkRRInterval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of chkRRInterval
